using Microsoft.VisualStudio.TestTools.UnitTesting;
using CharacterManagerForDrDplus.Services;
using System;

namespace CharacterManagerForDrDplusTests
{
	[TestClass]
	public class PenaltiesTests
	{
		[TestMethod]
		public void WeaponStrengthPenaltyTest()
		{
			Assert.AreEqual(0, Penalties.WeaponStrengthPenalty.GetCombatPenalty(0));
			Assert.AreEqual(-1, Penalties.WeaponStrengthPenalty.GetCombatPenalty(1));
			Assert.AreEqual(-1, Penalties.WeaponStrengthPenalty.GetCombatPenalty(2));
			Assert.AreEqual(-2, Penalties.WeaponStrengthPenalty.GetCombatPenalty(3));
			Assert.AreEqual(-2, Penalties.WeaponStrengthPenalty.GetCombatPenalty(4));
			Assert.AreEqual(-3, Penalties.WeaponStrengthPenalty.GetCombatPenalty(5));
			Assert.AreEqual(-3, Penalties.WeaponStrengthPenalty.GetCombatPenalty(6));
			Assert.AreEqual(-4, Penalties.WeaponStrengthPenalty.GetCombatPenalty(7));
			Assert.AreEqual(-4, Penalties.WeaponStrengthPenalty.GetCombatPenalty(8));
			Assert.AreEqual(-5, Penalties.WeaponStrengthPenalty.GetCombatPenalty(9));
			Assert.AreEqual(-5, Penalties.WeaponStrengthPenalty.GetCombatPenalty(10));
			Assert.AreEqual(null, Penalties.WeaponStrengthPenalty.GetCombatPenalty(11));


			Assert.AreEqual(0, Penalties.WeaponStrengthPenalty.GetAttackPenalty(0));
			Assert.AreEqual(0, Penalties.WeaponStrengthPenalty.GetAttackPenalty(1));
			Assert.AreEqual(-1, Penalties.WeaponStrengthPenalty.GetAttackPenalty(2));
			Assert.AreEqual(-1, Penalties.WeaponStrengthPenalty.GetAttackPenalty(3));
			Assert.AreEqual(-2, Penalties.WeaponStrengthPenalty.GetAttackPenalty(4));
			Assert.AreEqual(-2, Penalties.WeaponStrengthPenalty.GetAttackPenalty(5));
			Assert.AreEqual(-3, Penalties.WeaponStrengthPenalty.GetAttackPenalty(6));
			Assert.AreEqual(-3, Penalties.WeaponStrengthPenalty.GetAttackPenalty(7));
			Assert.AreEqual(-4, Penalties.WeaponStrengthPenalty.GetAttackPenalty(8));
			Assert.AreEqual(-4, Penalties.WeaponStrengthPenalty.GetAttackPenalty(9));
			Assert.AreEqual(-5, Penalties.WeaponStrengthPenalty.GetAttackPenalty(10));
			Assert.AreEqual(null, Penalties.WeaponStrengthPenalty.GetAttackPenalty(11));


			Assert.AreEqual(0, Penalties.WeaponStrengthPenalty.GetDefensePenalty(0));
			Assert.AreEqual(0, Penalties.WeaponStrengthPenalty.GetDefensePenalty(1));
			Assert.AreEqual(0, Penalties.WeaponStrengthPenalty.GetDefensePenalty(2));
			Assert.AreEqual(-1, Penalties.WeaponStrengthPenalty.GetDefensePenalty(3));
			Assert.AreEqual(-1, Penalties.WeaponStrengthPenalty.GetDefensePenalty(4));
			Assert.AreEqual(-2, Penalties.WeaponStrengthPenalty.GetDefensePenalty(5));
			Assert.AreEqual(-2, Penalties.WeaponStrengthPenalty.GetDefensePenalty(6));
			Assert.AreEqual(-3, Penalties.WeaponStrengthPenalty.GetDefensePenalty(7));
			Assert.AreEqual(-3, Penalties.WeaponStrengthPenalty.GetDefensePenalty(8));
			Assert.AreEqual(-4, Penalties.WeaponStrengthPenalty.GetDefensePenalty(9));
			Assert.AreEqual(-4, Penalties.WeaponStrengthPenalty.GetDefensePenalty(10));
			Assert.AreEqual(null, Penalties.WeaponStrengthPenalty.GetDefensePenalty(11));


			Assert.AreEqual(0, Penalties.WeaponStrengthPenalty.GetDamagePenalty(0));
			Assert.AreEqual(0, Penalties.WeaponStrengthPenalty.GetDamagePenalty(1));
			Assert.AreEqual(0, Penalties.WeaponStrengthPenalty.GetDamagePenalty(2));
			Assert.AreEqual(0, Penalties.WeaponStrengthPenalty.GetDamagePenalty(3));
			Assert.AreEqual(-1, Penalties.WeaponStrengthPenalty.GetDamagePenalty(4));
			Assert.AreEqual(-1, Penalties.WeaponStrengthPenalty.GetDamagePenalty(5));
			Assert.AreEqual(-2, Penalties.WeaponStrengthPenalty.GetDamagePenalty(6));
			Assert.AreEqual(-2, Penalties.WeaponStrengthPenalty.GetDamagePenalty(7));
			Assert.AreEqual(-3, Penalties.WeaponStrengthPenalty.GetDamagePenalty(8));
			Assert.AreEqual(-3, Penalties.WeaponStrengthPenalty.GetDamagePenalty(9));
			Assert.AreEqual(-4, Penalties.WeaponStrengthPenalty.GetDamagePenalty(10));
			Assert.AreEqual(null, Penalties.WeaponStrengthPenalty.GetDamagePenalty(11));
		}

		[TestMethod]
		public void WeaponSkillPenaltyTest()
		{
			Assert.AreEqual(-3, Penalties.WeaponSkillPenalty.GetCombatPenalty(0));
			Assert.AreEqual(-2, Penalties.WeaponSkillPenalty.GetCombatPenalty(1));
			Assert.AreEqual(-1, Penalties.WeaponSkillPenalty.GetCombatPenalty(2));
			Assert.AreEqual(0, Penalties.WeaponSkillPenalty.GetCombatPenalty(3));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Penalties.WeaponSkillPenalty.GetCombatPenalty(-1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Penalties.WeaponSkillPenalty.GetCombatPenalty(4));


			Assert.AreEqual(-3, Penalties.WeaponSkillPenalty.GetAttackPenalty(0));
			Assert.AreEqual(-2, Penalties.WeaponSkillPenalty.GetAttackPenalty(1));
			Assert.AreEqual(-1, Penalties.WeaponSkillPenalty.GetAttackPenalty(2));
			Assert.AreEqual(0, Penalties.WeaponSkillPenalty.GetAttackPenalty(3));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Penalties.WeaponSkillPenalty.GetAttackPenalty(-1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Penalties.WeaponSkillPenalty.GetAttackPenalty(4));


			Assert.AreEqual(-2, Penalties.WeaponSkillPenalty.GetBlockPenalty(0));
			Assert.AreEqual(-1, Penalties.WeaponSkillPenalty.GetBlockPenalty(1));
			Assert.AreEqual(-1, Penalties.WeaponSkillPenalty.GetBlockPenalty(2));
			Assert.AreEqual(0, Penalties.WeaponSkillPenalty.GetBlockPenalty(3));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Penalties.WeaponSkillPenalty.GetBlockPenalty(-1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Penalties.WeaponSkillPenalty.GetBlockPenalty(4));


			Assert.AreEqual(-1, Penalties.WeaponSkillPenalty.GetDamagePenalty(0));
			Assert.AreEqual(-1, Penalties.WeaponSkillPenalty.GetDamagePenalty(1));
			Assert.AreEqual(0, Penalties.WeaponSkillPenalty.GetDamagePenalty(2));
			Assert.AreEqual(0, Penalties.WeaponSkillPenalty.GetDamagePenalty(3));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Penalties.WeaponSkillPenalty.GetDamagePenalty(-1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Penalties.WeaponSkillPenalty.GetDamagePenalty(4));
		}
	}
}
