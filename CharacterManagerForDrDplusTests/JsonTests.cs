﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using CharacterManagerForDrDplus.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplusTests
{
	[TestClass]
	public class JsonTests
	{
		[TestMethod]
		public void RaceTest()
		{
			List<Race> list = new List<Race>(){ new Race("Člověk", "Lidská žena",
				new Dictionary<AttributeCode, int>(),
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Chr, 1 } },
				new List<string>(), "", 180, 80, 0, true,
				new RuleSource("pph",15)),
				new Race("Horal", "Lidská žena",
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, 1 }, { AttributeCode.Vol, 1 }, { AttributeCode.Int, -1 }, { AttributeCode.Chr, -1 } },
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Chr, 1 } },
				new List<string>(), "", 180, 80, 0, true,
				new RuleSource("pph",15)),
				new Race("Elf", "Elfka",
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Obr, 1 }, { AttributeCode.Zrc, 1 }, { AttributeCode.Vol, -2 }, { AttributeCode.Int, 1 }, { AttributeCode.Chr, 1 }, { AttributeCode.Odl, -1 } },
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Zrc, 1 }, { AttributeCode.Int, -1 }, { AttributeCode.Chr, 1 } },
				new List<string>(), "Zrak", 160, 50, -1, true,
				new RuleSource("pph",15)),
				new Race("Zelený elf", "Elfka",
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Obr, 1 }, { AttributeCode.Zrc, 0 }, { AttributeCode.Vol, -1 }, { AttributeCode.Int, 1 }, { AttributeCode.Chr, 1 }, { AttributeCode.Odl, -1 } },
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Zrc, 1 }, { AttributeCode.Int, -1 }, { AttributeCode.Chr, 1 } },
				new List<string>(), "Zrak", 160, 50, -1, true,
				new RuleSource("pph",16)),
				new Race("Trpaslík", "Trpaslice",
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, 1 }, { AttributeCode.Obr, -1 }, { AttributeCode.Vol, 2 }, { AttributeCode.Int, -1 }, { AttributeCode.Chr, -2 }, { AttributeCode.Odl, 1 }, { AttributeCode.Sms, -1 } },
				new Dictionary<AttributeCode, int>() { { AttributeCode.Zrc, -1 }, { AttributeCode.Int, 1 } },
				new List<string>() { "infravidění" }, "Hmat", 140, 70, 0, true,
				new RuleSource("pph",16)),
				new Race("Lesní trpaslík", "Trpaslice",
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, 1 }, { AttributeCode.Obr, -1 }, { AttributeCode.Vol, 1 }, { AttributeCode.Int, -1 }, { AttributeCode.Chr, -1 }, { AttributeCode.Odl, 1 }, { AttributeCode.Sms, -1 } },
				new Dictionary<AttributeCode, int>() { { AttributeCode.Zrc, -1 }, { AttributeCode.Int, 1 } },
				new List<string>() { "infravidění" }, "Hmat", 140, 70, 0, true,
				new RuleSource("pph",16)),
				new Race("Horský trpaslík", "Trpaslice",
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, 2 }, { AttributeCode.Obr, -1 }, { AttributeCode.Vol, 2 }, { AttributeCode.Int, -2 }, { AttributeCode.Chr, -2 }, { AttributeCode.Odl, 1 }, { AttributeCode.Sms, -1 } },
				new Dictionary<AttributeCode, int>() { { AttributeCode.Zrc, -1 }, { AttributeCode.Int, 1 } },
				new List<string>() { "infravidění" }, "Hmat", 140, 70, 0, true,
				new RuleSource("pph",17)) };
			JsonSerializerOptions options = new JsonSerializerOptions();
			options.Converters.Add(new JsonStringEnumConverter());
			options.WriteIndented = true;

			string s = JsonSerializer.Serialize(list, options);

			List<Race> deList = JsonSerializer.Deserialize<List<Race>>(s, options);

			for (int i = 0; i < list.Count; i++)
			{
				Assert.AreEqual(true,EqualRaces(list[i], deList[i]));
			}
		}

		private bool EqualRaces(Race a, Race b)
		{
			if (a.Name != b.Name ||
				a.FemaleName != b.FemaleName ||
				a.AverageHeight != b.AverageHeight ||
				a.AverageWeight != b.AverageWeight ||
				a.BaseSize != b.BaseSize ||
				a.GenderModifier.Keys.Count != b.GenderModifier.Keys.Count ||
				a.RacialAbilities.Count != b.RacialAbilities.Count ||
				a.RacialSense != b.RacialSense ||
				a.RacialAttributes.Keys.Count != b.RacialAttributes.Keys.Count)
				return false;

			foreach (AttributeCode item in a.GenderModifier.Keys)
			{
				if (!b.GenderModifier.ContainsKey(item) || b.GenderModifier[item] != a.GenderModifier[item])
					return false;
			}

			foreach (AttributeCode item in a.RacialAttributes.Keys)
			{
				if (!b.RacialAttributes.ContainsKey(item) || b.RacialAttributes[item] != a.RacialAttributes[item])
					return false;
			}

			foreach (string itemA in a.RacialAbilities)
			{
				bool tmp = true;
				foreach (string itemB in b.RacialAbilities)
				{
					if (itemA == itemB)
						tmp = false;
				}
				if (tmp)
					return false;
			}

			return true;
		}

		[TestMethod]
		public void MeleeWeaponTest()
		{
			List<MeleeWeapon> _meleeWeapons = new List<MeleeWeapon>()
			{
				new MeleeWeapon("Kyj","Zbraň",1,300,1,new RuleSource("pph",85),Guid.NewGuid(),"Palice a kyje",3,1,2,3,"D",2,false),
				new MeleeWeapon("Dýka","Zbraň",0.2,1200,1,new RuleSource("pph",85),Guid.NewGuid(),"Nože a dýky",-1,0,1,1,"B",2,false),
				new MeleeWeapon("Sekera","Zbraň",2,2400,1,new RuleSource("pph",85),Guid.NewGuid(),"Sekery",6,2,3,5,"S",2,false),
				new MeleeWeapon("Široký meč","Zbraň",1.8,4800,1,new RuleSource("pph",85),Guid.NewGuid(),"Meče",6,2,4,4,"S",4,false),
				new MeleeWeapon("Nůž","Zbraň",0.2,330,1,new RuleSource("pph",85),Guid.NewGuid(),"Nože a dýky",-3,0,0,-2,"B",1,false),
				new MeleeWeapon("Kopí","Zbraň",1.2,3900,1,new RuleSource("pph",85),Guid.NewGuid(),"Hole a kopí",3,4,3,4,"B",4,false),
				new MeleeWeapon("Dlouhá dýka","Zbraň",0.3,1500,1,new RuleSource("pph",85),Guid.NewGuid(),"Nože a dýky",1,1,1,2,"B",2,false),
				new MeleeWeapon("Válečná sekera","Zbraň",2.5,3600,1,new RuleSource("pph",85),Guid.NewGuid(),"Sekery",9,3,3,7,"S",3,false),
				new MeleeWeapon("Tesák","Zbraň",1.2,3000,1,new RuleSource("pph",85),Guid.NewGuid(),"Šavle a tesáky",3,1,2,3,"S",3,false),
				new MeleeWeapon("Šavle","Zbraň",1.5,5200,1,new RuleSource("pph",85),Guid.NewGuid(),"Šavle a tesáky",6,2,4,4,"S",3,false)
			};
			JsonSerializerOptions options = new JsonSerializerOptions();
			options.Converters.Add(new JsonStringEnumConverter());
			options.WriteIndented = true;
			options.Encoder = JavaScriptEncoder.Create(UnicodeRanges.BasicLatin, UnicodeRanges.Latin1Supplement, UnicodeRanges.LatinExtendedA);
			options.PropertyNameCaseInsensitive = true;

			foreach (MeleeWeapon item in _meleeWeapons)
			{
				string s = JsonSerializer.Serialize(item, options);

				MeleeWeapon mw = JsonSerializer.Deserialize<MeleeWeapon>(s, options);

				Assert.AreEqual(true, EqualMeleeWeapons(item,mw));
			}
		}

		private bool EqualMeleeWeapons(MeleeWeapon a, MeleeWeapon b)
		{
			if (a.Name != b.Name || a.Attack != b.Attack || a.Block != b.Block || a.Category != b.Category ||
				a.Count != b.Count || a.Damage != b.Damage || a.DamageType != b.DamageType ||
				a.IsTwoHanded != b.IsTwoHanded || a.Length != b.Length || a.NeededStrength != b.NeededStrength ||
				a.Price != b.Price || a.WeaponCategory != b.WeaponCategory || a.Weight != b.Weight ||
				a.Source.SourceCode != b.Source.SourceCode || a.Source.SourcePage != b.Source.SourcePage)
				return false;

			return true;
		}

		[TestMethod]
		public void ProfessionTest()
		{
			JsonSerializerOptions options = GetOptions();

			Profession f = ProfessionManager.GetInstance().GetProfessionData(typeof(Fighter)).GetNewInstance();
			Profession w = ProfessionManager.GetInstance().GetProfessionData(typeof(Wizard)).GetNewInstance();
			Profession t = ProfessionManager.GetInstance().GetProfessionData(typeof(Thief)).GetNewInstance();

			string sf = JsonSerializer.Serialize(f, options);
			string sw = JsonSerializer.Serialize(w, options);
			string st = JsonSerializer.Serialize(t, options);

			Profession fo = JsonSerializer.Deserialize<Fighter>(sf, options);
			Profession wo = JsonSerializer.Deserialize<Wizard>(sw, options);
			Profession to = JsonSerializer.Deserialize<Thief>(st, options);

			IList<Profession> p = new List<Profession>();
			p.Add(f);
			p.Add(w);
			p.Add(t);

			string s = JsonSerializer.Serialize(p, options);

			IList<Profession> r = JsonSerializer.Deserialize<IList<Profession>>(s, options);
			

		}

		[TestMethod]
		public void CharacterTest()
		{
			Character c = new Character("Josh", 5, ProfessionManager.GetInstance().GetProfessionData(typeof(Fighter)).GetNewInstance(), RaceManager.GetInstance().GetAllRaces()[0]);

			JsonSerializerOptions options = GetOptions();

			string s = JsonSerializer.Serialize(c, options);

			Character c2 = JsonSerializer.Deserialize<Character>(s, options);

			string s2 = JsonSerializer.Serialize(c2, options);

			Assert.AreEqual(true, c.Name == c2.Name && c2.Profession.Name == c.Profession.Name && s == s2);

		}

		private JsonSerializerOptions GetOptions()
		{
			JsonSerializerOptions options = new JsonSerializerOptions();
			options.Converters.Add(new JsonStringEnumConverter());
			options.WriteIndented = true;
			options.Encoder = JavaScriptEncoder.Create(UnicodeRanges.BasicLatin, UnicodeRanges.Latin1Supplement, UnicodeRanges.LatinExtendedA);
			options.PropertyNameCaseInsensitive = true;
			options.Converters.Add(new ProfessionConverterWithTypeDiscriminator());
			return options;
		}
	}
}
