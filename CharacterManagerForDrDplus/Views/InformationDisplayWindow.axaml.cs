using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using CharacterManagerForDrDplus.ViewModels;
using ReactiveUI;
using System;

namespace CharacterManagerForDrDplus.Views
{
	public class InformationDisplayWindow : ReactiveWindow<InformationDisplayViewModel>
	{
		public InformationDisplayWindow()
		{
			InitializeComponent();
#if DEBUG
			this.AttachDevTools();
#endif
			this.WhenActivated(d => d(ViewModel.CloseCommand.Subscribe(Close)));
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
