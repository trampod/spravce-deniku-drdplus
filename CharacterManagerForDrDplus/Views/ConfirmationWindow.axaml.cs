using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using CharacterManagerForDrDplus.ViewModels;
using ReactiveUI;
using System;

namespace CharacterManagerForDrDplus.Views
{
	public class ConfirmationWindow : ReactiveWindow<ConfirmationViewModel>
	{
		public ConfirmationWindow()
		{
			InitializeComponent();
#if DEBUG
			this.AttachDevTools();
#endif
			this.WhenActivated(d => d(ViewModel.ConfirmationOK.Subscribe(Close)));
			this.WhenActivated(d => d(ViewModel.ConfirmationCancel.Subscribe(Close)));
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
