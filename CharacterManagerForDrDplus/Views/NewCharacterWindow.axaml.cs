using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using CharacterManagerForDrDplus.ViewModels;
using ReactiveUI;
using System;

namespace CharacterManagerForDrDplus.Views
{
	public class NewCharacterWindow : ReactiveWindow<NewCharacterViewModel>
	{
		public NewCharacterWindow()
		{
			InitializeComponent();
#if DEBUG
			this.AttachDevTools();
#endif
			this.WhenActivated(d => d(ViewModel.NewCharacterCommand.Subscribe(Close)));
			this.WhenActivated(d => d(ViewModel.NewCharacterCancel.Subscribe(Close)));
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
