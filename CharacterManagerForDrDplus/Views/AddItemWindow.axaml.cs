using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using CharacterManagerForDrDplus.ViewModels;
using ReactiveUI;
using System;

namespace CharacterManagerForDrDplus.Views
{
	public class AddItemWindow : ReactiveWindow<AddItemViewModel>
	{
		public AddItemWindow()
		{
			InitializeComponent();
#if DEBUG
			this.AttachDevTools();
#endif
			this.WhenActivated(d => d(ViewModel.AddItemCommand.Subscribe(Close)));
			this.WhenActivated(d => d(ViewModel.AddItemCancel.Subscribe(Close)));
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
