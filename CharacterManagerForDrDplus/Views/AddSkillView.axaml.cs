using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace CharacterManagerForDrDplus.Views
{
	public class AddSkillView : UserControl
	{
		public AddSkillView()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
