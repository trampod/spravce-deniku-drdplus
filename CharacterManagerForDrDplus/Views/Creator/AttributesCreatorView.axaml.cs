using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using Avalonia.VisualTree;
using CharacterManagerForDrDplus.ViewModels;
using ReactiveUI;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Views
{
	public class AttributesCreatorView : ReactiveUserControl<AttributesCreatorViewModel>
	{
		public AttributesCreatorView()
		{
			InitializeComponent();
			this.WhenActivated(d => d(ViewModel.ShowInformationDisplayDialog.RegisterHandler(DoShowInformationDisplayDialogAsync)));
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}

		private async Task DoShowInformationDisplayDialogAsync
			(InteractionContext<InformationDisplayViewModel, ResultViewModel> interaction)
		{
			var dialog = new InformationDisplayWindow();
			dialog.DataContext = interaction.Input;

			var result = await dialog.ShowDialog<ResultViewModel?>(((IVisual)this).FindAncestorOfType<Window>());
			interaction.SetOutput(result);
		}
	}
}
