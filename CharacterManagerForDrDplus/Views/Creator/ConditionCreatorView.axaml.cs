using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace CharacterManagerForDrDplus.Views
{
	public class ConditionCreatorView : UserControl
	{
		public ConditionCreatorView()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
