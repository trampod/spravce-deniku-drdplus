using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using Avalonia.VisualTree;
using CharacterManagerForDrDplus.ViewModels;
using ReactiveUI;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Views
{
	public class SkillCreatorView : ReactiveUserControl<SkillCreatorViewModel>
	{
		public SkillCreatorView()
		{
			InitializeComponent();
			this.WhenActivated(d => d(ViewModel.ShowAddSkillDialog.RegisterHandler(DoShowAddSkillDialogAsync)));
			this.WhenActivated(d => d(ViewModel.ShowRemoveSkillDialog.RegisterHandler(DoShowRemoveSkillDialogAsync)));
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
		private async Task DoShowAddSkillDialogAsync(InteractionContext<AddSkillViewModel, SkillViewModel?> interaction)
		{
			var dialog = new AddSkillWindow();
			dialog.DataContext = interaction.Input;

			//var result = await dialog.ShowDialog<ItemViewModel?>(((IClassicDesktopStyleApplicationLifetime)Application.Current.ApplicationLifetime).MainWindow);
			var result = await dialog.ShowDialog<SkillViewModel?>(((IVisual)this).FindAncestorOfType<Window>());
			interaction.SetOutput(result);
		}
		private async Task DoShowRemoveSkillDialogAsync(InteractionContext<ConfirmationViewModel, ResultViewModel?> interaction)
		{
			var dialog = new ConfirmationWindow();
			dialog.DataContext = interaction.Input;

			//var result = await dialog.ShowDialog<ItemViewModel?>(((IClassicDesktopStyleApplicationLifetime)Application.Current.ApplicationLifetime).MainWindow);
			var result = await dialog.ShowDialog<ResultViewModel?>(((IVisual)this).FindAncestorOfType<Window>());
			interaction.SetOutput(result);
		}
	}
}
