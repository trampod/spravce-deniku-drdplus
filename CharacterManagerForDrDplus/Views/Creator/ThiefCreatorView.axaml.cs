using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using Avalonia.VisualTree;
using CharacterManagerForDrDplus.ViewModels;
using ReactiveUI;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Views
{
	public class ThiefCreatorView : ReactiveUserControl<ThiefCreatorViewModel>
	{
		public ThiefCreatorView()
		{
			InitializeComponent();
			this.WhenActivated(d => d(ViewModel.ShowAddThiefSkillDialog.RegisterHandler(DoShowAddThiefSkillDialogAsync)));
			this.WhenActivated(d => d(ViewModel.ShowRemoveThiefSkillDialog.RegisterHandler(DoShowRemoveDialogAsync)));
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
		private async Task DoShowAddThiefSkillDialogAsync(InteractionContext<AddThiefSkillViewModel, ThiefSkillViewModel?> interaction)
		{
			var dialog = new AddThiefSkillWindow();
			dialog.DataContext = interaction.Input;

			//var result = await dialog.ShowDialog<ItemViewModel?>(((IClassicDesktopStyleApplicationLifetime)Application.Current.ApplicationLifetime).MainWindow);
			var result = await dialog.ShowDialog<ThiefSkillViewModel?>(((IVisual)this).FindAncestorOfType<Window>());
			interaction.SetOutput(result);
		}
		private async Task DoShowRemoveDialogAsync(InteractionContext<ConfirmationViewModel, ResultViewModel?> interaction)
		{
			var dialog = new ConfirmationWindow();
			dialog.DataContext = interaction.Input;

			//var result = await dialog.ShowDialog<ItemViewModel?>(((IClassicDesktopStyleApplicationLifetime)Application.Current.ApplicationLifetime).MainWindow);
			var result = await dialog.ShowDialog<ResultViewModel?>(((IVisual)this).FindAncestorOfType<Window>());
			interaction.SetOutput(result);
		}
	}
}
