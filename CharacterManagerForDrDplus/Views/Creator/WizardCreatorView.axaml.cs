using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace CharacterManagerForDrDplus.Views
{
	public class WizardCreatorView : UserControl
	{
		public WizardCreatorView()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
