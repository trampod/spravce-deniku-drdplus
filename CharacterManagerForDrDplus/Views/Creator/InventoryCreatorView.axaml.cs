using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using Avalonia.VisualTree;
using CharacterManagerForDrDplus.ViewModels;
using ReactiveUI;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Views
{
	public class InventoryCreatorView : ReactiveUserControl<InventoryCreatorViewModel>
	{
		public InventoryCreatorView()
		{
			InitializeComponent();
			this.WhenActivated(d => d(ViewModel.ShowAddItemDialog.RegisterHandler(DoShowAddItemDialogAsync)));
			this.WhenActivated(d => d(ViewModel.ShowRemoveItemDialog.RegisterHandler(DoShowRemoveDialogAsync)));
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
		private async Task DoShowAddItemDialogAsync(InteractionContext<AddItemViewModel, ItemDataViewModel?> interaction)
		{
			var dialog = new AddItemWindow();
			dialog.DataContext = interaction.Input;

			//var result = await dialog.ShowDialog<ItemViewModel?>(((IClassicDesktopStyleApplicationLifetime)Application.Current.ApplicationLifetime).MainWindow);
			var result = await dialog.ShowDialog<ItemDataViewModel?>(((IVisual)this).FindAncestorOfType<Window>());
			interaction.SetOutput(result);
		}
		private async Task DoShowRemoveDialogAsync(InteractionContext<ConfirmationViewModel, ResultViewModel?> interaction)
		{
			var dialog = new ConfirmationWindow();
			dialog.DataContext = interaction.Input;

			//var result = await dialog.ShowDialog<ItemViewModel?>(((IClassicDesktopStyleApplicationLifetime)Application.Current.ApplicationLifetime).MainWindow);
			var result = await dialog.ShowDialog<ResultViewModel?>(((IVisual)this).FindAncestorOfType<Window>());
			interaction.SetOutput(result);
		}
	}
}
