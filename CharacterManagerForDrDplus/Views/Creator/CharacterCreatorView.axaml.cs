using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace CharacterManagerForDrDplus.Views
{
	public class CharacterCreatorView : UserControl
	{
		public CharacterCreatorView()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
