using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using CharacterManagerForDrDplus.ViewModels;
using ReactiveUI;
using System;

namespace CharacterManagerForDrDplus.Views
{
	public class AddSkillWindow : ReactiveWindow<AddSkillViewModel>
	{
		public AddSkillWindow()
		{
			InitializeComponent();
#if DEBUG
			this.AttachDevTools();
#endif
			this.WhenActivated(d => d(ViewModel.AddSkillCommand.Subscribe(Close)));
			this.WhenActivated(d => d(ViewModel.AddSkillCancel.Subscribe(Close)));
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
