using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using Avalonia.VisualTree;
using CharacterManagerForDrDplus.ViewModels;
using ReactiveUI;
using System.Reactive;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Views
{
	public class MainWindow : ReactiveWindow<MainWindowViewModel>
	{
		public MainWindow()
		{
			InitializeComponent();
			DataContext = new MainWindowViewModel(this);
#if DEBUG
			this.AttachDevTools();
#endif
			this.WhenActivated(d => d(ViewModel.ShowNewCharacterDialog.RegisterHandler(DoShowNewCharacterDialogAsync)));
			this.WhenActivated(d => d(ViewModel.ShowConfirmationDialog.RegisterHandler(DoShowConfirmationDialogAsync)));
			this.WhenActivated(d => d(ViewModel.ShowInformationDisplayDialog.RegisterHandler(DoShowInformationDisplayDialogAsync)));
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}

		private async Task DoShowNewCharacterDialogAsync(InteractionContext<NewCharacterViewModel, CharacterViewModel?> interaction)
		{
			var dialog = new NewCharacterWindow();
			dialog.DataContext = interaction.Input;

			var result = await dialog.ShowDialog<CharacterViewModel?>(this);
			interaction.SetOutput(result);
		}

		private async Task DoShowConfirmationDialogAsync(InteractionContext<ConfirmationViewModel, ResultViewModel?> interaction)
		{
			var dialog = new ConfirmationWindow();
			dialog.DataContext = interaction.Input;

			var result = await dialog.ShowDialog<ResultViewModel?>(this);
			interaction.SetOutput(result);
		}

		private async Task DoShowInformationDisplayDialogAsync
			(InteractionContext<InformationDisplayViewModel, ResultViewModel> interaction)
		{
			var dialog = new InformationDisplayWindow();
			dialog.DataContext = interaction.Input;

			var result = await dialog.ShowDialog<ResultViewModel?>(this);
			interaction.SetOutput(result);
		}
	}
}