using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace CharacterManagerForDrDplus.Views
{
	public class EmptyCharacterView : UserControl
	{
		public EmptyCharacterView()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			AvaloniaXamlLoader.Load(this);
		}
	}
}
