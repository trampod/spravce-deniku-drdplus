﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class ThiefSkillCategory : Category<ThiefSkill>
	{
		public ThiefSkillCategory(string categoryName, IList<ThiefSkill> items) : base(categoryName, items)
		{
		}
	}
}
