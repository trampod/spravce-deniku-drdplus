﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class WizardData : ProfessionData
	{
		public override Profession GetNewInstance()
		{
			Wizard newWizard = new Wizard(CombatAttribute, MainAttributes);

			return newWizard;
		}

		public WizardData(AttributeCode combatAttribute, AttributeCode[] mainAttributes, int[,] startSkillTable) : base(combatAttribute, mainAttributes, startSkillTable)
		{

		}
	}
}
