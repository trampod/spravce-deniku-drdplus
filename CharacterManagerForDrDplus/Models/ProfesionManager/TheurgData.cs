﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class TheurgData : ProfessionData
	{
		public override Profession GetNewInstance()
		{
			Theurg newTheurg = new Theurg(CombatAttribute, MainAttributes);

			return newTheurg;
		}

		public TheurgData(AttributeCode combatAttribute, AttributeCode[] mainAttributes, int[,] startSkillTable) : base(combatAttribute, mainAttributes, startSkillTable)
		{

		}
	}
}
