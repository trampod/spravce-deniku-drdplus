﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class PriestData : ProfessionData
	{
		public override Profession GetNewInstance()
		{
			Priest newFighter = new Priest(CombatAttribute, MainAttributes);

			return newFighter;
		}

		public PriestData(AttributeCode combatAttribute, AttributeCode[] mainAttributes, int[,] startSkillTable) : base(combatAttribute, mainAttributes, startSkillTable)
		{

		}
	}
}
