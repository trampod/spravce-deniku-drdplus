﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class ThiefData : ProfessionData
	{
		public Dictionary<string,List<ThiefSkill>> ThiefSkillCategories { get; }
		public List<ThiefManeuver> ThiefManeuvers { get; }
		public List<string> GuildPositions { get; }

		public override Profession GetNewInstance()
		{
			Thief newThief = new Thief(CombatAttribute,MainAttributes,new List<ThiefSkill>(), new List<ThiefManeuver>(),0);

			return newThief;
		}

		public ThiefData(AttributeCode combatAttribute, AttributeCode[] mainAttributes, int[,] startSkillTable,
			Dictionary<string,List<ThiefSkill>> thiefSkillCategories, List<ThiefManeuver> thiefManeuvers, List<string> guildPositions) : base (combatAttribute,  mainAttributes, startSkillTable)
		{
			ThiefSkillCategories = thiefSkillCategories;
			ThiefManeuvers = thiefManeuvers;
			GuildPositions = guildPositions;
		}
	}
}
