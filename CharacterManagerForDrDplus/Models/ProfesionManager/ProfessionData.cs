﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public abstract class ProfessionData
	{
		public int[,] StartSkillTable { get; }
		public AttributeCode CombatAttribute { get; }
		public AttributeCode[] MainAttributes { get; }

		public ProfessionData(AttributeCode combatAttribute, AttributeCode[] mainAttributes, int[,] startSkillTable)
		{
			StartSkillTable = startSkillTable;
			CombatAttribute = combatAttribute;
			MainAttributes = mainAttributes;
		}

		internal void GetStartSkills(int startSkillGrade,out int startPhysicalSkills, out int startMentalSkills, out int startCombinedSkills)
		{
			startPhysicalSkills = StartSkillTable[startSkillGrade, 0];
			startMentalSkills = StartSkillTable[startSkillGrade, 1];
			startCombinedSkills = StartSkillTable[startSkillGrade, 2];
		}

		public abstract Profession GetNewInstance();
	}
}
