﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class FighterData : ProfessionData
	{
		public override Profession GetNewInstance()
		{
			Fighter newFighter = new Fighter(CombatAttribute, MainAttributes);

			return newFighter;
		}

		public FighterData(AttributeCode combatAttribute, AttributeCode[] mainAttributes, int[,] startSkillTable) : base(combatAttribute, mainAttributes, startSkillTable)
		{

		}
	}
}
