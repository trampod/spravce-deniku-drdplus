﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class ProfessionManager
	{
		private static ProfessionManager? instance;

		private Dictionary<Type,ProfessionData> professionDatas;

		public static ProfessionManager GetInstance()
		{
			if (instance == null) instance = DataLoader.LoadProfessionManagerData();//new ProfessionManager();
			return instance;
		}

		public ProfessionManager(Dictionary<Type, ProfessionData> data)
		{
			professionDatas = data;
			/*
			ThiefData thiefData = new ThiefData(AttributeCode.Zrc, new AttributeCode[] { AttributeCode.Zrc, AttributeCode.Obr }, new int[,]
				{
					{1,1,1 },
					{2,1,1 },
					{2,1,2 },
					{3,2,2 },
					{4,2,3 },
					{5,2,4 },
					{6,3,5 },
					{8,4,6 },
					{9,3,7 }
				});
			FighterData fighterData = new FighterData(AttributeCode.Obr, new AttributeCode[] { AttributeCode.Obr, AttributeCode.Sil }, new int[,]
				{
					{2,0,1 },
					{3,0,1 },
					{4,0,1 },
					{4,1,2 },
					{5,1,3 },
					{6,2,3 },
					{8,2,4 },
					{10,3,5 },
					{12,4,6 }
				});
			WizardData wizardData = new WizardData(AttributeCode.Int, new AttributeCode[] { AttributeCode.Int, AttributeCode.Vol }, new int[,]
				{
					{0,3,0 },
					{1,3,0 },
					{1,4,0 },
					{2,4,1 },
					{2,5,2 },
					{3,6,2 },
					{4,7,3 },
					{5,9,4 },
					{6,11,5 }
				});
			professionDatas = new Dictionary<Type, ProfessionData>()
			{
				{ typeof(Thief), thiefData },
				{ typeof(Fighter), fighterData },
				{ typeof(Wizard), wizardData }
			};*/
		}

		public static List<Type> GetAllProfessions()
		{
			return new List<Type>() { typeof(Fighter), typeof(Wizard), typeof(Thief), typeof(Theurg), typeof(Ranger), typeof(Priest) };
		}

		public ProfessionData GetProfessionData(Profession profession)
		{
			return professionDatas[profession.GetType()];
		}

		public ProfessionData GetProfessionData(Type professionType)
		{
			return professionDatas[professionType];
		}
	}
}
