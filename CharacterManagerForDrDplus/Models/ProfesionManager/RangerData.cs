﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class RangerData : ProfessionData
	{
		public override Profession GetNewInstance()
		{
			Ranger newRanger = new Ranger(CombatAttribute, MainAttributes);

			return newRanger;
		}

		public RangerData(AttributeCode combatAttribute, AttributeCode[] mainAttributes, int[,] startSkillTable) : base(combatAttribute, mainAttributes, startSkillTable)
		{

		}
	}
}
