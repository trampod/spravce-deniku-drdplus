﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public static class DataLoader
	{
		private const string DATA_LOCATION = "Data\\";
		private const string RACES_LOCATION = DATA_LOCATION + "Races.json";
		private const string ITEMS_LOCATION = DATA_LOCATION + "Items.json";
		private const string SKILLS_LOCATION = DATA_LOCATION + "Skills.json";
		private const string CHARACTER_LOCATION = DATA_LOCATION + "Character.plus";
		//private const string PROFESSION_LOCATION = "ProfessionData.json";
		private const string HELP_LOCATION = DATA_LOCATION + "Help.json";

		private static JsonSerializerOptions GetJsonSerializerOptions()
		{
			JsonSerializerOptions options = new JsonSerializerOptions();
			options.Converters.Add(new JsonStringEnumConverter());
			options.Converters.Add(new Array2DConverter());
			options.WriteIndented = true;
			options.Encoder = JavaScriptEncoder.Create(UnicodeRanges.BasicLatin, UnicodeRanges.Latin1Supplement, UnicodeRanges.LatinExtendedA);
			options.PropertyNameCaseInsensitive = true;
			return options;
		}

		public static RaceManager LoadRaceManagerData(string fileLocation = RACES_LOCATION)
		{
			JsonSerializerOptions options = GetJsonSerializerOptions();

			string s = File.ReadAllText(fileLocation);

			RaceManager? manager = JsonSerializer.Deserialize<RaceManager>(s,options);

			if (manager == null)
				throw new Exception();
			return manager;
		}

		public static void SaveRaceManagerData(RaceManager raceManager, string fileLocation = RACES_LOCATION)
		{
			JsonSerializerOptions options = GetJsonSerializerOptions();

			string s = JsonSerializer.Serialize(raceManager, options);

			File.WriteAllText(fileLocation, s);
		}

		public static ItemManager LoadItemManagerData(string fileLocation = ITEMS_LOCATION)
		{
			JsonSerializerOptions options = GetJsonSerializerOptions();

			string s = File.ReadAllText(fileLocation);

			ItemManager? manager = JsonSerializer.Deserialize<ItemManager>(s, options);

			if (manager == null)
				throw new Exception();
			return manager;
		}

		public static void SaveItemManagerData(ItemManager itemManager, string fileLocation = ITEMS_LOCATION)
		{
			JsonSerializerOptions options = GetJsonSerializerOptions();

			string s = JsonSerializer.Serialize(itemManager, options);

			File.WriteAllText(fileLocation, s);
		}

		public static SkillManager LoadSkillManagerData(string fileLocation = SKILLS_LOCATION)
		{

			JsonSerializerOptions options = GetJsonSerializerOptions();

			string s = File.ReadAllText(fileLocation);

			SkillManager? manager = JsonSerializer.Deserialize<SkillManager>(s, options);

			if (manager == null)
				throw new Exception();
			return manager;

		}

		public static void SaveSkillManagerData(SkillManager skillManager, string fileLocation = SKILLS_LOCATION)
		{
			JsonSerializerOptions options = GetJsonSerializerOptions();

			string s = JsonSerializer.Serialize(skillManager, options);

			File.WriteAllText(fileLocation, s);
		}

		public static void SaveCharacter(Character character, string fileLocation = CHARACTER_LOCATION)
		{
			JsonSerializerOptions options = GetJsonSerializerOptions();
			options.Converters.Add(new ProfessionConverterWithTypeDiscriminator());

			//Fighter f = new Fighter("fff", Services.AttributeCode.Chr, new Services.AttributeCode[] { Services.AttributeCode.Sil, Services.AttributeCode.Obr });
			//string sf = JsonSerializer.Serialize(f, options);
			//Fighter g = JsonSerializer.Deserialize<Fighter>(sf, options);
			//string sg = JsonSerializer.Serialize(g, options);

			string s = JsonSerializer.Serialize(character, options);

			File.WriteAllText(fileLocation, s);
		}

		public static Character LoadCharacter(string fileLocation = CHARACTER_LOCATION)
		{
			JsonSerializerOptions options = GetJsonSerializerOptions();
			options.Converters.Add(new ProfessionConverterWithTypeDiscriminator());

			//Fighter f = new Fighter("fff", Services.AttributeCode.Chr, new Services.AttributeCode[] { Services.AttributeCode.Sil, Services.AttributeCode.Obr });
			//string sf = JsonSerializer.Serialize(f, options);
			//Profession g = JsonSerializer.Deserialize<Fighter>(sf, options);
			//string sg = JsonSerializer.Serialize(g, options);
			//string test = "{\"Discriminator\": 1,\"Data\": {\"Name\": \"Bojovník\",\"CombatAttribute\": \"Obr\",\"MainAttributes\": [\"Obr\",\"Sil\"]}}";

			//Profession p = JsonSerializer.Deserialize<Profession>(test, options);




			string s = File.ReadAllText(fileLocation);

			Character? c = JsonSerializer.Deserialize<Character>(s, options);

			return c!;
		}

		public static void SaveProfessionManagerData(ProfessionManager professionManager)//, string fileLocation = PROFESSION_LOCATION)
		{
			JsonSerializerOptions options = GetJsonSerializerOptions();
			options.Converters.Add(new ProfessionDataConverterWithTypeDiscriminator());

			List<Type> professions = ProfessionManager.GetAllProfessions();

			foreach (Type item in professions)
			{
				ProfessionData pd = professionManager.GetProfessionData(item);
				string s = JsonSerializer.Serialize(pd, options);

				string fileLocation = DATA_LOCATION + item.Name + ".json";

				File.WriteAllText(fileLocation, s);

			}
		}

		public static ProfessionManager LoadProfessionManagerData()//, string fileLocation = PROFESSION_LOCATION)
		{
			JsonSerializerOptions options = GetJsonSerializerOptions();
			options.Converters.Add(new ProfessionDataConverterWithTypeDiscriminator());

			List<Type> professions = ProfessionManager.GetAllProfessions();
			Dictionary<Type, ProfessionData> professionDatas = new Dictionary<Type, ProfessionData>();

			foreach (Type item in professions)
			{

				string fileLocation = DATA_LOCATION + item.Name + ".json";

				string s = File.ReadAllText(fileLocation);

				//File.WriteAllText(fileLocation, s);
				ProfessionData? pd = JsonSerializer.Deserialize<ProfessionData>(s, options);
				if (pd == null) throw new Exception();
				professionDatas.Add(item,pd);
			}

			return new ProfessionManager(professionDatas);
		}

		internal static HelpManager LoadHelp(string fileLocation = HELP_LOCATION)
		{
			JsonSerializerOptions options = GetJsonSerializerOptions();

			string s = File.ReadAllText(fileLocation);

			HelpManager? manager = JsonSerializer.Deserialize<HelpManager>(s, options);

			if (manager == null)
				throw new Exception();
			return manager;
		}
	}
}
