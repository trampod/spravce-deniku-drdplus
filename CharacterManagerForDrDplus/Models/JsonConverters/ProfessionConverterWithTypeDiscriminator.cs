﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class ProfessionConverterWithTypeDiscriminator : JsonConverter<Profession>
	{
		private const string DISCRIMINATOR_NAME = "Discriminator";
		private const string DATA_NAME = "Data";

		public override Profession Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			if (reader.TokenType != JsonTokenType.StartObject)
			{
				throw new JsonException();
			}

			reader.Read();
			if (reader.TokenType != JsonTokenType.PropertyName)
			{
				throw new JsonException();
			}

			string propertyName = reader.GetString();
			if (propertyName != DISCRIMINATOR_NAME)
			{
				throw new JsonException();
			}

			reader.Read();
			if (reader.TokenType != JsonTokenType.Number)
			{
				throw new JsonException();
			}

			ProfessionDiscriminator typeDiscriminator = (ProfessionDiscriminator)reader.GetInt32();

			reader.Read();
			if (reader.TokenType != JsonTokenType.PropertyName)
			{
				throw new JsonException();
			}

			string dataName = reader.GetString();
			if (dataName != DATA_NAME)
			{
				throw new JsonException();
			}

			reader.Read();
			if (reader.TokenType != JsonTokenType.StartObject)
			{
				throw new JsonException();
			}

			Profession? readValue = typeDiscriminator switch
			{
				ProfessionDiscriminator.Fighter => JsonSerializer.Deserialize<Fighter>(ref reader,options),
				ProfessionDiscriminator.Wizard => JsonSerializer.Deserialize<Wizard>(ref reader,options),
				ProfessionDiscriminator.Thief => JsonSerializer.Deserialize<Thief>(ref reader, options),
				ProfessionDiscriminator.Theurg => JsonSerializer.Deserialize<Theurg>(ref reader, options),
				ProfessionDiscriminator.Ranger => JsonSerializer.Deserialize<Ranger>(ref reader, options),
				ProfessionDiscriminator.Priest => JsonSerializer.Deserialize<Priest>(ref reader, options),
				_ => throw new ArgumentException()
			};
			reader.Read();

			return readValue!;


			throw new JsonException();
		}
		public override void Write(Utf8JsonWriter writer, Profession value, JsonSerializerOptions options)
		{
			//ProfesionDiscriminator typeDiscriminator = value.Discriminator;

			ProfessionDiscriminator typeDiscriminator = value switch
			{
				Fighter => ProfessionDiscriminator.Fighter,
				Wizard => ProfessionDiscriminator.Wizard,
				Thief => ProfessionDiscriminator.Thief,
				Theurg => ProfessionDiscriminator.Theurg,
				Ranger => ProfessionDiscriminator.Ranger,
				Priest => ProfessionDiscriminator.Priest,
				_ => throw new ArgumentException("Invalid profesion type")
			};

			writer.WriteStartObject();

			writer.WriteNumber(DISCRIMINATOR_NAME, (int)typeDiscriminator);

			writer.WritePropertyName(DATA_NAME);
			JsonSerializer.Serialize(writer, value, value.GetType(), options);

			writer.WriteEndObject();
		}
	}

	public enum ProfessionDiscriminator
	{
		Fighter = 1,
		Wizard = 2,
		Thief = 3,
		Theurg = 4,
		Ranger = 5,
		Priest = 6
	}
}
