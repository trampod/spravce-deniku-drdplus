﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	class ProfessionDataConverterWithTypeDiscriminator : JsonConverter<ProfessionData>
	{
		private const string DISCRIMINATOR_NAME = "Discriminator";
		private const string DATA_NAME = "Data";

		public override ProfessionData Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			if (reader.TokenType != JsonTokenType.StartObject)
			{
				throw new JsonException();
			}

			reader.Read();
			if (reader.TokenType != JsonTokenType.PropertyName)
			{
				throw new JsonException();
			}

			string propertyName = reader.GetString();
			if (propertyName != DISCRIMINATOR_NAME)
			{
				throw new JsonException();
			}

			reader.Read();
			if (reader.TokenType != JsonTokenType.Number)
			{
				throw new JsonException();
			}

			ProfessionDiscriminator typeDiscriminator = (ProfessionDiscriminator)reader.GetInt32();

			reader.Read();
			if (reader.TokenType != JsonTokenType.PropertyName)
			{
				throw new JsonException();
			}

			string dataName = reader.GetString();
			if (dataName != DATA_NAME)
			{
				throw new JsonException();
			}

			reader.Read();
			if (reader.TokenType != JsonTokenType.StartObject)
			{
				throw new JsonException();
			}

			ProfessionData? readValue = typeDiscriminator switch
			{
				ProfessionDiscriminator.Fighter => JsonSerializer.Deserialize<FighterData>(ref reader, options),
				ProfessionDiscriminator.Wizard => JsonSerializer.Deserialize<WizardData>(ref reader, options),
				ProfessionDiscriminator.Thief => JsonSerializer.Deserialize<ThiefData>(ref reader, options),
				ProfessionDiscriminator.Theurg => JsonSerializer.Deserialize<TheurgData>(ref reader, options),
				ProfessionDiscriminator.Ranger => JsonSerializer.Deserialize<RangerData>(ref reader, options),
				ProfessionDiscriminator.Priest => JsonSerializer.Deserialize<PriestData>(ref reader, options),
				_ => throw new ArgumentException()
			};
			reader.Read();

			return readValue!;


			throw new JsonException();
		}
		public override void Write(Utf8JsonWriter writer, ProfessionData value, JsonSerializerOptions options)
		{
			//ProfesionDiscriminator typeDiscriminator = value.Discriminator;

			ProfessionDiscriminator typeDiscriminator = value switch
			{
				FighterData => ProfessionDiscriminator.Fighter,
				WizardData => ProfessionDiscriminator.Wizard,
				ThiefData => ProfessionDiscriminator.Thief,
				TheurgData => ProfessionDiscriminator.Theurg,
				RangerData => ProfessionDiscriminator.Ranger,
				PriestData => ProfessionDiscriminator.Priest,
				_ => throw new ArgumentException("Invalid profesion type")
			};

			writer.WriteStartObject();

			writer.WriteNumber(DISCRIMINATOR_NAME, (int)typeDiscriminator);

			writer.WritePropertyName(DATA_NAME);
			JsonSerializer.Serialize(writer, value, value.GetType(), options);

			writer.WriteEndObject();
		}
	}
}
