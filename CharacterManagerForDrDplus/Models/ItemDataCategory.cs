﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class ItemDataCategory : Category<IItemData>
	{
		public ItemDataCategory(string categoryName, IList<IItemData> items) : base(categoryName, items)
		{
		}
	}
}
