﻿using CharacterManagerForDrDplus.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public static class ViewModelMessenger
	{
        private static readonly ICollection<ViewModelBase> _viewModels = new HashSet<ViewModelBase>();

        public static void Register(ViewModelBase viewModel)
        {
            _viewModels.Add(viewModel);
        }

        public static void Unregister(ViewModelBase viewModel)
        {
            _viewModels.Remove(viewModel);
        }

        public static void NotifyOthers(ViewModelBase viewModel, IEnumerable<String> properties)
        {
            foreach (ViewModelBase vm in _viewModels)
            {
                if (Object.ReferenceEquals(viewModel, vm))
                    continue;

                vm.OnModelChanged(properties);
            }
        }

        public static void NotifyAll(IEnumerable<String> properties)
        {
            foreach (ViewModelBase vm in _viewModels)
            {
                vm.OnModelChanged(properties);
            }
        }

        public static void NotifySingle(ViewModelBase viewModel, IEnumerable<String> properties)
        {
            viewModel.OnModelChanged(properties);
        }
    }
}
