﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class AmmoData : IItemData
	{
		private string _name;
		private string _category;
		private double _weight;
		private int? _averagePrice;
		private RuleSource _source;

		private int? _neededStrength;
		private int _attack;
		private int _damage;
		private string _damageType;
		private int _range;

		public AmmoData(string name, string category, double weight, int? averagePrice, RuleSource source, int? neededStrength, int attack, int damage, string damageType, int range)
		{
			_name = name;
			_category = category;
			_weight = weight;
			_averagePrice = averagePrice;
			_source = source;

			_neededStrength = neededStrength;
			_attack = attack;
			_damage = damage;
			_damageType = damageType;
			_range = range;
		}

		public string Name => _name;

		public string Category => _category;

		public double Weight => _weight;

		public int? AveragePrice => _averagePrice;

		public RuleSource Source => _source;

		public int? NeededStrength => _neededStrength;

		public int Attack => _attack;

		public int Damage => _damage;

		public string DamageType => _damageType;

		public int Range => _range;

		public IItem NewInstance(int count = 1)
		{
			return new Ammo(Name, Category, Weight, AveragePrice, count, Source, NeededStrength, Attack, Damage, DamageType, Range);
		}

		public IItem NewPriceInstance(int newPrice, int count = 1)
		{
			return new Ammo(Name, Category, Weight, newPrice, count, Source, NeededStrength, Attack, Damage, DamageType, Range);
		}
	}
}
