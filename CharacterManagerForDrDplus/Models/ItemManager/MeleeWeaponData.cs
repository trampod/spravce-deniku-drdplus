﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class MeleeWeaponData : IItemData
	{

		//IItem
		private string _name;
		private string _category;
		private double _weight;
		private int? _averagePrice;
		private RuleSource _source;
		//IWeapon
		private string _weaponCategory;
		private int? _neededStrength;
		private int? _length;
		private int _attack;
		private int _damage;
		private string _damageType;
		private int _block;
		private bool _isTwoHanded;


		public MeleeWeaponData(string name, string category, double weight, int? averagePrice, RuleSource source,
			string weaponCategory, int? neededStrength, int? length, int attack, int damage, string damageType, int block, bool isTwoHanded)
		{
			_name = name;
			_category = category;
			_weight = weight;
			_averagePrice = averagePrice;
			_source = source;

			_weaponCategory = weaponCategory;
			_neededStrength = neededStrength;
			_length = length;
			_attack = attack;
			_damage = damage;
			_damageType = damageType;
			_block = block;
			_isTwoHanded = isTwoHanded;
		}

		//IItem
		public string Name => _name;

		public string Category => _category;

		public double Weight => _weight;

		public int? AveragePrice => _averagePrice;

		public RuleSource Source => _source;


		//IWeapon
		public string WeaponCategory => _weaponCategory;

		public int? NeededStrength => _neededStrength;

		public int? Length => _length;

		public int Attack => _attack;

		public int Damage => _damage;

		public string DamageType => _damageType;

		public int Block => _block;

		public bool IsTwoHanded => _isTwoHanded;

		public IItem NewInstance(int count = 1)
		{
			return new MeleeWeapon(Name, Category, Weight, AveragePrice, count, Source, Guid.NewGuid(), WeaponCategory, NeededStrength, Length, Attack, Damage, DamageType, Block, IsTwoHanded);
		}

		public IItem NewPriceInstance(int newPrice, int count = 1)
		{
			return new MeleeWeapon(Name, Category, Weight, newPrice, count, Source, Guid.NewGuid(), WeaponCategory, NeededStrength, Length, Attack, Damage, DamageType, Block, IsTwoHanded);
		}
	}
}
