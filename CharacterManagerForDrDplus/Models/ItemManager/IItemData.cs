﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public interface IItemData
	{
		public string Name { get; }
		public string Category { get; }
		public double Weight { get; }
		public int? AveragePrice { get; }
		public RuleSource Source { get; }

		public IItem NewInstance(int count = 1);
		public IItem NewPriceInstance(int newPrice, int count = 1);
	}
}
