﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class ArmorData : IItemData
	{
		private string _name;
		private string _category;
		private double _weight;
		private int? _averagePrice;
		private RuleSource _source;

		private int? _neededStrength;
		private int _restriction;
		private int _protection;
		private bool _isHelmet;

		public ArmorData(string name, string category, double weight, int? averagePrice, RuleSource source,
			int? neededStrength, int restriction, int protection, bool isHelmet)
		{
			_name = name;
			_category = category;
			_weight = weight;
			_averagePrice = averagePrice;
			_source = source;

			_neededStrength = neededStrength;
			_restriction = restriction;
			_protection = protection;
			_isHelmet = isHelmet;
		}

		public string Name => _name;

		public string Category => _category;

		public double Weight => _weight;

		public int? AveragePrice => _averagePrice;

		public RuleSource Source => _source;

		public int? NeededStrength => _neededStrength;

		public int Restriction => _restriction;

		public int Protection => _protection;

		public bool IsHelmet => _isHelmet;

		public IItem NewInstance(int count = 1)
		{
			return new Armor(Name, Category, Weight, AveragePrice, count, Source, NeededStrength, Restriction, Protection, IsHelmet);
		}

		public IItem NewPriceInstance(int newPrice, int count = 1)
		{
			return new Armor(Name, Category, Weight, newPrice, count, Source, NeededStrength, Restriction, Protection, IsHelmet);
		}
	}
}
