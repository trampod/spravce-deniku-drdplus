﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class EquipmentItemData : IItemData
	{
		//IItem
		private string _name;
		private string _category;
		private double _weight;
		private int? _averagePrice;
		private RuleSource _source;

		public EquipmentItemData(string name, string category, double weight, int? averagePrice, RuleSource source)
		{
			_name = name;
			_category = category;
			_weight = weight;
			_averagePrice = averagePrice;
			_source = source;
		}

		public string Name => _name;

		public string Category => _category;

		public double Weight => _weight;

		public int? AveragePrice => _averagePrice;

		public RuleSource Source => _source;

		public IItem NewInstance(int count = 1)
		{
			return new EquipmentItem(_name, _category, _weight, _averagePrice, count, _source);
		}

		public IItem NewPriceInstance(int newPrice, int count = 1)
		{
			return new EquipmentItem(_name, _category, _weight, newPrice, count, _source);
		}
	}
}
