﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class ItemManager
	{
		private static ItemManager? instance;

		public static ItemManager GetInstance()
		{
			if (instance == null) //instance = new ItemManager();
				instance = DataLoader.LoadItemManagerData();
			return instance;
		}

		private IList<MeleeWeaponData> _meleeWeapons;
		private IList<RangedWeaponData> _rangedWeapons;
		private IList<ShieldData> _shields;
		private IList<AmmoData> _amunition;
		private IList<ArmorData> _armors;
		private Dictionary<string, IList<EquipmentItemData>> _itemCategories;

		public IList<MeleeWeaponData> MeleeWeapons => _meleeWeapons;
		public IList<RangedWeaponData> RangedWeapons => _rangedWeapons;
		public IList<ShieldData> Shields => _shields;
		public IList<AmmoData> Amunition => _amunition;
		public IList<ArmorData> Armors => _armors;
		public Dictionary<string, IList<EquipmentItemData>> ItemCategories => _itemCategories;

		public ItemManager(IList<MeleeWeaponData> meleeWeapons,
			IList<RangedWeaponData> rangedWeapons,
			IList<ShieldData> shields,
			IList<AmmoData> amunition,
			IList<ArmorData> armors,
			Dictionary<string,IList<EquipmentItemData>> itemCategories)
		{
			_meleeWeapons = meleeWeapons;//.OrderBy(x => x.Name).ToList();
			_rangedWeapons = rangedWeapons;//.OrderBy(x => x.Name).ToList();
			_shields = shields;//.OrderBy(x => x.Name).ToList();
			_amunition = amunition;//.OrderBy(x => x.Name).ToList();
			_armors = armors;//.OrderBy(x => x.Name).ToList();
			_itemCategories = itemCategories;
			foreach(string key in _itemCategories.Keys)
			{
				_itemCategories[key] = _itemCategories[key].OrderBy(x => x.Name).ToList();
			}
		}

		public ItemDataCategory GetCategory(string categoryName)
		{
			IList<IItemData> ll;
			switch (categoryName)
			{
				case "Zbraně na blízko":
					ll = new List<IItemData>(_meleeWeapons);
					break;
				case "Zbraně na dálku":
					ll = new List<IItemData>(_rangedWeapons);
					break;
				case "Štíty":
					ll = new List<IItemData>(_shields);
					break;
				case "Munice":
					ll = new List<IItemData>(_amunition);
					break;
				case "Brnění":
					ll = new List<IItemData>(_armors);
					break;
				default:
					if (_itemCategories.ContainsKey(categoryName))
						ll = new List<IItemData>(_itemCategories[categoryName]);
					else
						throw new ArgumentException();
					break;
			}
			return new ItemDataCategory(categoryName, ll);
		}

		public List<ItemDataCategory> GetAllCategories()
		{
			List<ItemDataCategory> lic = new List<ItemDataCategory>();
			lic.Add(new ItemDataCategory("Zbraně na blízko", new List<IItemData>(_meleeWeapons)));
			lic.Add(new ItemDataCategory("Zbraně na dálku", new List<IItemData>(_rangedWeapons)));
			lic.Add(new ItemDataCategory("Štíty", new List<IItemData>(_shields)));
			lic.Add(new ItemDataCategory("Munice", new List<IItemData>(_amunition)));
			lic.Add(new ItemDataCategory("Brnění", new List<IItemData>(_armors)));

			foreach (string item in _itemCategories.Keys)
			{
				lic.Add(new ItemDataCategory(item, new List<IItemData>(_itemCategories[item])));
			}

			return lic.OrderBy(x => x.CategoryName).ToList();
		}

		public IList<string> GetCategoryNames()
		{
			IList<string> ll = new List<string>() { "Zbraně na blízko", "Zbraně na dálku", "Štíty", "Munice", "Brnění" };
			foreach (string item in _itemCategories.Keys)
				ll.Add(item);
			return ll;
		}

		public IList<string> GetEquipmentCategoryNames()
		{
			IList<string> ll = new List<string>();
			foreach (string item in _itemCategories.Keys)
				ll.Add(item);
			return ll;
		}

		public IList<EquipmentItemData> GetEquipmentList(string categoryName)
		{
			if (_itemCategories.ContainsKey(categoryName))
				return _itemCategories[categoryName];
			else
				throw new ArgumentException();
		}

		public IList<MeleeWeaponData> GetMeleeWeaponsList()
		{
			return _meleeWeapons;
		}

		public IList<RangedWeaponData> GetRangedWeaponsList()
		{
			return _rangedWeapons;
		}

		public IList<ShieldData> GetShieldsList()
		{
			return _shields;
		}

		public IList<AmmoData> GetAmunitionList()
		{
			return _amunition;
		}

		public IList<ArmorData> GetArmorsList()
		{
			return _armors;
		}
	}
}
