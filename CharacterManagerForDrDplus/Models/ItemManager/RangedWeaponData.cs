﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class RangedWeaponData : IItemData
	{

		//IItem
		private string _name;
		private string _category;
		private double _weight;
		private int? _averagePrice;
		private RuleSource _source;
		//IWeapon
		private string _weaponCategory;
		private int _neededStrength;
		private int _attack;
		private int _damage;
		private string _damageType;
		private int _block;
		private int? _maxStrength;
		private int? _range;
		private bool _isTwoHanded;



		public RangedWeaponData(string name, string category, double weight, int? averagePrice, RuleSource source,
			string weaponCategory, int neededStrength, int attack, int damage, string damageType, int block, int? maxStrength, int? range, bool isTwoHanded)
		{
			_name = name;
			_category = category;
			_weight = weight;
			_averagePrice = averagePrice;
			_source = source;

			_weaponCategory = weaponCategory;
			_neededStrength = neededStrength;
			_attack = attack;
			_damage = damage;
			_damageType = damageType;
			_block = block;
			_maxStrength = maxStrength;
			_range = range;
			_isTwoHanded = isTwoHanded;
		}

		public string Name => _name;

		public string Category => _category;

		public double Weight => _weight;

		public int? AveragePrice => _averagePrice;

		public RuleSource Source => _source;

		public string WeaponCategory => _weaponCategory;

		public int NeededStrength => _neededStrength;

		public int Attack => _attack;

		public int Damage => _damage;

		public string DamageType => _damageType;

		public int Block => _block;

		public int? MaxStrength => _maxStrength;

		public int? Range => _range;

		public bool IsTwoHanded => _isTwoHanded;

		public IItem NewInstance(int count = 1)
		{
			return new RangedWeapon(Name, Category, Weight, AveragePrice, count, Source, Guid.NewGuid(),
				WeaponCategory, NeededStrength, Attack, Damage, DamageType, Block, MaxStrength, Range, IsTwoHanded);
		}

		public IItem NewPriceInstance(int newPrice, int count = 1)
		{
			return new RangedWeapon(Name, Category, Weight, newPrice, count, Source, Guid.NewGuid(),
				WeaponCategory, NeededStrength, Attack, Damage, DamageType, Block, MaxStrength, Range, IsTwoHanded);
		}
	}
}
