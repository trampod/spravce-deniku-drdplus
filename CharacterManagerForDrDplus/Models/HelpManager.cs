﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class HelpManager
	{
		private static HelpManager? _instance;

		public static HelpManager GetInstance()
		{
			if (_instance == null) _instance = DataLoader.LoadHelp();
			return _instance;
		}

		private IList<Information> _informations;
		public IList<Information> Informations => _informations;

		public HelpManager(IList<Information> informations)
		{
			_informations = informations;
		}
	}
}
