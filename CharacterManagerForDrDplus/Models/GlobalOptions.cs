﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class GlobalOptions
	{
		private static GlobalOptions? _instance;
		public static GlobalOptions Instance
		{
			get
			{
				if (_instance == null)
					_instance = new GlobalOptions();
				return _instance;
			}
		}

		private Dictionary<string, string> SourceNames;

		private GlobalOptions()
		{
			SourceNames = new Dictionary<string, string>()
			{
				{"pph", "Pravidla pro hráče" },
				{"ppj", "Pravidla pro pána jezkyně" },
				{"boj", "Pravidla pro bojovníka" },
				{"čar", "Pravidla pro čaroděje" },
				{"zlo", "Pravidla pro zloděje" },
				{"the", "Pravidla pro theurga" },
				{"hra", "Pravidla pro hraničáře" },
				{"kně", "Pravidla pro kněze" },
				{"bes", "Bestiář" },
				{"ast", "Asterion - Čas Temna (Hlavní modul)" }
			};


		}

		public string GetSourceName(string sourceCode)
		{
			return SourceNames[sourceCode];
		}
	}
}
