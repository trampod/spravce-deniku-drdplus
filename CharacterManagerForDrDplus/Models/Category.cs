﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class Category<TType> : INotifyPropertyChanged
	{
		public string CategoryName { get; }

		public ObservableCollection<TType> Items { get; }

		public Category(string categoryName, IList<TType> items)
		{
			CategoryName = categoryName;
			Items = new ObservableCollection<TType>(items);
		}


		public event PropertyChangedEventHandler? PropertyChanged;

		public void NotifyPropetyChanged(string property)
		{
			if (this.PropertyChanged != null)
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
		}
	}
}
