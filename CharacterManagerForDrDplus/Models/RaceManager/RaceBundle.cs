﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class RaceBundle
	{
		public string BundleName { get; }
		public IList<Race> RaceList { get; }

		[JsonConstructor]
		public RaceBundle(string bundleName, IList<Race> raceList)
		{
			if (raceList.Count == 0) throw new ArgumentException();
			BundleName = bundleName;
			RaceList = raceList;
		}

		public RaceBundle(string bundleName, params Race[] races)
		{
			if (races.Length == 0) throw new ArgumentException();
			RaceList = new List<Race>(races);
			BundleName = bundleName;
		}
	}
}
