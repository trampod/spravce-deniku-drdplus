﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class RaceManager
	{
		private static RaceManager? instance;

		private IList<RaceBundle> _raceBundles;

		public IList<RaceBundle> RaceBundles => _raceBundles;

		public static RaceManager GetInstance()
		{
			if (instance == null) instance = DataLoader.LoadRaceManagerData();
			return instance;
		}

		public RaceManager(IList<RaceBundle> raceBundles)
		{
			_raceBundles = raceBundles;
			/*
			_raceBundles = new List<RaceBundle>();
			_raceBundles.Add(new RaceBundle("Člověk",
				new Race("Člověk", "Lidská žena",
				new Dictionary<AttributeCode, int>(),
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Chr, 1 } },
				new List<string>(), "", 180, 80, 0),
				new Race("Horal", "Lidská žena",
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, 1 }, { AttributeCode.Vol, 1 }, { AttributeCode.Int, -1 }, { AttributeCode.Chr, -1 } },
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Chr, 1 } },
				new List<string>(), "", 180, 80, 0)));

			_raceBundles.Add(new RaceBundle("Elf",
				new Race("Elf", "Elfka",
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Obr, 1 }, { AttributeCode.Zrc, 1 }, { AttributeCode.Vol, -2 }, { AttributeCode.Int, 1 }, { AttributeCode.Chr, 1 }, { AttributeCode.Odl, -1 } },
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Zrc, 1 }, { AttributeCode.Int, -1 }, { AttributeCode.Chr, 1 } },
				new List<string>(), "Zrak", 160, 50, -1),
				new Race("Zelený elf", "Elfka",
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Obr, 1 }, { AttributeCode.Zrc, 0 }, { AttributeCode.Vol, -1 }, { AttributeCode.Int, 1 }, { AttributeCode.Chr, 1 }, { AttributeCode.Odl, -1 } },
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Zrc, 1 }, { AttributeCode.Int, -1 }, { AttributeCode.Chr, 1 } },
				new List<string>(), "Zrak", 160, 50, -1)));

			_raceBundles.Add(new RaceBundle("Trpaslík",
				new Race("Trpaslík", "Trpaslice",
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, 1 }, { AttributeCode.Obr, -1 }, { AttributeCode.Vol, 2 }, { AttributeCode.Int, -1 }, { AttributeCode.Chr, -2 }, { AttributeCode.Odl, 1 }, { AttributeCode.Sms, -1 } },
				new Dictionary<AttributeCode, int>() { { AttributeCode.Zrc, -1 }, { AttributeCode.Int, 1 } },
				new List<string>() { "infravidění" }, "Hmat", 140, 70, 0),
				new Race("Lesní trpaslík", "Trpaslice",
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, 1 }, { AttributeCode.Obr, -1 }, { AttributeCode.Vol, 1 }, { AttributeCode.Int, -1 }, { AttributeCode.Chr, -1 }, { AttributeCode.Odl, 1 }, { AttributeCode.Sms, -1 } },
				new Dictionary<AttributeCode, int>() { { AttributeCode.Zrc, -1 }, { AttributeCode.Int, 1 } },
				new List<string>() { "infravidění" }, "Hmat", 140, 70, 0),
				new Race("Horský trpaslík", "Trpaslice",
				new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, 2 }, { AttributeCode.Obr, -1 }, { AttributeCode.Vol, 2 }, { AttributeCode.Int, -2 }, { AttributeCode.Chr, -2 }, { AttributeCode.Odl, 1 }, { AttributeCode.Sms, -1 } },
				new Dictionary<AttributeCode, int>() { { AttributeCode.Zrc, -1 }, { AttributeCode.Int, 1 } },
				new List<string>() { "infravidění" }, "Hmat", 140, 70, 0)));
			*/
		}

		public IList<Race> GetAllRaces()
		{
			IList<Race> list = new List<Race>();
			foreach (RaceBundle item in _raceBundles)
			{
				list = list.Concat(item.RaceList).ToList();
			}
			return list;
		}
	}
}
