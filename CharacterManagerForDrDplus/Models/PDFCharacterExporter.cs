﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using CharacterManagerForDrDplus.Services;

namespace CharacterManagerForDrDplus.Models
{
	public class PDFCharacterExporter
	{
		private Character? _character;
		private PdfDocument? _document;
		private XFont _font;
		private XFont _fontBonus;
		private XFont _fontTitle;
		private const int PADDING = 40;
		private const int PADDING_TOP = 60;
		private const int LINEHEIGHT = 30;
		private const int LABEL_WIDTH = 130;

		public PDFCharacterExporter()
		{
			System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
			_font = new XFont("Arial", 18);
			_fontBonus = new XFont("Arial", 18, XFontStyle.Bold);
			_fontTitle = new XFont("Arial", 25, XFontStyle.Bold);
		}

		public bool ExportCharacter(Character character, string saveLocation)
		{

			_document = new PdfDocument();
			_character = character;

			PdfPage page = _document.AddPage();

			XGraphics gfx = XGraphics.FromPdfPage(page);


			//gfx.DrawString("first line", font, XBrushes.Black,
			//	new XRect(0, 0, page.Width, page.Height),
			//	XStringFormats.Center);

			//gfx.DrawString("second line", font, XBrushes.Violet,
			//	new XRect(0, 0, page.Width, page.Height),
			//	XStringFormats.BottomLeft);

			//gfx.DrawString("third line", font, XBrushes.Black,
			//	new XPoint(100, 300));

			int pageWidth = (int)page.Width.Point;
			int pageHeight = (int)page.Height.Point;
				
			int currentHeight = DrawGeneral(ref gfx, pageWidth, pageHeight);
			currentHeight = DrawAttributes(ref gfx, currentHeight, pageWidth, pageHeight);
			currentHeight = DrawCombatInfo(ref gfx, currentHeight, pageWidth, pageHeight);
			currentHeight = DrawBackground(ref gfx, currentHeight, pageWidth, pageHeight);

			page = _document.AddPage();
			gfx = XGraphics.FromPdfPage(page);
			currentHeight = PADDING_TOP;

			foreach(WeaponCombinationBase item in _character.GetWeaponCombinations())
			{
				if(currentHeight + LINEHEIGHT * 4 > pageHeight - PADDING_TOP)
				{
					page = _document.AddPage();
					gfx = XGraphics.FromPdfPage(page);
					currentHeight = PADDING_TOP;
				}

				currentHeight = DrawCombination(ref gfx, item, currentHeight, pageWidth, pageHeight);
			}

			//page = _document.AddPage();
			//gfx = XGraphics.FromPdfPage(page);


			DrawInventory();

			DrawSkills();

			DrawProfession();

			_document.Save(saveLocation);
			return true;
		}

		public string Bonus(int? bonus)
		{
			if (bonus == null)
				return "-";
			if (bonus < 0)
				return bonus.ToString();
			return "+" + bonus;
		}

		private int DrawGeneral(ref XGraphics gfx, int pageWidth, int pageHeight)
		{
			gfx.DrawString("Jméno:", _font, XBrushes.Black,
		 		new XPoint(PADDING, PADDING_TOP));
			gfx.DrawString(_character!.Name, _fontBonus, XBrushes.Black,
		 		new XPoint(PADDING+LABEL_WIDTH, PADDING_TOP));


			gfx.DrawString("Povolání:", _font, XBrushes.Black,
		 		new XPoint(PADDING, PADDING_TOP + LINEHEIGHT));
			gfx.DrawString(_character.Profession.Name, _fontBonus, XBrushes.Black,
		 		new XPoint(PADDING+LABEL_WIDTH, PADDING_TOP + LINEHEIGHT));

			gfx.DrawString("Úroveň:", _font, XBrushes.Black,
		 		new XPoint(pageWidth / 2, PADDING_TOP + LINEHEIGHT));
			gfx.DrawString(_character.Level.ToString(), _fontBonus, XBrushes.Black,
		 		new XPoint((pageWidth / 2)+LABEL_WIDTH, PADDING_TOP + LINEHEIGHT));


			//gfx.DrawString("Rasa:", _font, XBrushes.Black,
			//		new XPoint(PADDING, PADDING_TOP + LINEHEIGHT*2));
			//gfx.DrawString(_character.Race.Name + (_character.Female?(" ("+_character.Race.FemaleName+")"):""), _fontBonus, XBrushes.Black,
			//		new XPoint(PADDING+LABEL_WIDTH, PADDING_TOP + LINEHEIGHT*2));
			DrawBonus(ref gfx, "Rasa:", _character.Race.Name, PADDING, PADDING_TOP + LINEHEIGHT * 2);


			DrawBonus(ref gfx, "Pohlaví:", _character.Female?"Žena":"Muž", PADDING, PADDING_TOP + LINEHEIGHT * 3);
			DrawBonus(ref gfx, "Velikost:", Bonus(_character.Size), pageWidth/2, PADDING_TOP + LINEHEIGHT * 3);
			//gfx.DrawString("Velikost:", _font, XBrushes.Black,
		 //		new XPoint(PADDING, PADDING_TOP + LINEHEIGHT * 3));
			//gfx.DrawString(Bonus(_character.Size), _fontBonus, XBrushes.Black,
		 //		new XPoint(PADDING + LABEL_WIDTH, PADDING_TOP + LINEHEIGHT * 3));


			DrawBonus(ref gfx, "Výška:", _character.Height + " (" + Bonus(_character.HeightBonus) + ")",
				PADDING, PADDING_TOP + LINEHEIGHT * 4);

			DrawBonus(ref gfx, "Váha:", _character.Weight + " (" + Bonus(_character.WeightBonus) + ")",
				pageWidth / 2, PADDING_TOP + LINEHEIGHT * 4);



			return PADDING_TOP + LINEHEIGHT * 5;
		}

		private int DrawAttributes(ref XGraphics gfx, int start, int pageWidth, int pageHeight)
		{
			start += LINEHEIGHT * 1;

			//gfx.DrawString("Síla:", _font, XBrushes.Black,
			//		new XPoint(PADDING, start + LINEHEIGHT * 0));
			//gfx.DrawString(Bonus(_character.GetAttribute(Services.AttributeCode.Sil)), _fontBonus, XBrushes.Black,
			//		new XPoint(PADDING+LABEL_WIDTH, start + LINEHEIGHT * 0));

			DrawBonus(ref gfx, "Síla:", Bonus(_character!.GetAttribute(Services.AttributeCode.Sil)),
				PADDING, start + LINEHEIGHT * 0);

			DrawBonus(ref gfx, "Obratnost:", Bonus(_character.GetAttribute(Services.AttributeCode.Obr)),
				PADDING, start + LINEHEIGHT * 1);

			DrawBonus(ref gfx, "Zručnost:", Bonus(_character.GetAttribute(Services.AttributeCode.Zrc)),
				PADDING, start + LINEHEIGHT * 2);

			DrawBonus(ref gfx, "Vůle:", Bonus(_character.GetAttribute(Services.AttributeCode.Vol)),
				PADDING, start + LINEHEIGHT * 3);

			DrawBonus(ref gfx, "Inteligence:", Bonus(_character.GetAttribute(Services.AttributeCode.Int)),
				PADDING, start + LINEHEIGHT * 4);

			DrawBonus(ref gfx, "Charisma:", Bonus(_character.GetAttribute(Services.AttributeCode.Chr)),
				PADDING, start + LINEHEIGHT * 5);


			DrawBonus(ref gfx, "Odolnost:", Bonus(_character.GetAttribute(Services.AttributeCode.Odl)),
				pageWidth / 2, start + LINEHEIGHT * 0);

			DrawBonus(ref gfx, "Výdrž:", Bonus(_character.GetAttribute(Services.AttributeCode.Vdr)),
				pageWidth / 2, start + LINEHEIGHT * 1);

			DrawBonus(ref gfx, "Rychlost:", Bonus(_character.GetAttribute(Services.AttributeCode.Rch)),
				pageWidth / 2, start + LINEHEIGHT * 2);

			DrawBonus(ref gfx, "Smysly:", Bonus(_character.GetAttribute(Services.AttributeCode.Sms)),
				pageWidth / 2, start + LINEHEIGHT * 3);


			DrawBonus(ref gfx, "Krása:", Bonus(_character.Beauty),
				pageWidth / 2, start + LINEHEIGHT * 5);

			DrawBonus(ref gfx, "Nebezpečnost:", Bonus(_character.Dangerousnes),
				pageWidth / 2, start + LINEHEIGHT * 6);

			DrawBonus(ref gfx, "Důstojnost:", Bonus(_character.Dignity),
				pageWidth / 2, start + LINEHEIGHT * 7);

			return start + LINEHEIGHT * 8;
		}

		private int DrawCombatInfo(ref XGraphics gfx, int start, int pageWidth, int pageHeight)
		{
			start += LINEHEIGHT;

			DrawBonus(ref gfx, "Boj:", Bonus(_character!.Combat),
				PADDING, start + LINEHEIGHT * 0);
			DrawBonus(ref gfx, "Útok:", Bonus(_character!.Attack),
				pageWidth / 2, start + LINEHEIGHT * 0);

			DrawBonus(ref gfx, "Obrana:", Bonus(_character!.Defense),
				PADDING, start + LINEHEIGHT * 1);
			DrawBonus(ref gfx, "Střelba:", Bonus(_character!.Ranged),
				pageWidth / 2, start + LINEHEIGHT * 1);

			DrawBonus(ref gfx, "Mez zranění:", Bonus(_character!.WoundLimit)
				+ " (" + LogTables.DamageFromBonus(_character.WoundLimit) + " bodů)",
				PADDING, start + LINEHEIGHT * 2);

			DrawBonus(ref gfx, "Mez únavy:", Bonus(_character!.FatigueLimit)
				+ " (" + LogTables.DamageFromBonus(_character.FatigueLimit) + " bodů)",
				PADDING, start + LINEHEIGHT * 3);


			return start + LINEHEIGHT * 4;
		}

		private int DrawBackground(ref XGraphics gfx, int start, int pageWidth, int pageHeight)
		{
			start += LINEHEIGHT;

			string exceptionality = _character!.Exceptionality switch
			{
				ExceptionalityType.Vlastnosti => "Vlastnosti",
				ExceptionalityType.Kombinovane => "Kombinované",
				ExceptionalityType.Zazemi => "Zázemí",
				_ => ""
			};
			


			DrawBonus(ref gfx, "Vyjímečnost:",exceptionality,
				PADDING, start + LINEHEIGHT * 0);

			DrawBonus(ref gfx, "Původ:", _character.Origin,
				PADDING, start + LINEHEIGHT * 1);

			DrawBonus(ref gfx, "Počát. peníze:", RuleTables.GetPresentableMoney( _character.StartMoneyValue),
				PADDING, start + LINEHEIGHT * 2);

			RuleTables.GetStartSkills(_character.StartSkillGrade, _character.Profession,
				out int p, out int m, out int c);

			DrawBonus(ref gfx, "Dovednosti:", "Fyz.: " + p + "   Psy.: " + m +"   Kom.: " + c,
				PADDING, start + LINEHEIGHT * 3);


			return start + LINEHEIGHT * 4;
		}

		private int DrawCombination(ref XGraphics gfx, WeaponCombinationBase combination, int start, int pageWidth, int pageHeight)
		{
			combination.Recalculate(_character!);
			switch (combination)
			{
				case SingleMeleeWeaponCombination:
					return DrawSingleMeleeWeaponCombination(ref gfx, (SingleMeleeWeaponCombination)combination, start, pageWidth, pageHeight);
				case SingleRangedWeaponCombination:
					return DrawSingleRangedWeaponCombination(ref gfx, (SingleRangedWeaponCombination)combination, start, pageWidth, pageHeight);
				default:
					return start;
			}
		}

		private int DrawSingleMeleeWeaponCombination(ref XGraphics gfx, SingleMeleeWeaponCombination combination, int start, int pageWidth, int pageHeight)
		{
			DrawBonus(ref gfx, "Kombinace:", combination.CombinationName,
				PADDING, start + LINEHEIGHT * 0);

			DrawBonus(ref gfx, "Dovednost:", combination.FullSkill,
				PADDING, start + LINEHEIGHT * 1);

			DrawBonus(ref gfx, "BČ:", combination.FullCombat,
				PADDING, start + LINEHEIGHT * 2, LABEL_WIDTH/2);
			DrawBonus(ref gfx, "ÚČ:", combination.FullAttack,
				pageWidth / 4, start + LINEHEIGHT * 2, LABEL_WIDTH/2);
			DrawBonus(ref gfx, "OČ (kryt):", combination.FullDefense,
				pageWidth / 2 - PADDING , start + LINEHEIGHT * 2, LABEL_WIDTH - 10);
			DrawBonus(ref gfx, "ZZ:", combination.FullDamage,
				(pageWidth / 4) * 3 , start + LINEHEIGHT * 2, LABEL_WIDTH/2);

			DrawBonus(ref gfx, "Chybějící síla:", combination.PresentableMissingStrength,
				PADDING, start + LINEHEIGHT * 3, LABEL_WIDTH + LABEL_WIDTH/2);

			return start + LINEHEIGHT * 5;
		}

		private int DrawSingleRangedWeaponCombination(ref XGraphics gfx, SingleRangedWeaponCombination combination, int start, int pageWidth, int pageHeight)
		{
			DrawBonus(ref gfx, "Kombinace:", combination.CombinationName,
				PADDING, start + LINEHEIGHT * 0);

			DrawBonus(ref gfx, "Dovednost:", combination.FullSkill,
				PADDING, start + LINEHEIGHT * 1);

			DrawBonus(ref gfx, "BČ:", combination.FullCombat,
				PADDING, start + LINEHEIGHT * 2, LABEL_WIDTH / 2);
			DrawBonus(ref gfx, "ÚČ:", combination.FullAttack,
				pageWidth / 4, start + LINEHEIGHT * 2, LABEL_WIDTH / 2);
			DrawBonus(ref gfx, "OČ (kryt):", combination.FullDefense,
				pageWidth / 2 - PADDING, start + LINEHEIGHT * 2, LABEL_WIDTH - 10);
			DrawBonus(ref gfx, "ZZ:", combination.FullDamage,
				(pageWidth / 4) * 3, start + LINEHEIGHT * 2, LABEL_WIDTH / 2);

			DrawBonus(ref gfx, "Chybějící síla:", combination.PresentableMissingStrength,
				PADDING, start + LINEHEIGHT * 3, LABEL_WIDTH + LABEL_WIDTH/2);
			DrawBonus(ref gfx, "SD:", combination.FullRange,
				(pageWidth / 2)+9, start + LINEHEIGHT * 3, LABEL_WIDTH/2-4);

			return start + LINEHEIGHT * 5;
		}

		private void DrawInventory()
		{
			IList<ItemCategory> items = _character!.GetInventoryCategories();
			
			PdfPage page = _document!.AddPage();
			XGraphics gfx = XGraphics.FromPdfPage(page);
			int currentHeight = PADDING_TOP;

			int pageWidth = (int)page.Width.Point;
			int pageHeight = (int)page.Height.Point;

			gfx.DrawString("Vybavení", _fontTitle, XBrushes.Black,
		 		new XPoint(PADDING, currentHeight));
			currentHeight += LINEHEIGHT + 10;

			DrawBonus(ref gfx, "Peníze:", RuleTables.GetPresentableMoney(_character.Money + _character.StartMoneyValue),
				PADDING, currentHeight);

			currentHeight += LINEHEIGHT;

			foreach(ItemCategory category in items)
			{
				if (currentHeight + LINEHEIGHT * 4 > pageHeight - PADDING_TOP)
				{
					page = _document.AddPage();
					gfx = XGraphics.FromPdfPage(page);
					currentHeight = PADDING_TOP;
				}

				currentHeight += LINEHEIGHT * 1;
				gfx.DrawString(category.CategoryName, _fontBonus, XBrushes.Black,
		 			new XPoint(PADDING, currentHeight));
				currentHeight += LINEHEIGHT * 2;

				foreach (IItem item in category.Items)
				{
					if (currentHeight + LINEHEIGHT * GetItemLines(item) > pageHeight - PADDING_TOP)
					{
						page = _document.AddPage();
						gfx = XGraphics.FromPdfPage(page);
						currentHeight = PADDING_TOP;
					}

					currentHeight = DrawItem(ref gfx, item, currentHeight, pageWidth, pageHeight);
				}
			}



		}

		private int GetItemLines(IItem item)
		{
			switch (item)
			{
				case MeleeWeapon:
					return 6;
				case RangedWeapon:
					return 6;
				case Shield:
					return 6;
				default:
					return 3;
			}
		}

		private int DrawItem(ref XGraphics gfx, IItem item, int start, int pageWidth, int pageHeight)
		{
			DrawBonus(ref gfx, "Pŕedmět:", item.Name,
				PADDING, start + LINEHEIGHT * 0);

			DrawBonus(ref gfx, "Cena/ks:", RuleTables.GetPresentableMoney(item.Price),
				PADDING, start + LINEHEIGHT * 1);
			DrawBonus(ref gfx, "Váha:", item.Weight + " kg",
				pageWidth/2, start + LINEHEIGHT * 1);
			DrawBonus(ref gfx, "Počet:", item.Count.ToString(),
				PADDING, start + LINEHEIGHT * 2);
			DrawBonus(ref gfx, "Pravidla:", item.Source.ToString(),
				pageWidth/2, start + LINEHEIGHT * 2);

			start = DrawWeapon(ref gfx, item, start + LINEHEIGHT * 3, pageWidth, pageHeight);

			return start + LINEHEIGHT * 1;
		}

		private int DrawWeapon(ref XGraphics gfx, IItem item, int start, int pageWidth, int pageHeight)
		{
			switch (item)
			{
				case MeleeWeapon:
					return DrawMeleeWeapon(ref gfx, (MeleeWeapon)item, start, pageWidth, pageHeight);
				case RangedWeapon:
					return DrawRangedWeapon(ref gfx, (RangedWeapon)item, start, pageWidth, pageHeight);
				default:
					return start;
			}
		}

		private int DrawMeleeWeapon(ref XGraphics gfx, MeleeWeapon item, int start, int pageWidth, int pageHeight)
		{
			DrawBonus(ref gfx, "Potřebná síla:", Bonus(item.NeededStrength),
				PADDING, start + LINEHEIGHT * 0);
			DrawBonus(ref gfx, "Délka:", item.Length.ToString()!,
				pageWidth / 2, start + LINEHEIGHT * 0);

			DrawBonus(ref gfx, "Útočnost:", Bonus(item.Attack),
				PADDING, start + LINEHEIGHT * 1);
			DrawBonus(ref gfx, "Kryt:", item.Block.ToString(),
				pageWidth / 2, start + LINEHEIGHT * 1);

			DrawBonus(ref gfx, "Zranění:", Bonus(item.Damage),
				PADDING, start + LINEHEIGHT * 2);
			DrawBonus(ref gfx, "Typ:", item.DamageType,
				pageWidth / 2, start + LINEHEIGHT * 2);

			return start + LINEHEIGHT * 3;
		}

		private int DrawRangedWeapon(ref XGraphics gfx, RangedWeapon item, int start, int pageWidth, int pageHeight)
		{
			DrawBonus(ref gfx, "Potřebná síla:", Bonus(item.NeededStrength) + (item.MaxStrength==null?"":(Bonus(item.MaxStrength))),
				PADDING, start + LINEHEIGHT * 0);
			DrawBonus(ref gfx, "Dostřel:", Bonus(item.Range),
				pageWidth / 2, start + LINEHEIGHT * 0);

			DrawBonus(ref gfx, "Útočnost:", Bonus(item.Attack),
				PADDING, start + LINEHEIGHT * 1);
			DrawBonus(ref gfx, "Kryt:", item.Block.ToString(),
				pageWidth / 2, start + LINEHEIGHT * 1);

			DrawBonus(ref gfx, "Zranění:", Bonus(item.Damage),
				PADDING, start + LINEHEIGHT * 2);
			DrawBonus(ref gfx, "Typ:", item.DamageType,
				pageWidth / 2, start + LINEHEIGHT * 2);

			return start + LINEHEIGHT * 3;
		}

		private void DrawSkills()
		{
			IList<Skill> skills = _character!.Skills;

			PdfPage page = _document!.AddPage();
			XGraphics gfx = XGraphics.FromPdfPage(page);
			int currentHeight = PADDING_TOP;

			int pageWidth = (int)page.Width.Point;
			int pageHeight = (int)page.Height.Point;

			gfx.DrawString("Dovednosti", _fontTitle, XBrushes.Black,
		 		new XPoint(PADDING, currentHeight));
			currentHeight += LINEHEIGHT + 10;

			//DrawBonus(ref gfx, "Peníze:", RuleTables.GetPresentableMoney(_character.Money + _character.StartMoneyValue),
			//	PADDING, currentHeight);

			//currentHeight += LINEHEIGHT;

			foreach (Skill skill in skills)
			{
				if (currentHeight + LINEHEIGHT * 2 > pageHeight - PADDING_TOP)
				{
					page = _document.AddPage();
					gfx = XGraphics.FromPdfPage(page);
					currentHeight = PADDING_TOP;
				}

				currentHeight = DrawSkill(ref gfx, skill, currentHeight, pageWidth, pageHeight);
			}
		}

		private int DrawSkill(ref XGraphics gfx, Skill skill, int start, int pageWidth, int pageHeight)
		{
			DrawBonus(ref gfx, "Dovednost:", skill.Name,
				PADDING, start + LINEHEIGHT * 0);

			string type = skill.Type switch
			{
				SkillType.Fyzicka => "Fyzická",
				SkillType.Psychicka => "Psychická",
				SkillType.Kombinovana => "Kombinovaná",
				_ => "neznámá"
			};

			DrawBonus(ref gfx, "Typ:", type,
				PADDING, start + LINEHEIGHT * 1,LABEL_WIDTH/3);
			DrawBonus(ref gfx, "Stupeň:", skill.Rating.ToString(),
				pageWidth/2-PADDING, start + LINEHEIGHT * 1,LABEL_WIDTH/2);
			DrawBonus(ref gfx, "Pravidla:", skill.Source.ToString(),
				(pageWidth / 3)*2, start + LINEHEIGHT * 1,(LABEL_WIDTH/3)*2);

			return start + LINEHEIGHT * 3;
		}

		private void DrawBonus(ref XGraphics gfx, string name, string value, int x, int y, int labelWidth = LABEL_WIDTH)
		{
			gfx.DrawString(name, _font, XBrushes.Black,
		 		new XPoint(x, y));
			gfx.DrawString(value, _fontBonus, XBrushes.Black,
		 		new XPoint(x+labelWidth, y));
		}

		private void DrawProfession()
		{
			Profession profession = _character!.Profession;
			switch (profession)
			{
				case Thief:
					DrawThief((Thief)profession);
					break;
				default:
					break;
			}
		}

		private void DrawThief(Thief thief)
		{
			List<ThiefSkill> skills = thief.ThiefSkills;
			List<ThiefManeuver> maneuvers = thief.ThiefManeuvers;

			PdfPage page = _document!.AddPage();
			XGraphics gfx = XGraphics.FromPdfPage(page);
			int currentHeight = PADDING_TOP;

			int pageWidth = (int)page.Width.Point;
			int pageHeight = (int)page.Height.Point;

			gfx.DrawString("Zloděj", _fontTitle, XBrushes.Black,
		 		new XPoint(PADDING, currentHeight));
			currentHeight += LINEHEIGHT + 10;


			//profibody
			DrawBonus(ref gfx, "Profibody:", thief.Profipoints + "/" + (8 + 6 * _character!.Level),
				PADDING, currentHeight);

			if (!string.IsNullOrWhiteSpace(thief.GuildName) || !string.IsNullOrWhiteSpace(thief.GuildMentor))
			{
				currentHeight += LINEHEIGHT * 1;
				DrawBonus(ref gfx, "Cech:", thief.GuildName,
					PADDING, currentHeight);
				currentHeight += LINEHEIGHT * 1;
				DrawBonus(ref gfx, "Mentor:", thief.GuildMentor,
					PADDING, currentHeight);
				currentHeight += LINEHEIGHT * 1;
				DrawBonus(ref gfx, "Pozice:", thief.GuildPosition,
					PADDING, currentHeight);
			}

			currentHeight += LINEHEIGHT * 2;

			gfx.DrawString("Dovednosti", _fontBonus, XBrushes.Black,
		 		new XPoint(PADDING, currentHeight));

			currentHeight += LINEHEIGHT * 2;

			//dovednosti
			foreach (ThiefSkill skill in skills)
			{
				if (currentHeight + LINEHEIGHT * 3 > pageHeight - PADDING_TOP)
				{
					page = _document.AddPage();
					gfx = XGraphics.FromPdfPage(page);
					currentHeight = PADDING_TOP;
				}

				currentHeight = DrawThiefSkill(ref gfx, skill, currentHeight, pageWidth);
			}


			if (currentHeight + LINEHEIGHT * 5 > pageHeight - PADDING_TOP)
			{
				page = _document.AddPage();
				gfx = XGraphics.FromPdfPage(page);
				currentHeight = PADDING_TOP;
			}

			gfx.DrawString("Finty", _fontBonus, XBrushes.Black,
		 		new XPoint(PADDING, currentHeight));

			currentHeight += LINEHEIGHT * 2;

			//finty
			foreach (ThiefManeuver maneuver in maneuvers)
			{
				if (currentHeight + LINEHEIGHT * 3 > pageHeight - PADDING_TOP)
				{
					page = _document.AddPage();
					gfx = XGraphics.FromPdfPage(page);
					currentHeight = PADDING_TOP;
				}

				currentHeight = DrawThiefManeuver(ref gfx, maneuver, currentHeight, pageWidth);
			}
		}

		private int DrawThiefSkill(ref XGraphics gfx, ThiefSkill skill, int start, int pageWidth)
		{
			DrawBonus(ref gfx, "Dovednost:", skill.Name,
				PADDING, start + LINEHEIGHT * 0);

			DrawBonus(ref gfx, "Kategorie:", skill.Category,
				PADDING, start + LINEHEIGHT * 1);
			if(skill.Rating == 3 && skill.Mastered)
			DrawBonus(ref gfx, "Bonus mistra:", skill.MasterBonus,
				pageWidth / 2, start + LINEHEIGHT * 1);

			DrawBonus(ref gfx, "Stupeň:", skill.Rating + ((skill.Rating==3&&skill.Mastered)?(" + M"):""),
				PADDING, start + LINEHEIGHT * 2);
			DrawBonus(ref gfx, "Pravidla:", skill.Source.ToString(),
				pageWidth / 2, start + LINEHEIGHT * 2, (LABEL_WIDTH / 3) * 2);

			return start + LINEHEIGHT * 4;
		}

		private int DrawThiefManeuver(ref XGraphics gfx, ThiefManeuver maneuver, int start, int pageWidth)
		{
			//název
			//zbraně
			//převaha
			//pravidla

			DrawBonus(ref gfx, "Název:", maneuver.Name,
				PADDING, start + LINEHEIGHT * 0, (LABEL_WIDTH / 3) * 2);

			DrawBonus(ref gfx, "Zbraně:", maneuver.Weapons,
				PADDING, start + LINEHEIGHT * 1, (LABEL_WIDTH / 3) * 2);
			if(maneuver.SpecificAdvantage == "")
			{
				DrawBonus(ref gfx, "Převaha:", Bonus(maneuver.Advantage) + (maneuver.GroupAdvantage?" proti každému protivníkovi":""),
					PADDING, start + LINEHEIGHT * 2, (LABEL_WIDTH / 3) * 2);
			}
			else
			{
				DrawBonus(ref gfx, "Převaha:", maneuver.SpecificAdvantage,
					PADDING, start + LINEHEIGHT * 2, (LABEL_WIDTH / 3) * 2);
			}
			DrawBonus(ref gfx, "Pravidla:", maneuver.Source.ToString(),
				(pageWidth / 3) * 2, start + LINEHEIGHT * 2, (LABEL_WIDTH / 3) * 2);

			return start + LINEHEIGHT * 4;
		}
	}
}
