﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class SkillManager
	{
		private static SkillManager? instance;

		private IList<SkillData> _physicalSkills;
		private IList<SkillData> _mentalSkills;
		private IList<SkillData> _combinedSkills;

		public IList<SkillData> PhysicalSkills => _physicalSkills;
		public IList<SkillData> MentalSkills => _mentalSkills;
		public IList<SkillData> CombinedSkills => _combinedSkills;

		public static SkillManager GetInstance()
		{
			if (instance == null) instance = DataLoader.LoadSkillManagerData();
			return instance;
		}

		public SkillManager(IList<SkillData> physicalSkills, IList<SkillData> mentalSkills, IList<SkillData> combinedSkills)
		{
			_physicalSkills = physicalSkills;
			_mentalSkills = mentalSkills;
			_combinedSkills = combinedSkills;
		}

		public IList<SkillData> GetAllSkills()
		{
			IList<SkillData> list = new List<SkillData>();
			list = list.Concat(_physicalSkills).ToList();
			list = list.Concat(_mentalSkills).ToList();
			list = list.Concat(_combinedSkills).ToList();
			return list;
		}

		public IList<SkillData> GetAllPhysicalCombatSkills()
		{
			IList<SkillData> list = new List<SkillData>();
			foreach(SkillData item in _physicalSkills)
			{
				if (item.CombatSkill)
					list.Add(item);
			}
			return list;
		}
	}
}
