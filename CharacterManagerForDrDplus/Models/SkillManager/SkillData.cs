﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Models
{
	public class SkillData
	{
		public string Name { get; }
		public bool Specifiable { get; }
		public IList<string> Specifictions { get; }
		public bool CombatSkill { get; }
		public SkillType Type { get; }
		public IList<SkillBonus> SkillBonuses { get; }
		public RuleSource Source { get; }

		public SkillData(string name, bool specifiable, IList<string> specifictions, bool combatSkill, SkillType type, IList<SkillBonus> skillBonuses, RuleSource source)
		{
			Name = name;
			Specifiable = specifiable;
			Specifictions = specifictions;
			CombatSkill = combatSkill;
			Type = type;
			SkillBonuses = skillBonuses;
			Source = source;
		}

		public Skill NewInstance(int level = 0,string specifiction = "")
		{
			return new Skill(Name, specifiction, CombatSkill, Type, level, SkillBonuses, Source);
		}
	}
}
