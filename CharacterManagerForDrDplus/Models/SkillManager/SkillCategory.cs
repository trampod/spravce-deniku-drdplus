﻿using CharacterManagerForDrDplus.Entities;
using System.Collections.Generic;

namespace CharacterManagerForDrDplus.Models
{
	public class SkillCategory : Category<Skill>
	{
		public SkillCategory(string categoryName, IList<Skill> items):base(categoryName,items)
		{
		}
	}
}
