﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Services
{
	public static class Penalties
	{

		/// <summary>
		/// All the methods take in integer representing characters missing strength and return coresponding penalty.
		/// </summary>
		public static class WeaponStrengthPenalty
		{
			public static int? GetCombatPenalty(int missingStrength)
			{
				return getPenalty(missingStrength, 0);
			}
			public static int? GetAttackPenalty(int missingStrength)
			{
				return getPenalty(missingStrength, 1);
			}
			public static int? GetDefensePenalty(int missingStrength)
			{
				return getPenalty(missingStrength, 2);
			}
			public static int? GetDamagePenalty(int missingStrength)
			{
				return getPenalty(missingStrength, 3);
			}

			public static int? GetRangedCombatPenalty(int missingStrength)
			{
				if (missingStrength >= 11) return null;
				else if (missingStrength <= 0) return 0;
				return -((missingStrength-1)%6+1);
			}

			public static int? GetRangePenalty(int missingStrength)
			{
				return getPenalty(missingStrength, 2);
			}

			public static int? GetReloadTime(int missingStrength)
			{
				if (missingStrength >= 11) return null;
				else if (missingStrength <= 0) return 1;
				return ((missingStrength-1)/6+1);
			}

			private static int? getPenalty(int missingStrength, int start)
			{
				if (missingStrength >= 11) return null;
				else if (missingStrength <= start) return 0;
				return -((missingStrength + 1 - start) / 2);
			}
		}
		public static class WeaponSkillPenalty
		{
			public static int? GetCombatPenalty(int skill)
			{
				if (skill > 3 || skill < 0) throw new ArgumentOutOfRangeException();
				return -3 + skill;
			}
			public static int? GetAttackPenalty(int skill)
			{
				if (skill > 3 || skill < 0) throw new ArgumentOutOfRangeException();
				return -3 + skill;
			}
			public static int? GetBlockPenalty(int skill)
			{
				if (skill > 3 || skill < 0) throw new ArgumentOutOfRangeException();
				return -2 + ((skill+1)/2);
			}
			public static int? GetDamagePenalty(int skill)
			{
				if (skill > 3 || skill < 0) throw new ArgumentOutOfRangeException();
				return -1 + ((skill)/2);
			}
		}
	}
}
