﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Services
{
	public static class LogTables
    {
        static double[] fake_table { get; } = { 10, 11, 12, 14, 16, 18, 20, 22, 25, 28, 32, 36, 40, 45, 50, 56, 63, 70, 80, 90 };

        public static double FakeValueFromBonus(int bonus)
        {
            int exponent = (bonus - 10) / 20;
            double multiplier = exponent > 0 ? Math.Pow(10, exponent) : 1 / Math.Pow(10, (-bonus + 29) / 20);
            int index = ((bonus - 10) % 20 + 20) % 20;
            double result = fake_table[index] * multiplier;
            return result;
        }

		/// <summary>
		/// Returns distance in meters for the coresponding bonus. Value is based on rulebook tables.
		/// </summary>
		/// <param name="bonus"></param>
		/// <returns>Distance in meters taken from the rulebook tables.</returns>
		public static double DistanceFromBonus(int bonus)
        {
            double raw = FakeValueFromBonus(bonus - 10);
            return raw;
        }

		public static int BonusFromDistance(double value)
		{
			double raw = BonusFromRealValue(value);
            return (int)Math.Round(raw) + 10;
		}

		public static double WeightFromBonus(int bonus)
        {
            double raw = FakeValueFromBonus(bonus + 10);
            return raw;
        }

		public static int BonusFromWeight(double value)
        {
            double raw = BonusFromRealValue(value);
            return (int)Math.Round(raw) - 10;
        }

        public static double DamageFromBonus(int bonus)
        {
            double raw = FakeValueFromBonus(bonus);
            return raw;
        }

		public static int BaseDamage(int bonusA, int bonusB)
        {
            // ß(τ(Sil) + τ(Zranění zbraně)) − 5
            return (int)Math.Round(BonusFromRealValue(RealValueFromBonus(bonusA) + RealValueFromBonus(bonusB)) - 5);

        }

        public static double RealValueFromBonus(double bonus)
        {
            return Math.Pow(10, bonus / 20 + 0.5);
        }


        public static double BonusFromRealValue(double value)
        {
            return 20 * Math.Log10(value) - 10;
        }
    }
}
