﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Services
{
	public static class CustomFunctions
	{
		internal static List<IItem> ConvertToIItemList<TType>(IList<TType> meleeWeapons) where TType : IItem
		{
			List<IItem> li = new List<IItem>();
			foreach (TType item in meleeWeapons)
				li.Add(item);
			return li;
		}

		internal static List<IItem> ConvertToIItemList(IList<MeleeWeapon> meleeWeapons)
		{
			List<IItem> li = new List<IItem>();
			foreach (MeleeWeapon item in meleeWeapons)
				li.Add(item);
			return li;
		}

		internal static List<IItem> ConvertToIItemList(IList<RangedWeapon> rangedWeapons)
		{
			List<IItem> li = new List<IItem>();
			foreach (RangedWeapon item in rangedWeapons)
				li.Add(item);
			return li;
		}

		internal static List<IItem> ConvertToIItemList(IList<EquipmentItem> equipmentItems)
		{
			List<IItem> li = new List<IItem>();
			foreach (EquipmentItem item in equipmentItems)
				li.Add(item);
			return li;
		}
	}
}
