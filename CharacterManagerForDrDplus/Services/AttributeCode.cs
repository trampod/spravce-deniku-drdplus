﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Services
{
	public enum AttributeCode
	{
		Sil,
		Obr,
		Zrc,
		Vol,
		Int,
		Chr,
		Odl,
		Vdr,
		Rch,
		Sms
	}
}
