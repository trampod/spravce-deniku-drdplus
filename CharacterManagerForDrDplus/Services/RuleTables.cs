﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Services
{
	public static class RuleTables
	{
		public static int GetStartMoneyValue(int startMoneyGrade)
		{
			return (int)Math.Pow(10, (double)(startMoneyGrade / 2)) * (1 + (2 * (startMoneyGrade % 2))) * 100;
		}

		public static string GetOriginName(int originGrade)
		{
			switch (originGrade)
			{
				case 0:
					return "Nalezenec";
				case 1:
					return "Sirotek";
				case 2:
					return "Z neúplné rodiny";
				case 3:
					return "Z pochybné rodiny";
				case 4:
					return "Ze slušné rodiny";
				case 5:
					return "Z dobré rodiny";
				case 6:
					return "Z velmi dobré rodiny";
				case 7:
					return "Šlechtic";
				case 8:
					return "Šlechtic z dobrého rodu";
				default:
					throw new ArgumentOutOfRangeException("Invalid OriginGrade");
			}
		}

		public static void GetStartSkills(int startSkillGrade, Profession profession, out int startPhysicalSkills, out int startMentalSkills,out int startCombinedSkills)
		{
			ProfessionManager.GetInstance().GetProfessionData(profession).GetStartSkills(startSkillGrade,out startPhysicalSkills, out startMentalSkills, out startCombinedSkills);
		}

		public static int GetHeightCorrection(int heightBonus)
		{
			if (heightBonus < 4) return -1;
			else if (heightBonus > 7) return 1;
			else return 0;
		}

		public static int GetStartingMainAttributePoints(ExceptionalityType et)
		{
			switch (et)
			{
				case ExceptionalityType.Vlastnosti:
					return 3;
				case ExceptionalityType.Kombinovane:
					return 2;
				case ExceptionalityType.Zazemi:
					return 1;
				default:
					return 0;
			}
		}

		public static int GetStartingMinorAttributePoints(ExceptionalityType et)
		{
			return GetStartingMainAttributePoints(et) * 2;
		}

		public static int GetStartingMaxAttribute(ExceptionalityType et)
		{
			return GetStartingMainAttributePoints(et);
		}

		public static string GetPresentableMoney(int? money)
		{
			int moneyValue;
			if (money == null)
				return "-";
			else
				moneyValue = (int)money;

			if (moneyValue == 0)
				return "0zl";

			int zl = moneyValue / 100;
			int st = (moneyValue - zl * 100) / 10;
			int md = (moneyValue - zl * 100 - st * 10);

			List<string> ls = new List<string>();

			if (zl != 0)
				ls.Add(zl + " zl");
			if (st != 0)
				ls.Add(st + " st");
			if (md != 0)
				ls.Add(md + " md");

			return string.Join("  ", ls);
		}

		internal static string SkillTypeToString(SkillType physical)
		{
			return physical switch
			{
				SkillType.Fyzicka => "Fyzické",
				SkillType.Psychicka => "Psychické",
				SkillType.Kombinovana => "Kombinovaný",
				_ => "neznámý"
			};
		}
	}
}
