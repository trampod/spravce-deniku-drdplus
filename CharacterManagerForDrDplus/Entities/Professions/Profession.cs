﻿using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public abstract class Profession
	{
		private AttributeCode _combatAttribute;
		private AttributeCode[] _mainAttributes;

		public virtual string Name => "neznámá";
		public AttributeCode CombatAttribute { get { return _combatAttribute; } }
		public AttributeCode[] MainAttributes { get { return _mainAttributes; } }

		public Profession(AttributeCode combatAttribute, AttributeCode[] mainAttributes)
		{
			_combatAttribute = combatAttribute;
			_mainAttributes = mainAttributes;
		}

		public abstract bool IsValid(Character character, ref IList<Information> problems);

		public abstract void UpdateProfession(Character character);
	}
}
