﻿using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class Fighter : Profession
	{
		public override string Name => "Bojovník";

		public Fighter(AttributeCode combatAttribute, AttributeCode[] mainAttributes) : base(combatAttribute, mainAttributes)
		{

		}

		public override bool IsValid(Character character, ref IList<Information> problems)
		{
			//problems = new List<string>();
			return true;
		}

		public override void UpdateProfession(Character character)
		{

		}

	//public Fighter(string name, string combatAttribute, string[] mainAttributes) : base(name, combatAttribute, mainAttributes)
	//{

	//}
}
}
