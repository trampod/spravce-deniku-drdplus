﻿using CharacterManagerForDrDplus.Models;
using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class Thief : Profession
	{
		public override string Name => "Zloděj";

		public List<ThiefSkill> ThiefSkills { get; }

		public List<ThiefManeuver> _thiefManeuvers;
		public List<ThiefManeuver> ThiefManeuvers => _thiefManeuvers;
		public int Profipoints { get; set; }

		public string GuildName { get; set; }
		public string GuildMentor { get; set; }
		public string GuildPosition { get; set; }

		public Thief(AttributeCode combatAttribute, AttributeCode[] mainAttributes, List<ThiefSkill> thiefSkills,
			List<ThiefManeuver> thiefManeuvers, int profipoints,
			string guildName = "", string guildMentor = "", string guildPosition = "") : base(combatAttribute,mainAttributes)
		{
			ThiefSkills = thiefSkills;
			_thiefManeuvers = thiefManeuvers;
			Profipoints = profipoints;
			GuildName = guildName;
			GuildMentor = guildMentor;
			GuildPosition = guildPosition;
		}

		public List<string> GetGuildPositions()
		{
			return ((ThiefData)ProfessionManager.GetInstance().GetProfessionData(this)).GuildPositions;
		}

		internal int GetThiefSkillRating(string subject)
		{
			foreach (ThiefSkill item in ThiefSkills)
			{
				if (item.Name == subject)
				{
					return item.Rating;
				}
			}
			return 0;
		}

		internal int GetThiefSkillBonus(string subject)
		{
			foreach (ThiefSkill item in ThiefSkills)
			{
				if (item.Name == subject)
				{
					return item.Rating * item.BonusPerLevel;
				}
			}
			return 0;
		}

		public override bool IsValid(Character character, ref IList<Information> problems)
		{
			List<string> thiefSkills = new List<string>();
			foreach(ThiefSkill item in ThiefSkills)
			{
				if(!item.IsValid(character))
				{
					thiefSkills.Add(item.Name);	
				}
			}
				
			if (thiefSkills.Count != 0)
			{
				problems.Add(new Information("Zloděj","Některé zlodějské dovednosti nemají splněny podmínky: " + string.Join(", ",thiefSkills) + ".\n\nPostarej se o naplnění jejich podmínek. Pokud je nemůžeš splnit, popřemýšlej o jejich odebrání nebo snížení."));
				return false;
			}
			else
				return true;		
		}

		internal int GetThiefCombatBonus(string combatSkill)
		{
			switch (combatSkill)
			{
				case "Nože a dýky":
					return GetThiefSkillBonus("Dýka");
				case "Kuše":
					return GetThiefSkillBonus("Kuše a minikuše");
				case "Vrhací zbraně":
					return GetThiefSkillBonus("Malé vrhací zbraně");
				default:
					return 0;
			}
		}

		public override void UpdateProfession(Character character)
		{
			UpdateThiefManeuvers(character);
		}

		//aktualizuje finty zloděje
		public void UpdateThiefManeuvers(Character character)
		{
			List<ThiefManeuver> ltm = ((ThiefData)ProfessionManager.GetInstance().GetProfessionData(this)).ThiefManeuvers;

			_thiefManeuvers = ltm.Where(x => x.IsValid(character)).ToList();

		}

		internal bool RemoveThiefSkill(ThiefSkill thiefSkill, Character character)
		{
			if (ThiefSkills.Contains(thiefSkill) &&
				(!character.CharacterIsClosed || thiefSkill.GetPrice() <= Profipoints))
			{
				Profipoints += thiefSkill.GetPrice();
				ThiefSkills.Remove(thiefSkill);
				ThiefSkills.Sort((x, y) => string.Compare(x.Name, y.Name));
				UpdateThiefManeuvers(character);
				return true;
			}
			return false;
		}

		internal bool AddThiefSkill(ThiefSkill thiefSkill, Character character)
		{
			if (!character.CharacterIsClosed || thiefSkill.GetPrice() <= Profipoints)
			{
				Profipoints -= thiefSkill.GetPrice();
				ThiefSkills.Add(thiefSkill);
				ThiefSkills.Sort((x, y) => string.Compare(x.Name, y.Name));
				UpdateThiefManeuvers(character);
				return true;
			}
			return false;
		}
	}
}
