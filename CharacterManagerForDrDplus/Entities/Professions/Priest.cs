﻿using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class Priest : Profession
	{
		public override string Name => "Kněz";

		public Priest(AttributeCode combatAttribute, AttributeCode[] mainAttributes) : base(combatAttribute, mainAttributes)
		{

		}

		public override bool IsValid(Character character, ref IList<Information> problems)
		{
			//problems = new List<string>();
			return true;
		}

		public override void UpdateProfession(Character character)
		{

		}
	}
}
