﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class ThiefManeuver
	{
		public string Name { get; }
		public string Weapons { get; }
		public int? Advantage { get; }
		public bool GroupAdvantage { get; }
		public string SpecificAdvantage { get; }
		public IList<Requirement> Requirements { get; }
		public RuleSource Source { get; }

		public ThiefManeuver(string name, string weapons, int? advantage, bool groupAdvantage,
			string specificAdvantage, IList<Requirement> requirements, RuleSource source)
		{
			Name = name;
			Weapons = weapons;
			Advantage = advantage;
			GroupAdvantage = groupAdvantage;
			SpecificAdvantage = specificAdvantage;
			Requirements = requirements;
			Source = source;
		}

		public bool IsValid(ICharacter character)
		{
			foreach (Requirement item in Requirements)
			{
				if (!item.AreMet(character))
					return false;
			}
			return true;
		}

		public string GetPresentableRequirments()
		{
			List<string> list = Requirements.Select(x => x.ToString()!).ToList();

			if (list.Count == 0)
				return "-";

			return string.Join(", ", list);
		}

		public string GetAdvantageString()
		{
			if (SpecificAdvantage != "")
				return SpecificAdvantage;
			if (Advantage == null)
				return "-";
			return (Advantage < 0 ? "" : "+") + Advantage + (!GroupAdvantage ? "" : " proti každému protivníkovi");
		}
	}
}
