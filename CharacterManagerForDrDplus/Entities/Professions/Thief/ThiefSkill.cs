﻿using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class ThiefSkill : INotifyPropertyChanged
	{
		public string Name { get; }
		public string Category { get; }
		public List<AttributeCode> Attributes { get; }
		public string MasterBonus { get; }
		private int _rating;
		public int Rating { get => _rating; set { _rating = value; NotifyPropetyChanged(nameof(PresentableName)); } }
		private bool _mastered;
		public bool Mastered { get => _mastered; set { _mastered = value; NotifyPropetyChanged(nameof(PresentableName)); } }
		public Dictionary<int,List<Requirement>> Requirements { get; }
		public List<RollType> BaseRolls { get; }
		public int BonusPerLevel { get; }
		public string Gear { get; }
		public string Concentration { get; }
		public string Duration { get; }
		public RuleSource Source { get; }
		public RuleSource? GMSource { get; }


		[JsonIgnore]
		public string PresentableName
		{
			get
			{
				string val = Rating switch
				{
					1 => "I.",
					2 => "II.",
					3 => !Mastered ? "III." : "M (" + MasterBonus + ")",
					_ => "0."
				};
				return Name + " - " + val;
			}
		}

		public string PresentableRequirements(int rating)
		{
			if (rating == 0)
				return "-";
			List<Requirement> allRequirements = new List<Requirement>();
			for (int i = rating; i > 0; i--)
			{
				foreach(Requirement newR in Requirements[i])
				{
					if(allRequirements.Where(x=> x.Type == newR.Type && x.Subject == newR.Subject).Count() == 0)
					{
						allRequirements.Add(newR);
					}
					/*foreach (SkillRequirement item in allRequirements)
					{
						if (item.Type == newR.Type && item.Subject == newR.Subject)
							allRequirements.Remove(item);
					}*/
				}
			}

			List<string> list = allRequirements.Select(x => x.ToString()!).ToList();

			if (list.Count == 0)
				return "-";

			return string.Join(", ", list);
		}

		public event PropertyChangedEventHandler? PropertyChanged;

		public void NotifyPropetyChanged(string property)
		{
			if (this.PropertyChanged != null)
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
		}

		public ThiefSkill(string name, string category, List<AttributeCode> attributes, string masterBonus, int rating,
			bool mastered, Dictionary<int, List<Requirement>> requirements, List<RollType> baseRolls, int bonusPerLevel, string gear,
			string concentration, string duration, RuleSource source, RuleSource? gmsource)
		{
			Name = name;
			Category = category;
			Attributes = attributes;
			MasterBonus = masterBonus;
			Rating = rating;
			Mastered = mastered;
			Requirements = requirements;
			BaseRolls = baseRolls;
			BonusPerLevel = bonusPerLevel;
			Gear = gear;
			Concentration = concentration;
			Duration = duration;
			Source = source;
			GMSource = gmsource;
		}

		public int GetPrice()
		{
			if (Mastered) return 8;
			return Rating switch
			{
				3 => 6,
				2 => 3,
				1 => 1,
				_ => 0
			};
		}

		public bool IsValid(Character character)
		{
			if (Rating == 0)
				return true;
			if (!Requirements.ContainsKey(Rating))
				return false;
			if (character.Level > Rating)
				return false;
			foreach (Requirement item in Requirements[Rating])
			{
				if (!item.AreMet(character))
					return false;
			}
			return true;
		}

		public bool IncreaseSkill(Character character)
		{
			return false;
		}

		public ThiefSkill NewInstance(int rating = 1, bool mastered = false)
		{
			return new ThiefSkill(Name, Category, Attributes, MasterBonus, mastered ? 3 : rating, mastered,
				Requirements, BaseRolls, BonusPerLevel, Gear, Concentration, Duration, Source, GMSource);
		}
	}
}
