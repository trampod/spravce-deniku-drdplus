﻿using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class Requirement
	{
		public ThiefRequirementType Type { get; }
		public string Subject { get; }
		public int Value { get; }

		public Requirement(ThiefRequirementType type, string subject, int value)
		{
			Type = type;
			Subject = subject;
			Value = value;
		}

		public bool AreMet(ICharacter character)
		{
			switch (Type)
			{
				case ThiefRequirementType.Uroven:
					return AreMetLevel(character);
				case ThiefRequirementType.Vlastnost:
					return AreMetAttribute(character);
				case ThiefRequirementType.Dovednost:
					return AreMetSkill(character);
				case ThiefRequirementType.ZlodejskaDovednost:
					return AreMetThiefSkill(character);
				case ThiefRequirementType.BonusMistra:
					return AreMetMasterBonus(character);
				case ThiefRequirementType.ZlodejskaDovednostniSkupina:
					return AreMetThiefSkillCategory(character);
				case ThiefRequirementType.BojoveDovednosti:
					return AreMetCombatSkills(character);
				case ThiefRequirementType.ZlodejskaDovednostNebo:
					return AreMetThiefSkillOr(character);
				default:
					throw new Exception();
			}
		}

		private bool AreMetLevel(ICharacter character)
		{
			return character.Level >= Value;
		}

		private bool AreMetAttribute(ICharacter character)
		{
			if(Enum.TryParse(Subject, out AttributeCode attribute))
			{
				return character.GetAttribute(attribute) >= Value;
			}
			throw new Exception(Subject + " is not valid AttributeCode.");
		}

		private bool AreMetSkill(ICharacter character)
		{
			if(Subject.Contains(" (") && Subject.Contains(")"))
			{
				string specification = Subject.Split('(', ')')[1];
				int index = Subject.IndexOf(" (");
				return character.GetSkillRating(Subject.Substring(0, index), specification) >= Value;
			}
			return character.GetSkillRating(Subject) >= Value;
		}

		private bool AreMetThiefSkill(ICharacter character)
		{
			if (character.Profession.GetType() != typeof(Thief))
				return false;

			return ((Thief)character.Profession).GetThiefSkillRating(Subject) >= Value;
		}

		private bool AreMetMasterBonus(ICharacter character)
		{
			if (character.Profession.GetType() != typeof(Thief))
				return false;

			foreach(ThiefSkill item in ((Thief)character.Profession).ThiefSkills)
			{
				if (item.Mastered && item.MasterBonus == Subject)
					return true;
			}
			return false;
		}

		private bool AreMetThiefSkillCategory(ICharacter character)
		{
			if (character.Profession.GetType() != typeof(Thief))
				return false;
			int count = 0;
			foreach (ThiefSkill item in ((Thief)character.Profession).ThiefSkills)
			{
				if (item.Category == Subject && item.Rating > 0)
					count++;
			}
			if (count >= Value)
				return true;
			return false;
		}

		private bool AreMetCombatSkills(ICharacter character)
		{
			int rating = Subject.Length;
			int count = 0;
			foreach (Skill item in character.Skills)
			{
				if (item.Name == "Boj se zbraní" && item.Rating >= rating)
					count++;
			}
			return count >= Value;
		}

		private bool AreMetThiefSkillOr(ICharacter character)
		{
			if (character.Profession.GetType() != typeof(Thief))
				return false;

			List<string> subjects = Subject.Split(';').ToList();
			foreach(string item in subjects)
			{
				if (((Thief)character.Profession).GetThiefSkillRating(item) >= Value)
					return true;
			}

			return false;
		}

		public override string ToString()
		{
			if (Type == ThiefRequirementType.ZlodejskaDovednostniSkupina)
				return "alespoň " + Value + " schopnosti ze skupiny " + Subject;

			if (Type == ThiefRequirementType.BojoveDovednosti)
				return "alespoň " + Value + " dovednosti Boj s... o stupni " + Subject + ".";

			if (Type == ThiefRequirementType.ZlodejskaDovednostNebo)
			{
				List<string> subjects = Subject.Split(';').ToList();
				for (int i = 0; i < subjects.Count; i++)
				{
					subjects[i] += Value switch
					{
						3 => "III.",
						2 => "II.",
						1 => "I.",
						_ => "0."
					};
				}
				return string.Join(" nebo ", subjects);
			}


			string output = Type==ThiefRequirementType.Uroven?"Úroveň":Subject;
			output += " ";
			switch (Type)
			{
				case ThiefRequirementType.Uroven:
					output += Value;
					break;
				case ThiefRequirementType.Vlastnost:
					output += Value < 0 ? Value : ("+" + Value);
					break;
				case ThiefRequirementType.Dovednost:
				case ThiefRequirementType.ZlodejskaDovednost:
					output += Value switch {
					3 => "III.",
					2 => "II.",
					1 => "I.",
					_ => "0."
					};
					break;
				case ThiefRequirementType.BonusMistra:
					output+= "(" + "bonus Mistra" +")";
					break;
				default:
					break;
			}
			return output;
		}
	}

	public enum ThiefRequirementType
	{
		Uroven,
		Vlastnost,
		Dovednost,
		ZlodejskaDovednost,
		BonusMistra,
		ZlodejskaDovednostniSkupina,
		BojoveDovednosti,
		ZlodejskaDovednostNebo
	}
}
