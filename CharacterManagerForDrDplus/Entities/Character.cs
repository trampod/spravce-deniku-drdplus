﻿using CharacterManagerForDrDplus.Models;
using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class Character : ICharacter
	{
		public string Name { get; set; }
		public int Level { get; set; }
		public bool CharacterIsClosed { get; private set; } = false;

		public IList<MeleeWeapon> MeleeWeapons { get; set; }
		public IList<RangedWeapon> RangedWeapons { get; set; }
		public IList<Shield> Shields { get; set; }
		public IList<Ammo> Amunition { get; set; }
		public IList<Armor> Armors { get; set; }

		public Dictionary<string, IList<EquipmentItem>> Inventory { get; set; }

		public IList<WeaponCombinationHolder> OwnedWeaponCombinations { get; set; }
		//public IList<WeaponCombination> OwnedWeaponCombinations { get; set; }
		//public IList<WeaponCombination> CustomWeaponCombinations { get; set; }

		public Profession Profession { get; set; }

		public ExceptionalityType Exceptionality { get; set; }

		public Race Race { get; set; }

		public int Size { get; set; }

		public int MaxWeight {
			get 
			{
				int tmp = Race.AverageWeight + Race.AverageWeight / 10;
				return tmp; 
			}  
		}

		[JsonIgnore]
		public int MinWeight { get { return Race.AverageWeight - Race.AverageWeight / 10;}  }

		public int Weight { get; set; }

		[JsonIgnore]
		public int WeightBonus { get { return LogTables.BonusFromWeight(Weight)-12; }  }

		[JsonIgnore]
		public int MaxHeight { get {return Race.AverageHeight + Race.AverageHeight / 10; }  }

		[JsonIgnore]
		public int MinHeight { get { return Race.AverageHeight - Race.AverageHeight / 10;}  }

		public int Height { get; set; }

		[JsonIgnore]
		public int HeightBonus { get { return LogTables.BonusFromDistance(((double)Height)/100); } }

		public Dictionary<AttributeCode, int> BaseAttributes { get; set; }

		public Dictionary<AttributeCode, int> DerivedAttributes { get; set; }

		[JsonIgnore]
		public int Combat { get; private set; }

		[JsonIgnore]
		public int Attack { get; private set; }

		[JsonIgnore]
		public int Ranged { get; private set; }

		[JsonIgnore]
		public int Defense { get; private set; }

		//public int TestSkill { get; set; }

		private AttributeCode? LastMainAttributeIncrease { get; set; } = null;
		private AttributeCode? LastMinorAttributeIncrease { get; set; } = null;
		private int MainAttributeIncreaseCount { get; set; } = 0;

		[JsonIgnore]
		public int Beauty { get; private set; }

		[JsonIgnore]
		public int Dangerousnes { get; private set; }

		[JsonIgnore]
		public int Dignity { get; private set; }

		//public int PhysicalSkillPoints { get; set; }

		//public int MentalSkillPoints { get; set; }

		//public int CombinedSkillPoints { get; set; }

		//public int StartPhysicalSkillPoints { get; set; }

		//public int StartMentalSkillPoints { get; set; }

		//public int StartCombinedSkillPoints { get; set; }

		public Dictionary<SkillType, int> SkillPoints { get; set; }

		public Dictionary<SkillType, int> StartSkillPoints { get; set; }

		public Dictionary<SkillType, int> StartSkillPointsAdjusment { get; set; }

		[JsonIgnore]
		public string Origin { get { return RuleTables.GetOriginName(OriginGrade); } }
		[JsonIgnore]
		public int StartMoneyValue { get { return RuleTables.GetStartMoneyValue(StartMoneyGrade); } }

		[JsonIgnore]
		public int WoundLimit { get; private set; }

		[JsonIgnore]
		public int FatigueLimit { get; private set; }

		public bool Female { get; set; }

		public bool ApplyGender { get; set; }
		[JsonIgnore]
		public int BackroundPoints { get
			{
				switch (Exceptionality)
				{
					case ExceptionalityType.Vlastnosti:
						return 5;
					case ExceptionalityType.Kombinovane:
						return 10;
					case ExceptionalityType.Zazemi:
						return 15;
					default:
						return 0;
				}
			} }

		public bool AddItem(IItem item, int buyingPrice = 0)
		{
			if (buyingPrice > Money && CharacterIsClosed)
				return false;

			Money -= buyingPrice * item.Count;
			switch (item)
			{
				case EquipmentItem:
					AddEquipment((EquipmentItem)item);
					break;
				case MeleeWeapon:
					AddMeleeWeapon((MeleeWeapon)item);
					break;
				case RangedWeapon:
					AddRangedWeapon((RangedWeapon)item);
					break;
				case Shield:
					AddShield((Shield)item);
					break;
				case Ammo:
					AddAmmo((Ammo)item);
					break;
				case Armor:
					AddArmor((Armor)item);
					break;
				default:
					break;
			}
			return true;
		}

		private void AddEquipment(EquipmentItem item)
		{
			if (Inventory.ContainsKey(item.Category))
				Inventory[item.Category].Add(item);
			else
				Inventory.Add(item.Category, new List<EquipmentItem>() { item });
		}

		private void AddMeleeWeapon(MeleeWeapon item)
		{
			MeleeWeapons.Add(item);
			OwnedWeaponCombinations.Add(new WeaponCombinationHolder(item.Name,item.Guid,null));
		}

		private void AddRangedWeapon(RangedWeapon item)
		{
			RangedWeapons.Add(item);
			OwnedWeaponCombinations.Add(new WeaponCombinationHolder(item.Name, item.Guid, null));
		}

		private void AddShield(Shield item)
		{
			Shields.Add(item);
		}

		private void AddAmmo(Ammo item)
		{
			Amunition.Add(item);
		}

		private void AddArmor(Armor item)
		{
			Armors.Add(item);
		}

		public bool RemoveItem(IItem item,int sellingPrice = 0)
		{
			Money += sellingPrice * item.Count;
			switch (item)
			{
				case EquipmentItem:
					RemoveEquipment((EquipmentItem)item);
					break;
				case MeleeWeapon:
					RemoveMeleeWeapon((MeleeWeapon)item);
					break;
				case RangedWeapon:
					RemoveRangedWeapon((RangedWeapon)item);
					break;
				case Shield:
					RemoveShield((Shield)item);
					break;
				case Ammo:
					RemoveAmmo((Ammo)item);
					break;
				case Armor:
					RemoveArmor((Armor)item);
					break;
				default:
					break;
			}
			return true;
		}

		private void RemoveEquipment(EquipmentItem item)
		{
			if (Inventory.ContainsKey(item.Category))
				Inventory[item.Category].Remove(item);
		}

		private void RemoveMeleeWeapon(MeleeWeapon item)
		{
			MeleeWeapons.Remove(item);
			RemoveWeaponCombination(item.Guid);
		}

		private void RemoveRangedWeapon(RangedWeapon item)
		{
			RangedWeapons.Remove(item);
			//foreach (WeaponCombinationHolder holder in OwnedWeaponCombinations)
			//{
			//	if (holder.WeaponGuid1 == item.Guid)
			//		OwnedWeaponCombinations.Remove(holder);
			//}
			RemoveWeaponCombination(item.Guid);
		}

		private void RemoveWeaponCombination(Guid weapon_guid)
		{
			for (int i = OwnedWeaponCombinations.Count - 1; i >= 0; i--)
			{
				if (OwnedWeaponCombinations[i].WeaponGuid1 == weapon_guid)
					OwnedWeaponCombinations.RemoveAt(i);
			}
		}

		private void RemoveShield(Shield item)
		{
			Shields.Remove(item);
		}

		private void RemoveAmmo(Ammo item)
		{
			Amunition.Remove(item);
		}

		private void RemoveArmor(Armor item)
		{
			Armors.Remove(item);
		}

		private int _originGrade;
		public int OriginGrade 
		{
			get { return _originGrade; } 
			set
			{
				_originGrade = value;
				if (_startMoneyGrade > _originGrade + 3)
					StartMoneyGrade = _originGrade + 3;
				if (_startSkillGrade > _originGrade + 3)
					StartSkillGrade = _originGrade + 3;
			}
		}

		private int _startMoneyGrade;
		public int StartMoneyGrade { get { return _startMoneyGrade; } set { if (value <= OriginGrade + 3) _startMoneyGrade = value; } }

		private int _startSkillGrade;
		public int StartSkillGrade
		{
			get { return _startSkillGrade; }
			set 
			{
				if (value < OriginGrade + 4)
				{
					_startSkillGrade = value;

					int p, m, c;
					RuleTables.GetStartSkills(_startSkillGrade, Profession, out p, out m, out c);

					StartSkillPoints[SkillType.Fyzicka] = p;
					StartSkillPoints[SkillType.Psychicka] = m;
					StartSkillPoints[SkillType.Kombinovana] = c;
					//StartMentalSkillPoints = m;
					//StartPhysicalSkillPoints = p;
					//StartCombinedSkillPoints = c;
				}
			} 
		}

		[JsonIgnore]
		public int FreeMainAttributePoints 
		{ 
			get
			{
				int max = Level - 1 + RuleTables.GetStartingMainAttributePoints(Exceptionality);
				int current = 0;
				foreach(AttributeCode item in Profession.MainAttributes)
				{
					current += BaseAttributes[item];
				}
				return max - current+2;
			}
		}

		[JsonIgnore]
		public int FreeMinorAttributePoints 
		{ 
			get 
			{
				int max = Level - 1 + RuleTables.GetStartingMinorAttributePoints(Exceptionality);
				int current = 0;
				foreach (AttributeCode item in Profession.MainAttributes)
				{
					current -= BaseAttributes[item];
				}
				foreach (AttributeCode item in BaseAttributes.Keys)
				{
					current += BaseAttributes[item];
				}
				return max - current;
			}
		}

		public int Money { get; set; }

		public IList<Skill> Skills { get; set; }

		public Character(string Name, int Level, Profession Profession, Race Race)
		{
			this.Name = Name;
			this.Level = Level;
			this.Profession = Profession;
			this.Race = Race;
			Height = Race.AverageHeight;
			Weight = Race.AverageWeight;
			this.Exceptionality = ExceptionalityType.Kombinovane;
			OwnedWeaponCombinations = new List<WeaponCombinationHolder>();

			BaseAttributes = new Dictionary<AttributeCode, int>();
			BaseAttributes.Add(AttributeCode.Sil, 0);
			BaseAttributes.Add(AttributeCode.Obr, 0);
			BaseAttributes.Add(AttributeCode.Zrc, 0);
			BaseAttributes.Add(AttributeCode.Vol, 0);
			BaseAttributes.Add(AttributeCode.Int, 0);
			BaseAttributes.Add(AttributeCode.Chr, 0);

			SkillPoints = new Dictionary<SkillType, int>();
			SkillPoints.Add(SkillType.Fyzicka, 0);
			SkillPoints.Add(SkillType.Psychicka, 0);
			SkillPoints.Add(SkillType.Kombinovana, 0);

			StartSkillPoints = new Dictionary<SkillType, int>();
			StartSkillPoints.Add(SkillType.Fyzicka, 0);
			StartSkillPoints.Add(SkillType.Psychicka, 0);
			StartSkillPoints.Add(SkillType.Kombinovana, 0);

			StartSkillPointsAdjusment = new Dictionary<SkillType, int>();
			StartSkillPointsAdjusment.Add(SkillType.Fyzicka, 0);
			StartSkillPointsAdjusment.Add(SkillType.Psychicka, 0);
			StartSkillPointsAdjusment.Add(SkillType.Kombinovana, 0);

			OriginGrade = 0;
			StartSkillGrade = 0;
			StartMoneyGrade = 0;

			foreach (AttributeCode item in Profession.MainAttributes)
			{
				BaseAttributes[item] = 1;
			}

			DerivedAttributes = new Dictionary<AttributeCode, int>();
			DerivedAttributes.Add(AttributeCode.Odl, 0);
			DerivedAttributes.Add(AttributeCode.Vdr, 0);
			DerivedAttributes.Add(AttributeCode.Rch, 0);
			DerivedAttributes.Add(AttributeCode.Sms, 0);

			UpdateDerivedValues();

			Skills = new List<Skill>();

			MeleeWeapons = new List<MeleeWeapon>();
			RangedWeapons = new List<RangedWeapon>();
			Shields = new List<Shield>();
			Amunition = new List<Ammo>();
			Armors = new List<Armor>();
			Inventory = new Dictionary<string, IList<EquipmentItem>>();
			IList<string> itemCategories = ItemManager.GetInstance().GetEquipmentCategoryNames();
			foreach(string item in itemCategories)
			{
				Inventory.Add(item, new List<EquipmentItem>());
			}

			//OwnedWeaponCombinations = new List<WeaponCombination>();
			//CustomWeaponCombinations = new List<WeaponCombination>();

			//OwnedWeaponCombinations.Add(new WeaponCombination("Sekera",new MeleeWeapon("Sekera","Zbraň",2,2400,1,new RuleSource("pph",85),"Sekery",6,2,3,5,"S",2,false), null,this));
		}

		public int GetAttribute(AttributeCode attribute)
		{
			int baseValue = 0, raceValue, improvementsValue = 0;
			if (BaseAttributes.ContainsKey(attribute)) baseValue = BaseAttributes[attribute];
			else if (DerivedAttributes.ContainsKey(attribute)) baseValue = DerivedAttributes[attribute];
			else throw new ArgumentOutOfRangeException("Tried to get nonexistent attribute.");

			raceValue = Race.GetAttribute(attribute, Female && ApplyGender);

			return baseValue + raceValue + improvementsValue;
		}

		public int GetAttributeMin(AttributeCode attribute)
		{
			if (!BaseAttributes.ContainsKey(attribute)) 
				throw new ArgumentOutOfRangeException("Tried to get nonexistent attribute.");

			if (IsMainAttribute(attribute))
					return 1;

			return 0;
		}

		public int GetAttributeMax(AttributeCode attribute)
		{
			if (!BaseAttributes.ContainsKey(attribute))
				throw new ArgumentOutOfRangeException("Tried to get nonexistent attribute.");

			if(IsMainAttribute(attribute))
				return 3+(Level-1)-((Level-1)/3);
			
			return RuleTables.GetStartingMaxAttribute(Exceptionality) + (Level-1) - (Level-1)/2;
		}

		public int GetAttributeStartingMin(AttributeCode attribute)
		{
			if (!BaseAttributes.ContainsKey(attribute))
				throw new ArgumentOutOfRangeException("Tried to get nonexistent attribute.");

			if (IsMainAttribute(attribute))
				return 1;

			return 0;
		}

		public void GetSkillPointLimits(SkillType skillType, out int surePoints, out int variablePoints)
		{
			switch (skillType)
			{
				case SkillType.Fyzicka:
					CalculateSkillPointLimits(skillType, AttributeCode.Sil, AttributeCode.Obr, out surePoints, out variablePoints);
					break;
				case SkillType.Psychicka:
					CalculateSkillPointLimits(skillType, AttributeCode.Int, AttributeCode.Vol, out surePoints, out variablePoints);
					break;
				case SkillType.Kombinovana:
					CalculateSkillPointLimits(skillType, AttributeCode.Zrc, AttributeCode.Chr, out surePoints, out variablePoints);
					break;
				default:
					throw new Exception();
			}
		}

		private void CalculateSkillPointLimits(SkillType skillType, AttributeCode attribute1, AttributeCode attribute2, out int surePoints, out int variablePoints)
		{
			CalculateAttributePointLimits(attribute1, out int SP1, out int VP1);
			CalculateAttributePointLimits(attribute2, out int SP2, out int VP2);


			surePoints = StartSkillPoints[skillType] + StartSkillPointsAdjusment[skillType] + SP1 + SP2;
			variablePoints = VP1+VP2;
		}

		private void CalculateAttributePointLimits(AttributeCode attribute, out int surePoints, out int variablePoints)
		{
			int MaxStart = IsMainAttribute(attribute) ? 3 : RuleTables.GetStartingMaxAttribute(Exceptionality);
			int MaxZv = GetAttributeMax(attribute) - MaxStart;
			int Val = BaseAttributes[attribute];
			surePoints = Math.Max(Val - MaxStart, 0);
			variablePoints = Math.Min(MaxZv - surePoints, Val - (IsMainAttribute(attribute) ? 1 : 0));
		}

		public bool IsMainAttribute(AttributeCode attribute)
		{
			foreach (AttributeCode item in Profession.MainAttributes)
			{
				if (item == attribute)
				{
					return true;
				}
			}
			return false;
		}

		public string GetAttributeNote(AttributeCode attribute)
		{
			int baseValue = 0, raceValue = 0, improvementsValue = 0;
			if (BaseAttributes.ContainsKey(attribute)) baseValue = BaseAttributes[attribute];
			else if (DerivedAttributes.ContainsKey(attribute)) baseValue = DerivedAttributes[attribute];
			else throw new ArgumentOutOfRangeException("Tried to get nonexistent attribute.");

			raceValue = Race.GetAttribute(attribute,Female && ApplyGender);

			return "Rasa(" + raceValue +") + Úrovně(" +  baseValue + ") + Ostatní(" + improvementsValue + ")";
		}

		public bool IncreaseAttribute(AttributeCode attribute)
		{
			if(Profession.MainAttributes.Contains(attribute))
			{
				if (FreeMainAttributePoints <= 0)
					return false;
				if(LastMainAttributeIncrease == attribute)
				{
					if (MainAttributeIncreaseCount > 1)
					{
						return false;
					}
					else
					{
						MainAttributeIncreaseCount++;
					}
				}
				else
				{
					LastMainAttributeIncrease = attribute;
					MainAttributeIncreaseCount = 1;
				}
			}
			else
			{
				if (FreeMinorAttributePoints <= 0)
					return false;
				if(LastMinorAttributeIncrease == attribute)
				{
					return false;
				}
				else
				{
					LastMinorAttributeIncrease = attribute;
				}
			}
			BaseAttributes[attribute]++;
			UpdateDerivedValues();
			return true;
		}

		public void UpdateDerivedValues()
		{
			int Sil = GetAttribute(AttributeCode.Sil);
			int Obr = GetAttribute(AttributeCode.Obr);
			int Zrc = GetAttribute(AttributeCode.Zrc);
			int Vol = GetAttribute(AttributeCode.Vol);
			int Int = GetAttribute(AttributeCode.Int);
			int Chr = GetAttribute(AttributeCode.Chr);


			DerivedAttributes[AttributeCode.Sms] = Zrc;//+Race.RacialAttributes[AttributeCode.Sms];
			DerivedAttributes[AttributeCode.Odl] = Sil;//+Race.RacialAttributes[AttributeCode.Odl];
			DerivedAttributes[AttributeCode.Vdr] = (Sil + Vol)/ 2;//+Race.RacialAttributes[AttributeCode.Vdr];
			DerivedAttributes[AttributeCode.Rch] = (Sil+Obr)/2 + RuleTables.GetHeightCorrection(HeightBonus);//+Race.RacialAttributes[AttributeCode.Rch];

			Combat = (Obr + GetAttribute(Profession.CombatAttribute)) / 2 + RuleTables.GetHeightCorrection(HeightBonus);
			if (Profession.GetType() == typeof(Thief))
				Combat += ((Thief)Profession).GetThiefSkillBonus("Periferní vidění");
			Attack = Obr / 2;
			Defense = (Obr + 1) / 2;
			Ranged = Zrc / 2;

			WoundLimit = DerivedAttributes[AttributeCode.Odl] + 10;
			FatigueLimit = DerivedAttributes[AttributeCode.Vdr] + 10;

			Beauty = (Obr + Zrc) / 2 + Chr/2;
			Dangerousnes = (Sil + Vol) / 2 + Chr/2;
			Dignity = (Int + Vol) / 2 + Chr/2;
		}

		public List<ItemCategory> GetInventoryCategories()
		{
			List<ItemCategory> lic = new List<ItemCategory>();
			if (MeleeWeapons.Count > 0)
				lic.Add(new ItemCategory("Zbraně na blízko", new List<IItem>(MeleeWeapons)));
			if (RangedWeapons.Count > 0)
				lic.Add(new ItemCategory("Zbraně na dálku", new List<IItem>(RangedWeapons)));
			if (Shields.Count > 0)
				lic.Add(new ItemCategory("Štíty", new List<IItem>(Shields)));
			if (Amunition.Count > 0)
				lic.Add(new ItemCategory("Munice", new List<IItem>(Amunition)));
			if (Armors.Count > 0)
				lic.Add(new ItemCategory("Brnění", new List<IItem>(Armors)));

			foreach (string item in Inventory.Keys)
			{
				if(Inventory[item].Count > 0)
					lic.Add(new ItemCategory(item, new List<IItem>(Inventory[item])));
			}

			return lic;
		}

		public List<IItem> GetInventoryCategory(string category)
		{
			switch (category)
			{
				case "Zbraně na blízko":
					return CustomFunctions.ConvertToIItemList(MeleeWeapons);
				case "Zbraně na dálku":
					return CustomFunctions.ConvertToIItemList(RangedWeapons);
				case "Štíty":
					return CustomFunctions.ConvertToIItemList(Shields);
				case "Munice":
					return CustomFunctions.ConvertToIItemList(Amunition);
				case "Brnění":
					return CustomFunctions.ConvertToIItemList(Armors);
				default:
					return CustomFunctions.ConvertToIItemList(Inventory[category]);
			}
		}

		public int GetCompleteInventoryValue()
		{
			int sum = 0;
			foreach (MeleeWeapon item in MeleeWeapons)
			{
				sum += item.Price == null ? 0 : (int)item.Price;
			}
			foreach (RangedWeapon item in RangedWeapons)
			{
				sum += item.Price == null ? 0 : (int)item.Price;
			}
			foreach (Shield item in Shields)
			{
				sum += item.Price == null ? 0 : (int)item.Price;
			}
			foreach (Ammo item in Amunition)
			{
				sum += item.Price == null ? 0 : (int)item.Price;
			}
			foreach (Armor item in Armors)
			{
				sum += item.Price == null ? 0 : (int)item.Price;
			}
			foreach (IList<EquipmentItem> list in Inventory.Values)
			{
				foreach (EquipmentItem item in list)
				{
					sum += item.Price == null ? 0 : (int)item.Price;
				}
			}
			return sum;
		}

		public IList<Information> ValidateStartingCharacter()
		{
			IList<Information> problems = new List<Information>();

			if (Name == "")
				problems.Add(new Information("Jméno", "Postava potřebuje jméno k tomu aby byla validní.\n\nNezapomeň, že jméno je jen označení postavy. Pokud tvoje postava nezná svoje jméno, má asi nějaký způsob jak se označuje, buď ona sama, nebo ostatními kolem ní. Ale každá postava by měla mít své označení, byť by to bylo \"Bezejmený\""));

			int currentMoney = Money + StartMoneyValue;
			if (currentMoney < 0)
				problems.Add(new Information("Peníze","Byli přesaženy peníze o " + RuleTables.GetPresentableMoney(-currentMoney) + ".\n\nAby mohla být postava uzavřena je potřeba nepřesáhnout peníze získané ze zázemí postavy. Buď odeberte některé předměty nebo zvyšte počáteční peníze postavy."));

			int backgroundPoints = BackroundPoints - OriginGrade - StartMoneyGrade - StartSkillGrade;

			if (backgroundPoints < 0)
				problems.Add(new Information("Zázemí","Byl přesažen počet využitých bodů zázemí. Bylo použito o "+ (-backgroundPoints) + " bodů více.\n\nSniž počet využitých bodů, nebo zvyš počet dostupných bodů skrze vyjímečnost, pokud je to možné."));
			else if(backgroundPoints>0)
				problems.Add(new Information("Zázemí", "Máš " + backgroundPoints + " nevyužitých bodů zázemí.\n\nZvyš jednu z kategorií zázemí, nebo sniž počet dostupných bodů skrze vyjímečnost, pokud je to možné."));

			int freeMainAtt = FreeMainAttributePoints;
			if(freeMainAtt !=0)
			{
				string mainAttMsg = "";
				if (freeMainAtt < 0)
					mainAttMsg = "Přesáhl jsi o " + (-freeMainAtt) + " body hlavních vlastností. Aby postava byla validní je potřeba aby využila přesně její body hlavních vlastností.";
				else if (freeMainAtt > 0)
					mainAttMsg = "Máš " + freeMainAtt + " nevyužitých bodů hlavních vlastností. Aby postava byla validní je potřeba aby využila všechny její body hlavních vlastností.";

				problems.Add(new Information("Hlavní vlastnosti", mainAttMsg));
			}

			int freeMinorAtt = FreeMinorAttributePoints;
			if (freeMinorAtt != 0)
			{
				string minorAttMsg = "";
				if (freeMinorAtt < 0)
					minorAttMsg = "Přesáhl jsi o " + (-freeMinorAtt) + " body vedlejších vlastností. Aby postava byla validní je potřeba aby využila přesně její body vedlejších vlastností.";
				else if (freeMinorAtt > 0)
					minorAttMsg = "Máš " + freeMinorAtt + " nevyužitých bodů vedlejších vlastností. Aby postava byla validní je potřeba aby využila všechny její body vedlejších vlastností.";

				problems.Add(new Information("Vedlejší vlastnosti", minorAttMsg));
			}


			//dovednosti
			if (Level == 1)
			{
				int phy , men , com;
				phy = FreeStartingSkillPoints(SkillType.Fyzicka);
				men = FreeStartingSkillPoints(SkillType.Psychicka);
				com = FreeStartingSkillPoints(SkillType.Kombinovana);

				string skillPointList = "";

				if (phy < 0)
					skillPointList += "\nFyzické dovednosti přesáhnuty o " + (-phy) + " bodů.";
				else if (phy > 0)
					skillPointList += "\nFyzické dovednosti mají " + phy + " nevyužitých bodů.";
				if (men < 0)
					skillPointList += "\nPsychické dovednosti přesáhnuty o " + (-men) + " bodů.";
				else if (men > 0)
					skillPointList += "\nPsychické dovednosti mají " + men + " nevyužitých bodů.";
				if (com < 0)
					skillPointList += "\nKombinované dovednosti přesáhnuty o " + (-com) + " bodů.";
				else if (com > 0)
					skillPointList += "\nKombinované dovednosti mají " + com + " nevyužitých bodů.";

				string joke = "";
				if (men < 0 && GetSkillRating("Čtení / psaní") > 0)
					joke = "\nTřeba se můžeš zbavit dovednosti Čtení/psaní. Kdo by to potřeboval? A navíc knihy jsou děsně drahý, ty si jako chudý dobrodruch stejně nebudeš moct dovolit. To se radši nauč ohánět s pořádným kyjem. Ten je levnej, neničíš si oči když ho používáš ve tmě či za šera a navíc tím můžeš uspat toho otravného kněze co se ti neustále snaží prokecat díru do hlavy.";

				if (skillPointList != "")
				{
					problems.Add(new Information("Dovednosti","Špatně využity body dovedností. Pro validitu postavy je potřeba využít všechny počáteční body dovedností.\n"
						+skillPointList+"\n\nUprav naučené dovednosti, nebo změň míru počátečních dovedností ze zázemí." + joke));
				}

			}
			else
			{
				int max = GetMaxSkillPoints();
				int current = SkillPoints.Aggregate(0, (result, x) => result + x.Value);
				string msg = "";

				int phy, men, com;
				phy = SkillPoints[SkillType.Fyzicka];
				men = SkillPoints[SkillType.Psychicka];
				com = SkillPoints[SkillType.Kombinovana];

				GetSkillPointLimits(SkillType.Fyzicka, out int surePointsPhy, out int variablePointsPhy);
				GetSkillPointLimits(SkillType.Psychicka, out int surePointsMen, out int variablePointsMen);
				GetSkillPointLimits(SkillType.Kombinovana, out int surePointsCom, out int variablePointsCom);

				if (max + current > 0)
					msg += "\nSoučet všech využitých bodů dovedností byl přesažen o " + (-(max + current)) + " bodů.";
				else if (max + current < 0)
					msg += "\nSoučet všech využitých bodů dovedností není dostatečný a zbývají " + (max + current) + " bodů dovedností.";

				if (phy + surePointsPhy > 0)
					msg += "\nZbývá " + (phy + surePointsPhy) + " jistých fyzických dovednostních bodů.";
				else if (phy + variablePointsPhy < 0)
					msg += "\nNejisté fyzické dovednostní body byli přesaženy o " + (-(phy + variablePointsPhy)) + " bodů.";
				if (men + surePointsMen > 0)
					msg += "\nZbývá " + (men + surePointsMen) + " jistých psychických dovednostních bodů.";
				else if (men + variablePointsPhy < 0)
					msg += "\nNejisté psychických dovednostní body byli přesaženy o " + (-(men + variablePointsMen)) + " bodů.";
				if (com + surePointsCom > 0)
					msg += "\nZbývá " + (com + surePointsCom) + " jistých kombinovaných dovednostních bodů.";
				else if (com + variablePointsCom < 0)
					msg += "\nNejisté kombinovaných dovednostní body byli přesaženy o " + (-(com + variablePointsCom)) + " bodů.";


				if (msg != "")
				{
					problems.Add(new Information("Dovednosti", "Špatně využity body dovedností. Pro validitu postavy je potřeba využít všechny jisté body dovedností, nesmí se přesáhnout omezení z nejistých bodů dovedností a musí být plně využit součet všech bodů dovedností.\n"
						+ msg + "\n\nUprav naučené dovednosti, rozdělení vlastností, úroveň postavy nebo změň míru počátečních dovedností ze zázemí."));
				}
			}

			Profession.IsValid(this, ref problems);


			return problems;
		}

		private int FreeStartingSkillPoints(SkillType type)
		{
			return StartSkillPoints[type]
					+ StartSkillPointsAdjusment[type]
					+ SkillPoints[type];
		}

		public int GetMaxSkillPoints()
		{
			return StartSkillPoints[SkillType.Fyzicka]
					+ StartSkillPoints[SkillType.Psychicka]
					+ StartSkillPoints[SkillType.Kombinovana]
					+ StartSkillPointsAdjusment[SkillType.Fyzicka]
					+ StartSkillPointsAdjusment[SkillType.Psychicka]
					+ StartSkillPointsAdjusment[SkillType.Kombinovana]
					+ (Level - 1) * 2;
		}

		public bool AddSkill(Skill skill)
		{
			Skills.Add(skill);
			SkillPoints[skill.Type]-=skill.Rating;
			return true;
		}

		public bool RemoveSkill(Skill skill)
		{
			Skills.Remove(skill);
			SkillPoints[skill.Type]+=skill.Rating;
			return true;
		}

		public bool IncreaseSkill(Skill skill)
		{
			throw new NotImplementedException();
		}

		public int GetFreeSkillPoints(SkillType skillType)
		{
			throw new NotImplementedException();
		}
		public int GetSkillRating(string skillName, string specification = "")
		{
			foreach(Skill item in Skills)
			{
				if(item.Name == skillName)
				{
					if (specification == "")
						return item.Rating;
					if (specification != "" && item.Specifiction == specification)
						return item.Rating;
				}
			}
			return 0;
		}

		internal IWeapon? GetWeapon(Guid? weaponGuid)
		{
			if (weaponGuid == null)
				return null;

			foreach (IWeapon item in MeleeWeapons)
			{
				if (item.Guid == weaponGuid)
					return item;
			}
			foreach (IWeapon item in RangedWeapons)
			{
				if (item.Guid == weaponGuid)
					return item;
			}

			return null;
		}

		public bool ValidateAndClose(out List<string> issues)
		{
			issues = new List<string>();
			if(CharacterIsClosed)
			{
				issues.Add("Postava již uzavřena.");
				return false;
			}

			if(FreeMainAttributePoints != 0 || FreeMinorAttributePoints != 0)
			{
				issues.Add("Chybně využité body vlastností.");
			}

			if((StartMoneyValue+Money)<0)
			{
				issues.Add("Postava nemůže mít negativní peníze.");
			}

			SkillType[] skillTypes = { SkillType.Fyzicka, SkillType.Kombinovana, SkillType.Psychicka };
			if(Level == 1)
			{
				foreach(SkillType item in skillTypes)
				{
					if ((SkillPoints[item] + StartSkillPoints[item] + StartSkillPointsAdjusment[item]) != 0)
					{
						issues.Add("Chybně využité body dovedností.");
						break;
					}
				}
			}
			else
			{
				foreach (SkillType item in skillTypes)
				{
					GetSkillPointLimits(item, out int surePoints, out int variablePoints);
					if ((SkillPoints[item] + surePoints) > 0)
					{
						issues.Add("Nedostatečně využité nutné body dovedností.");
						break;
					}
				}
				foreach (SkillType item in skillTypes)
				{
					GetSkillPointLimits(item, out int surePoints, out int variablePoints);
					if ((SkillPoints[item] + surePoints + variablePoints) < 0)
					{
						issues.Add("Přesažen limit nejasných bodů dovednosti.");
						break;
					}
				}
				int sum = skillTypes.Aggregate(0,(result, x) => result + SkillPoints[x]);
				int max = skillTypes.Aggregate(0, (result, x) => result + StartSkillPoints[x] + StartSkillPointsAdjusment[x])
					+ (Level - 1) * 2;
				if((sum + max) != 0)
				{
					issues.Add("Chybný součet všech dovedností.");
				}
			}

			if ((BackroundPoints - (StartMoneyGrade + StartSkillGrade + OriginGrade)) != 0)
			{
				issues.Add("Chybně rozdělené body zázemí.");
			}


			if (issues.Count > 0)
				return false;

			return CloseCharacter();
		}
		public IList<IWeaponCombination> GetWeaponCombinations()
		{
			List<IWeaponCombination> outList = new List<IWeaponCombination>();

			foreach(WeaponCombinationHolder item in OwnedWeaponCombinations)
			{
				outList.Add(item.GetWeaponCombination(this));
			}

			return outList;
		}

		private bool CloseCharacter()
		{
			throw new NotImplementedException();
		}
	}

	public enum ExceptionalityType
	{
		Vlastnosti = 0,
		Kombinovane = 1,
		Zazemi = 2
	}
}
