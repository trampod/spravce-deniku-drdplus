﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public abstract class WeaponCombinationBase : IWeaponCombination, INotifyPropertyChanged
	{
		public string CombinationName { get; set; }

		public WeaponCombinationBase(string combinationName)
		{
			CombinationName = combinationName;
		}

		public event PropertyChangedEventHandler? PropertyChanged;

		public void OnPropertyChanged(string property)
		{
			//if (this.PropertyChanged != null)
			//	this.PropertyChanged(this, new PropertyChangedEventArgs(property));
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
		}

		public abstract void Recalculate(Character character);
	}
}
