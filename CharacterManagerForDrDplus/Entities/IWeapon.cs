﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public interface IWeapon : IItem
	{
		public Guid Guid { get; }
		public string WeaponCategory { get; }
		public int? NeededStrength { get; } //boj bezezbraně nemá podmínky síly
		public int? Length { get; } //(štít má 0)
		public int Attack { get; }
		public int Damage { get; }
		public string DamageType { get; }
		public int Block { get; } //
		public int? Restriction { get; }//
		public int? MaxStrength { get; }//
		public int? Range { get; } //
		public bool IsTwoHanded { get; }
		public bool IsShield { get; }
	}
}
