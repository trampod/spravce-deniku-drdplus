﻿using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class SingleMeleeWeaponCombination : WeaponCombinationBase
	{
		private MeleeWeapon _weapon;
		
		//public string CombinationName { get; set; }

		public SingleMeleeWeaponHand CombinationHands { get; set; }

		public int? Combat { get; private set; }
		public int? Attack { get; private set; }
		public int? Defense { get; private set; }
		public int? Block { get; private set; }
		public int? Damage { get; private set; }
		public int MissingStrength { get; private set; }
		public string DamageType => _weapon.DamageType;
		public string CombatSkill => _weapon.WeaponCategory;
		public string PresentableHands => CombinationHands switch
		{
			SingleMeleeWeaponHand.dominantni => "V dominantní ruce",
			SingleMeleeWeaponHand.nedominantni => "V druhé ruce",
			SingleMeleeWeaponHand.obouruc => "Obouručně",
			_ => "Žádné"
		};

		//presentables
		public string FullDefense => Block==null?"-":
			((Defense < 0 ? Defense : ("+" + Defense)) 
			+ " (" + (Block < 0 ? Block : ("+" + Block)) + ")");
		public string FullDamage => Damage==null?"-":
			(Damage < 0 ? Damage : ("+" + Damage)) + " " + DamageType;
		public string FullAttack => Attack==null?"-":
			(Attack < 0 ? Attack + "" : ("+" + Attack));
		public string FullCombat => Combat == null ? "-" :
			(Combat < 0 ? Combat + "" : ("+" + Combat));
		public string FullSkill => "Boj se zbraní (" + CombatSkill + ")";
		public string PresentableMissingStrength => MissingStrength > 0 ? MissingStrength.ToString() : "-";

		public SingleMeleeWeaponCombination(string combinationName, MeleeWeapon meleeWeapon, SingleMeleeWeaponHand combinationHands = SingleMeleeWeaponHand.dominantni) : base(combinationName)
		{
			_weapon = meleeWeapon;
			//CombinationName = combinationName;
			CombinationHands = meleeWeapon.IsTwoHanded?SingleMeleeWeaponHand.obouruc:combinationHands;
		}

		public override void Recalculate(Character character)
		{
			int skill = character.GetSkillRating("Boj se zbraní", CombatSkill);

			int thiefBonus = 0;
			if (character.Profession.GetType() == typeof(Thief))
				thiefBonus = ((Thief)character.Profession).GetThiefCombatBonus(CombatSkill);

			MissingStrength = _weapon.NeededStrength==null?0:
				Math.Max(0, (int)_weapon.NeededStrength - character.GetAttribute(AttributeCode.Sil));

			Combat = _weapon.Length! + character.Combat
				+ Penalties.WeaponSkillPenalty.GetCombatPenalty(skill)
				+ Penalties.WeaponStrengthPenalty.GetCombatPenalty(MissingStrength);
			Attack = _weapon.Attack + character.Attack + thiefBonus
				+ Penalties.WeaponSkillPenalty.GetAttackPenalty(skill)
				+ Penalties.WeaponStrengthPenalty.GetAttackPenalty(MissingStrength);
			Defense = character.Defense
				+ Penalties.WeaponStrengthPenalty.GetDefensePenalty(MissingStrength);
			Block = _weapon.Block + thiefBonus
				+ Penalties.WeaponSkillPenalty.GetBlockPenalty(skill);
			Damage = LogTables.BaseDamage(character.GetAttribute(AttributeCode.Sil),_weapon.Damage) + thiefBonus
				+ Penalties.WeaponSkillPenalty.GetDamagePenalty(skill)
				+ Penalties.WeaponStrengthPenalty.GetDamagePenalty(MissingStrength);


			OnPropertyChanged(nameof(FullSkill));
			OnPropertyChanged(nameof(PresentableHands));
			OnPropertyChanged(nameof(FullAttack));
			OnPropertyChanged(nameof(FullCombat));
			OnPropertyChanged(nameof(FullDamage));
			OnPropertyChanged(nameof(FullDefense));
			OnPropertyChanged(nameof(PresentableMissingStrength));
		}
	}

	public enum SingleMeleeWeaponHand
	{
		dominantni,
		nedominantni,
		obouruc
	}
}
