﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public interface IWeaponCombination
	{
		public string CombinationName { get; }

		public void Recalculate(Character character);
	}
}
