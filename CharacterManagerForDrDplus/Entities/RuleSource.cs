﻿using CharacterManagerForDrDplus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class RuleSource
	{
		public string SourceCode { get; }
		public int SourcePage { get; }

		public RuleSource(string sourceCode, int sourcePage)
		{
			SourceCode = sourceCode;
			SourcePage = sourcePage;
		}

		public override string ToString()
		{
			return SourceCode + " " + SourcePage;
		}

		public string GetFullName()
		{
			return GlobalOptions.Instance.GetSourceName(SourceCode);
		}
	}
}
