﻿using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class SingleRangedWeaponCombination : WeaponCombinationBase
	{
		private RangedWeapon _weapon;

		//public string CombinationName { get; set; }

		public int? Combat { get; private set; }
		public int? Attack { get; private set; }
		public int? Defense { get; private set; }
		public int? Block { get; private set; }
		public int? Damage { get; private set; }
		public int? Range { get; private set; }
		public int? Reload { get; private set; }
		public int MissingStrength { get; private set; }
		public string DamageType => _weapon.DamageType;
		public string CombatSkill => _weapon.WeaponCategory;

		//presentables
		public string FullDefense => Block == null ? "-" :
			((Defense < 0 ? Defense : ("+" + Defense))
			+ " (" + (Block < 0 ? Block : ("+" + Block)) + ")");
		public string FullDamage => Damage == null ? "-" :
			(Damage < 0 ? Damage : ("+" + Damage)) + " " + DamageType;
		public string FullAttack => Attack == null ? "-" :
			(Attack < 0 ? Attack + "" : ("+" + Attack));
		public string FullCombat => Combat == null ? "-" :
			(Combat < 0 ? Combat + "" : ("+" + Combat));
		public string FullRange => Range == null ? "-" :
			((Range < 0 ? Range + "" : ("+" + Range)) + " (" + Math.Round( LogTables.DistanceFromBonus((int)Range),2) + " m)");
		public string FullSkill => GetSkillName() + " (" + CombatSkill + ")";
		public string PresentableMissingStrength => MissingStrength > 0 ? MissingStrength.ToString() : "-";

		public SingleRangedWeaponCombination(string combinationName, RangedWeapon meleeWeapon) : base(combinationName)
		{
			_weapon = meleeWeapon;
			//CombinationName = combinationName;
		}

		private string GetSkillName()
		{
			switch (_weapon.WeaponCategory)
			{
				case "Vrhací zbraně":
					return "Boj se zbraní";
				default:
					return "Boj se střelnými zbraněmi";
			}
		}

		public override void Recalculate(Character character)
		{
			int skill = character.GetSkillRating(GetSkillName(), CombatSkill);

			int thiefBonus = 0;
			if (character.Profession.GetType() == typeof(Thief))
				thiefBonus = ((Thief)character.Profession).GetThiefCombatBonus(CombatSkill);

			int strength = _weapon.MaxStrength==null?character.GetAttribute(AttributeCode.Sil):
				Math.Min(character.GetAttribute(AttributeCode.Sil), (int)_weapon.MaxStrength);
			MissingStrength = _weapon.NeededStrength == null?0:
				Math.Max(0, (int)_weapon.NeededStrength - strength);
			Combat = character.Combat + thiefBonus
				+ Penalties.WeaponSkillPenalty.GetCombatPenalty(skill)
				+ Penalties.WeaponStrengthPenalty.GetRangedCombatPenalty(MissingStrength);
			Attack = _weapon.Attack + thiefBonus + (_weapon.WeaponCategory == "Vrhací zbraně" ? character.Attack : character.Ranged)
				+ Penalties.WeaponSkillPenalty.GetAttackPenalty(skill)
				+ Penalties.WeaponStrengthPenalty.GetAttackPenalty(MissingStrength);
			Defense = character.Defense;
			Block = _weapon.Block
				+ Penalties.WeaponSkillPenalty.GetBlockPenalty(skill);
			Range = _weapon.Range + (_weapon.WeaponCategory switch
			{
				"Vrhací zbraně" => character.GetAttribute(AttributeCode.Rch) / 2,
				"Luky" => strength,
				_ => 0
			}) + Penalties.WeaponStrengthPenalty.GetRangePenalty(MissingStrength);
			Reload = Penalties.WeaponStrengthPenalty.GetReloadTime(MissingStrength);

			int damageBonus = _weapon.WeaponCategory switch
			{
				"Kuše" => (int)_weapon.NeededStrength!,
				_ => strength
			};
			Damage = LogTables.BaseDamage(damageBonus, _weapon.Damage) + thiefBonus
				+ Penalties.WeaponSkillPenalty.GetDamagePenalty(skill)
				+ Penalties.WeaponStrengthPenalty.GetDamagePenalty(MissingStrength);


			OnPropertyChanged(nameof(FullSkill));
			OnPropertyChanged(nameof(FullRange));
			OnPropertyChanged(nameof(FullAttack));
			OnPropertyChanged(nameof(FullCombat));
			OnPropertyChanged(nameof(FullDamage));
			OnPropertyChanged(nameof(FullDefense));
			OnPropertyChanged(nameof(PresentableMissingStrength));
		}
	}
}
