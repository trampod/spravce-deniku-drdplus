﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class Information
	{
		public string Subject { get; set; }
		public string Content { get; set; }

		public Information(string subject, string content)
		{
			Subject = subject;
			Content = content;
		}
	}
}
