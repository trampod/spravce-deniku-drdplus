﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public interface IItem
	{
		public string Name { get; }
		public string Category { get; }
		public double Weight { get; }
		public int? Price { get; }
		public int Count { get; set; }
		public RuleSource Source { get; }

		public IItem NewInstance(int count = 1);
		public IItem NewPriceInstance(int newPrice, int count = 1);
	}
}
