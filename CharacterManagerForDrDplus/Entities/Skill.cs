﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class Skill : INotifyPropertyChanged
	{
		public string Name { get; }
		public string Specifiction { get; }
		public bool CombatSkill { get; }
		public SkillType Type {get;}
		private int _rating;
		public int Rating { get => _rating; set { _rating = value; NotifyPropetyChanged(nameof(PresentableName)); } }
		public IList<SkillBonus> SkillBonuses { get; }
		public RuleSource Source { get; }


		[JsonIgnore]
		public string FullName => Name + (Specifiction != "" ? " (" + Specifiction + ")" : "");
		[JsonIgnore]
		public string PresentableName
		{
			get 
			{
				string val = Rating switch
				{
					1 => "I.",
					2 => "II.",
					3 => "III.",
					_ => "0."
				};
				string spec = Specifiction != "" ? " (" + Specifiction + ")" : "";
				return Name + spec + " - " + val; 
			}
		}

		public Skill(string name, string specifiction, bool combatSkill, SkillType type, int rating, IList<SkillBonus> skillBonuses, RuleSource source)
		{
			Name = name;
			Specifiction = specifiction;
			CombatSkill = combatSkill;
			Type = type;
			Rating = rating;
			SkillBonuses = skillBonuses;
			Source = source;
		}

		public event PropertyChangedEventHandler? PropertyChanged;

		public void NotifyPropetyChanged(string property)
		{
			if (this.PropertyChanged != null)
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
		}
	}

	public enum SkillType
	{
		Fyzicka = 0,
		Psychicka = 1,
		Kombinovana = 2
	}
}
