﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class WeaponCombinationHolder
	{
		private string _name;
		private Guid _weaponGuid1;
		private Guid? _weaponGuid2;

		public string Name { get => _name; set => _name = value; }
		public Guid WeaponGuid1 => _weaponGuid1;
		public Guid? WeaponGuid2 => _weaponGuid2;

		public WeaponCombinationHolder(string name, Guid weaponGuid1, Guid? weaponGuid2)
		{
			_name = name;
			_weaponGuid1 = weaponGuid1;
			_weaponGuid2 = weaponGuid2;
		}

		public IWeaponCombination GetWeaponCombination(Character owner)
		{
			IWeapon? weapon1 = owner.GetWeapon(_weaponGuid1);
			IWeapon? weapon2 = owner.GetWeapon(_weaponGuid2);

			if(weapon1 == null && weapon2 == null)
			{

			}
			if(weapon1 != null && weapon2 != null)
			{

			}
			IWeapon weapon = weapon1 != null ? weapon1 : weapon2!;
			switch (weapon)
			{
				case MeleeWeapon:
					return new SingleMeleeWeaponCombination(Name,(MeleeWeapon)weapon);
				case RangedWeapon:
					return new SingleRangedWeaponCombination(Name,(RangedWeapon)weapon);
				default:
					break;
			}
			throw new Exception();
		}

	}
}
