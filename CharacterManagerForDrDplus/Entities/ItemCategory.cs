﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class ItemCategory : INotifyPropertyChanged
	{
		public string CategoryName { get; }
		//public IList<IItem> _items { get; }
		public ObservableCollection<IItem> Items { get; }

		public ItemCategory(string categoryName, IList<IItem> items)
		{
			CategoryName = categoryName;
			//_items = items;
			Items = new ObservableCollection<IItem>(items);
		}

		public event PropertyChangedEventHandler? PropertyChanged;

		public void NotifyPropetyChanged(string property)
		{
			if (this.PropertyChanged != null)
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
		}
	}
}
