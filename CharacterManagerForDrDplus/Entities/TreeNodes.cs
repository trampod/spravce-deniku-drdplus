﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public interface ITreeNode
	{
	}

	public class TreeCategory:ITreeNode
	{
		public ItemCategory ItemCategory { get; }

		public TreeCategory(ItemCategory itemCategory)
		{
			ItemCategory = itemCategory;
		}
		/*public List<TreeCategory> ConvertList(IList<ItemCategory> list)
		{
			List<TreeCategory> outList = new List<TreeCategory>();
			foreach (ItemCategory item in list)
				outList.Add(TreeCategory)
		}*/
	}

	public class TreeItem:ITreeNode
	{
		public IItem Item { get; }

		public TreeItem(IItem item)
		{
			Item = item;
		}
	}
}
