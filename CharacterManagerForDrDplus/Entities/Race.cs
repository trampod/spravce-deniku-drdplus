﻿using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;

namespace CharacterManagerForDrDplus.Entities
{
	public class Race
	{
		public string Name { get; }
		public string FemaleName { get; }
		public Dictionary<AttributeCode, int> RacialAttributes { get; }
		public Dictionary<AttributeCode, int> GenderModifier{ get; }
		public List<string> RacialAbilities { get; }
		public int AverageHeight { get; }
		public int AverageWeight { get; }
		public int BaseSize { get; }
		public string RacialSense { get; }
		public bool Playable { get; }
		public RuleSource Source { get; }

		public Race(string name, string femaleName, Dictionary<AttributeCode,int> racialAttributes, Dictionary<AttributeCode, int> genderModifier, List<string> racialAbilities, string racialSense, int averageHeight,int averageWeight, int baseSize, bool playable, RuleSource source)
		{
			Name = name;
			FemaleName = femaleName;
			RacialAttributes = racialAttributes;
			GenderModifier = genderModifier;
			RacialAbilities = racialAbilities;
			RacialSense = racialSense;
			AverageHeight = averageHeight;
			AverageWeight = averageWeight;
			BaseSize = baseSize;
			Playable = playable;
			Source = source;
		}

		public int GetAttribute(AttributeCode attribute, bool applyGender = false)
		{
			int tmp = 0;

			if (RacialAttributes.ContainsKey(attribute))
				tmp = RacialAttributes[attribute];

			if (applyGender && GenderModifier.ContainsKey(attribute))
				tmp += GenderModifier[attribute];

			return tmp;
		}

		public int GetGenderModifier(AttributeCode attribute)
		{
			if (GenderModifier.ContainsKey(attribute))
				return GenderModifier[attribute];
			return 0;
		}
	}
}