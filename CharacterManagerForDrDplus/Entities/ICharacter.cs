﻿using CharacterManagerForDrDplus.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public interface ICharacter
	{
		public string Name { get; set; }
		public int Level { get; }
		public Profession Profession { get; }
		public Race Race { get; }
		public bool Female { get; }
		public bool ApplyGender { get; }
		public int Size { get; }
		public int Weight { get; }
		public int WeightBonus { get; }
		public int Height { get; }
		public int HeightBonus { get; }
		//public Dictionary<string, int> TotalAttribute { get; }
		//public Dictionary<string, int> DerivedAttributes { get; }
		public int Combat { get; }
		public int Attack { get; }
		public int Ranged { get; }
		public int Defense { get; }
		public int WoundLimit { get; }
		public int FatigueLimit { get; }
		public int Beauty { get; }
		public int Dangerousnes { get; }
		public int Dignity { get; }
		//public int PhysicalSkillPoints { get; }
		//public int MentalSkillPoints { get; }
		//public int CombinedSkillPoints { get; }

		//public Dictionary<SkillType, int> SkillPoints { get; }

		//public Dictionary<SkillType, int> StartSkillPoints { get; set; }

		//public Dictionary<SkillType, int> StartSkillPointsAdjusment { get; set; }
		public int FreeMainAttributePoints { get; }
		public int FreeMinorAttributePoints { get; }
		public string Origin { get; }
		public int Money { get; }

		int GetSkillRating(string skillName, string specification = "");

		//public int TestSkill { get; }
		//public IList<WeaponCombination> OwnedWeaponCombinations { get; }
		//public IList<WeaponCombination> CustomWeaponCombinations { get; }

		public IList<Skill> Skills { get; }

		public int GetAttribute(AttributeCode attribute);
		public int GetFreeSkillPoints(SkillType skillType);
		public bool IncreaseAttribute(AttributeCode Attribute);
		public int GetCompleteInventoryValue();
		public bool AddItem(IItem item, int sellingPrice = 0);
		public bool RemoveItem(IItem item, int sellingPrice = 0);
		public bool AddSkill(Skill skill);
		public bool IncreaseSkill(Skill skill);
		public IList<IWeaponCombination> GetWeaponCombinations();
	}
}
