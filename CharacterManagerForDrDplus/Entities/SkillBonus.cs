﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class SkillBonus
	{
		public string BonusName { get; }
		public int?[] BonusValue { get; }

		public string this[int i] => BonusValueToString(i);

		private string BonusValueToString(int i)
		{
			if (BonusValue.Length < i + 1 || BonusValue[i] == null)
				return "-";
			return (BonusValue[i] > 0 ? "+" : "") + BonusValue[i];
		}

		public SkillBonus(string bonusName, int?[] bonusValue)
		{
			BonusName = bonusName;
			BonusValue = bonusValue;
		}
	}
}
