﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class Shield : IWeapon
	{

		//IItem
		private string _name;
		private string _category;
		private double _weight;
		private int? _price;
		private int _count;
		private RuleSource _source;
		//IWeapon
		private Guid _guid;
		private string _weaponCategory;
		private int? _neededStrength;
		private int _attack;
		private int _damage;
		private string _damageType;
		private int _block;
		private int? _restriction;

		public Shield(string name, string category, double weight, int? price, int count, RuleSource source,
			Guid guid, string weaponCategory, int? neededStrength, int attack, int damage, string damageType, int block, int? restriction)
		{
			_name = name;
			_category = category;
			_weight = weight;
			_price = price;
			_count = count;
			_source = source;

			_guid = guid;
			_weaponCategory = weaponCategory;
			_neededStrength = neededStrength;
			_attack = attack;
			_damage = damage;
			_damageType = damageType;
			_block = block;
			_restriction = restriction;
		}

		//IItem
		public string Name => _name;

		public string Category => _category;

		public double Weight => _weight;

		public int? Price => _price;

		public int Count { get => _count; set => _count = value > 0 ? value : 0; }

		public RuleSource Source => _source;


		//IWeapon
		public Guid Guid => _guid;

		public string WeaponCategory => _weaponCategory;

		public int? NeededStrength => _neededStrength;

		[JsonIgnore]
		public int? Length => 0;

		public int Attack => _attack;

		public int Damage => _damage;

		public string DamageType => _damageType;

		public int Block => _block;

		public int? Restriction => _restriction;

		[JsonIgnore]
		public int? MaxStrength => null;

		[JsonIgnore]
		public int? Range => null;

		[JsonIgnore]
		public bool IsTwoHanded => false;

		[JsonIgnore]
		public bool IsShield => true;

		public IItem NewInstance(int count = 1)
		{
			return new Shield(Name, Category, Weight, Price, count, Source, Guid.NewGuid(), WeaponCategory, NeededStrength, Attack, Damage, DamageType, Block, Restriction);
		}

		public IItem NewPriceInstance(int newPrice, int count = 1)
		{
			return new Shield(Name, Category, Weight, newPrice, count, Source, Guid.NewGuid(), WeaponCategory, NeededStrength, Attack, Damage, DamageType, Block, Restriction);
		}
	}
}
