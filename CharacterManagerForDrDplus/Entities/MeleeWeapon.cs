﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class MeleeWeapon : IWeapon
	{
		
		//IItem
		private string _name;
		private string _category;
		private double _weight;
		private int? _price;
		private int _count;
		private RuleSource _source;
		//IWeapon
		private Guid _guid;
		private string _weaponCategory;
		private int? _neededStrength;
		private int? _length;
		private int _attack;
		private int _damage;
		private string _damageType;
		private int _block;
		private bool _isTwoHanded;

		public MeleeWeapon(string name, string category, double weight, int? price, int count, RuleSource source,
			Guid guid, string weaponCategory, int? neededStrength, int? length, int attack, int damage, string damageType, int block, bool isTwoHanded)
		{
			_name = name;
			_category = category;
			_weight = weight;
			_price = price;
			_count = count;
			_source = source;

			_guid = guid;
			_weaponCategory = weaponCategory;
			_neededStrength = neededStrength;
			_length = length;
			_attack = attack;
			_damage = damage;
			_damageType = damageType;
			_block = block;
			_isTwoHanded = isTwoHanded;
		}

		//IItem
		public string Name => _name;

		public string Category => _category;

		public double Weight => _weight;

		public int? Price => _price;

		public int Count { get => _count; set => _count = value > 0 ? value : 0; }

		public RuleSource Source => _source;


		//IWeapon
		public Guid Guid => _guid;

		public string WeaponCategory => _weaponCategory;

		public int? NeededStrength => _neededStrength;

		public int? Length => _length;

		public int Attack => _attack;

		public int Damage => _damage;

		public string DamageType => _damageType;

		public int Block => _block;

		[JsonIgnore]
		public int? Restriction => null;

		[JsonIgnore]
		public int? MaxStrength => null;

		[JsonIgnore]
		public int? Range => null;

		public bool IsTwoHanded => _isTwoHanded;

		[JsonIgnore]
		public bool IsShield => false;

		public IItem NewInstance(int count = 1)
		{
			return new MeleeWeapon(Name, Category, Weight, Price, count, Source, Guid.NewGuid(), WeaponCategory, NeededStrength, Length, Attack, Damage, DamageType, Block, IsTwoHanded);
		}

		public IItem NewPriceInstance(int newPrice, int count = 1)
		{
			return new MeleeWeapon(Name, Category, Weight, newPrice, count, Source, Guid.NewGuid(), WeaponCategory, NeededStrength, Length, Attack, Damage, DamageType, Block, IsTwoHanded);
		}
	}
}
