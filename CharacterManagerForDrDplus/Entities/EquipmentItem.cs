﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.Entities
{
	public class EquipmentItem : IItem
	{
		//IItem
		private string _name;
		private string _category;
		private double _weight;
		private int? _price;
		private int _count;
		private RuleSource _source;

		public EquipmentItem(string name, string category, double weight, int? price, int count, RuleSource source)
		{ 
			_name = name;
			_category = category;
			_weight = weight;
			_price = price;
			_count = count;
			_source = source;
		}

		public string Name => _name;

		public string Category => _category;

		public double Weight => _weight;

		public int? Price => _price;

		public int Count { get => _count; set => _count = value > 0 ? value : 0; }

		public RuleSource Source => _source;

		public IItem NewInstance(int count = 1)
		{
			return new EquipmentItem(_name, _category, _weight, _price, count, _source);
		}

		public IItem NewPriceInstance(int newPrice, int count = 1)
		{
			return new EquipmentItem(_name, _category, _weight, newPrice, count, _source);
		}
	}
}
