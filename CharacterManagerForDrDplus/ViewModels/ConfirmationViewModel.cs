﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class ConfirmationViewModel : ViewModelBase
	{
		public string Title { get; }
		private string _question;
		public string Note { get; }
		public string Question => "Opravdu chcete " + _question + "?";
		

		public ReactiveCommand<Unit, ResultViewModel> ConfirmationOK { get; }
		public ReactiveCommand<Unit, ResultViewModel> ConfirmationCancel { get; }


		public ConfirmationViewModel(string title, string question, string note = "")
		{
			Title = title;
			_question = question;
			Note = note;


			ConfirmationOK = ReactiveCommand.Create(() =>
			{
				return new ResultViewModel(true);
			});

			ConfirmationCancel = ReactiveCommand.Create(() =>
			{
				return new ResultViewModel(false);
			});
		}
	}
}
