﻿using CharacterManagerForDrDplus.Entities;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class InformationDisplayViewModel : ViewModelBase
	{
		private InformationViewModel _selectedInformation;
		public InformationViewModel SelectedInformation 
		{
			get => _selectedInformation;
			set
			{
				if(SetAndRaiseIfChanged(() => _selectedInformation, () => _selectedInformation = value, value))
				{

				}
			}
		}

		public IList<InformationViewModel> InformationList { get; }
		public string Title { get; }

		public ResultViewModel PlaceHolderResult => new ResultViewModel(true);

		//public ICommand CloseCommand { get; }
		public ReactiveCommand<Unit, ResultViewModel> CloseCommand { get; }

		public InformationDisplayViewModel(string title, IList<Information> informations)
		{
			Title = title;
			InformationList = new List<InformationViewModel>();

			foreach(Information item in informations)
			{
				InformationList.Add(new InformationViewModel(item));
			}

			_selectedInformation = InformationList[0];


			CloseCommand = ReactiveCommand.Create(() =>
			{
				return PlaceHolderResult;
			});
		}
	}
}
