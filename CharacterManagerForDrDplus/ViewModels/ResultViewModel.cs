﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class ResultViewModel : ViewModelBase
	{
		public bool Value { get; }

		public ResultViewModel(bool resultValue)
		{
			Value = resultValue;
		}
	}
}
