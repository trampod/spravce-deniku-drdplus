﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using CharacterManagerForDrDplus.Services;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class AddSkillViewModel : ViewModelBase
	{
		private readonly static SkillData[] _empty = new SkillData[0];

		public IList<SkillData> CategoryItems
		{
			get
			{
				/*if (IsSearching)
				{
					return _searchResult;
				}
				else*/
				if (!String.IsNullOrWhiteSpace(_categoryName) && _itemList.TryGetValue(_categoryName, out IList<SkillData>? category))
				{
					return category;
				}
				else
				{
					return _empty;
				}
			}
		}

		private Dictionary<string, IList<SkillData>> _itemList;

		public IReadOnlyCollection<String> CategoryList
		{
			get
			{
				return _itemList.Keys;
			}
		}

		private IReadOnlyCollection<IItem> _allItems;
		private IList<IItem> _searchResult;


		private string _categoryName = "";
		public string CategoryName
		{
			get
			{
				return _categoryName;
			}
			set
			{
				if (value != _categoryName)
				{
					_categoryName = value;
					NotifySingle(this, nameof(CategoryItems),
					nameof(SelectedSkill));
				}
			}
		}

		private string _searchValue = "";
		public string SearchValue
		{
			get => _searchValue;
			set
			{
			}
		}

		public bool IsSearching => _searchValue != "";

		public SkillData? _selectedSkill;
		public SkillData? SelectedSkill
		{
			get => _selectedSkill;
			set
			{
				if (value != _selectedSkill)
				{
					_selectedSkill = value;
					_specification = "";
					NotifySingle(this, nameof(SelectedSkill),
						nameof(SkillName), nameof(SkillCategory), nameof(SkillSource), nameof(SkillSourceFull),
						nameof(SkillBonuses),
						nameof(Specifiable),
						nameof(FreeSpecification),
						nameof(SelectedSpecification),
						nameof(SpecificationList),
						nameof(SpecificationText),
						nameof(CanAdd));
				}
			}
		}

		public string SkillName => SelectedSkill == null ? "" : SelectedSkill.Name + (!SelectedSkill.Specifiable ? "" : (" (" +(_specification==""?"?":_specification)+")"));

		public string SkillCategory
		{
			get
			{
				return SelectedSkill == null ? "" :
					SelectedSkill.Type switch
					{
						SkillType.Fyzicka => "Fyzická",
						SkillType.Psychicka => "Psychická",
						SkillType.Kombinovana => "Kombinovaná",
						_ => "neznámá"
					};
			}
		}

		public string SkillSource => SelectedSkill == null ? "" : SelectedSkill.Source.ToString();

		public string SkillSourceFull => SelectedSkill == null ? "" : SelectedSkill.Source.GetFullName();

		public IList<SkillBonus>? SkillBonuses => SelectedSkill == null ? null : SelectedSkill.SkillBonuses;

		public bool Specifiable => SelectedSkill == null ? false : SelectedSkill.Specifiable;
		public bool FreeSpecification => (SelectedSkill == null || !SelectedSkill.Specifiable) ? false : SelectedSkill.Specifictions.Count==0;

		public IList<string>? SpecificationList => SelectedSkill == null ? null : SelectedSkill.Specifictions;

		public string _specification;
		public string SelectedSpecification
		{
			get => _specification;
			set
			{
				if(value != _specification)
				{
					_specification = value;
					NotifySingle(this, nameof(CanAdd),nameof(SkillName));
				}
			}
		}
		public string SpecificationText
		{
			get => _specification;
			set
			{
				if(value != _specification)
				{
					_specification = value;
					NotifySingle(this, nameof(CanAdd),nameof(SkillName));
				}
			}
		}
		public bool CanAdd => SelectedSkill == null ? false : (
			!Specifiable ? true : _specification != "");

		public ReactiveCommand<Unit, SkillViewModel?> AddSkillCommand { get; }
		public ReactiveCommand<Unit, SkillViewModel?> AddSkillCancel { get; }

		private SkillViewModel? SelectedSkillViewModel
		{
			get => new SkillViewModel(SelectedSkill, _specification);
		}

		private SkillViewModel? EmptySkillViewModel
		{
			get => new SkillViewModel(null,"");
		}

		public AddSkillViewModel(HashSet<string> skillBlacklist)
		{
			_itemList = new Dictionary<string, IList<SkillData>>();
			SkillManager sm = SkillManager.GetInstance();
			IList<SkillData> tmp = sm.GetAllSkills();
			IList<SkillData> p = new List<SkillData>();
			IList<SkillData> m = new List<SkillData>();
			IList<SkillData> c = new List<SkillData>();
			foreach (SkillData item in tmp)
			{
				if (!skillBlacklist.Contains(item.Name))
				{
					switch (item.Type)
					{
						case SkillType.Fyzicka:
							p.Add(item);
							break;
						case SkillType.Psychicka:
							m.Add(item);
							break;
						case SkillType.Kombinovana:
							c.Add(item);
							break;
						default:
							break;
					}
				}
			}
			_itemList.Add(RuleTables.SkillTypeToString(SkillType.Fyzicka), p);
			_itemList.Add(RuleTables.SkillTypeToString(SkillType.Psychicka), m);
			_itemList.Add(RuleTables.SkillTypeToString(SkillType.Kombinovana), c);

			CategoryName = RuleTables.SkillTypeToString(SkillType.Fyzicka);


			AddSkillCommand = ReactiveCommand.Create(() =>
			{
				return SelectedSkillViewModel;
			});

			AddSkillCancel = ReactiveCommand.Create(() =>
			{
				return EmptySkillViewModel;
			});
		}
	}
}
