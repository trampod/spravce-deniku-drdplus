﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class AddThiefSkillViewModel : ViewModelBase
	{
		private readonly static ThiefSkill[] _empty = new ThiefSkill[0];

		public IList<ThiefSkill> CategoryItems
		{
			get
			{
				/*if (IsSearching)
				{
					return _searchResult;
				}
				else*/
				if (!String.IsNullOrWhiteSpace(_categoryName) && _itemList.TryGetValue(_categoryName, out IList<ThiefSkill>? category))
				{
					return category;
				}
				else
				{
					return _empty;
				}
			}
		}

		private Dictionary<string, IList<ThiefSkill>> _itemList;

		public IReadOnlyCollection<String> CategoryList
		{
			get
			{
				return _itemList.Keys;
			}
		}
		private string _categoryName = "";
		public string CategoryName
		{
			get
			{
				return _categoryName;
			}
			set
			{
				if (value != _categoryName)
				{
					_categoryName = value;
					NotifySingle(this, nameof(CategoryItems),
					nameof(SelectedThiefSkill));
				}
			}
		}

		private string _searchValue = "";
		public string SearchValue
		{
			get => _searchValue;
			set
			{
			}
		}

		public bool IsSearching => _searchValue != "";

		public ThiefSkill? _selectedThiefSkill;
		public ThiefSkill? SelectedThiefSkill
		{
			get => _selectedThiefSkill;
			set
			{
				if (value != _selectedThiefSkill)
				{
					_selectedThiefSkill = value;
					NotifyEverything();
				}
			}
		}




		public string ThiefSkillName => _selectedThiefSkill == null ? "" : _selectedThiefSkill.Name;
		public string ThiefSkillCategory => _selectedThiefSkill == null ? "" : _selectedThiefSkill.Category;
		public string ThiefSkillAttributes => _selectedThiefSkill == null ? "" : string.Join(", ", _selectedThiefSkill.Attributes);
		public string ThiefSkillDuration => _selectedThiefSkill == null ? "" : _selectedThiefSkill.Duration;
		public string ThiefSkillConcentration => _selectedThiefSkill == null ? "" : _selectedThiefSkill.Concentration;
		public string ThiefSkillRequirements => _selectedThiefSkill == null ? "" : _selectedThiefSkill.PresentableRequirements(1);
		public string ThiefSkillBonusPerLevel => _selectedThiefSkill == null ? "" : "+" + _selectedThiefSkill.BonusPerLevel;
		public string ThiefSkillMasterBonus => _selectedThiefSkill == null ? "" : _selectedThiefSkill.MasterBonus;
		public string ThiefSkillSource => _selectedThiefSkill == null ? "" : _selectedThiefSkill.Source.ToString();
		public string ThiefSkillSourceFull => _selectedThiefSkill == null ? "" : _selectedThiefSkill.Source.GetFullName();
		public string ThiefSkillGMSource => _selectedThiefSkill == null ? "" :
			(_selectedThiefSkill.GMSource == null ? "-" : _selectedThiefSkill.GMSource.ToString());
		public string ThiefSkillGMSourceFull => _selectedThiefSkill == null ? "" :
			(_selectedThiefSkill.GMSource == null ? "" : _selectedThiefSkill.GMSource.GetFullName());





		public ReactiveCommand<Unit, ThiefSkillViewModel?> AddThiefSkillCommand { get; }
		public ReactiveCommand<Unit, ThiefSkillViewModel?> AddThiefSkillCancel { get; }

		private ThiefSkillViewModel? SelectedThiefSkillViewModel
		{
			get => new ThiefSkillViewModel(_selectedThiefSkill);
		}

		private ThiefSkillViewModel? EmptyThiefSkillViewModel
		{
			get => new ThiefSkillViewModel(null);
		}

		public AddThiefSkillViewModel(HashSet<string> skillBlacklist)
		{
			_itemList = new Dictionary<string, IList<ThiefSkill>>();
			ThiefData td = (ThiefData)ProfessionManager.GetInstance().GetProfessionData(typeof(Thief));

			foreach(string key in td.ThiefSkillCategories.Keys)
			{
				List<ThiefSkill> data = td.ThiefSkillCategories[key].Where(x => !skillBlacklist.Contains(x.Name)).ToList();
				if (data.Count > 0)
					_itemList.Add(key, data);
			}

			_categoryName = _itemList.Keys.First();


			AddThiefSkillCommand = ReactiveCommand.Create(() =>
			{
				return SelectedThiefSkillViewModel;
			});

			AddThiefSkillCancel = ReactiveCommand.Create(() =>
			{
				return EmptyThiefSkillViewModel;
			});
		}
	}
}
