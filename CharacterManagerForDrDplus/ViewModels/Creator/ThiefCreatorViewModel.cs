﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using CharacterManagerForDrDplus.Services;
using ReactiveUI;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Windows.Input;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class ThiefCreatorViewModel : CharacterViewModelBase
	{
		private Thief profession;

		private ObservableCollection<ThiefSkillCategory> _allThiefSkills;
		public ObservableCollection<ThiefSkillCategory> AllThiefSkills => _allThiefSkills;
		public ThiefSkill? _selectedThiefSkill;
		public ThiefSkill? SelectedThiefSkill 
		{
			get => _selectedThiefSkill;
			set
			{
				if(value != _selectedThiefSkill)
				{
					_selectedThiefSkill = value;

					NotifyEverything();
				}
			}
		}


		public string Profipoints => (profession.Profipoints + Character.Level * 6 + 2) + "/" + (Character.Level * 6 + 2);

		public string ThiefSkillName => _selectedThiefSkill == null ? "" : _selectedThiefSkill.Name;
		public string ThiefSkillCategory => _selectedThiefSkill == null ? "" : _selectedThiefSkill.Category;
		public string ThiefSkillAttributes => _selectedThiefSkill == null ? "" : string.Join(", ", _selectedThiefSkill.Attributes);
		public string ThiefSkillDuration => _selectedThiefSkill == null ? "" : _selectedThiefSkill.Duration;
		public string ThiefSkillConcentration => _selectedThiefSkill == null ? "" : _selectedThiefSkill.Concentration;
		public string ThiefSkillRequirements => _selectedThiefSkill == null ? "" : _selectedThiefSkill.PresentableRequirements(_selectedThiefSkill.Rating);
		public int ThiefSkillRating
		{
			get
			{
				return _selectedThiefSkill == null ? 0 : _selectedThiefSkill.Rating;
			}
			set
			{
				if(_selectedThiefSkill != null)
				{
					if(_selectedThiefSkill.Rating != value)
					{
						profession.Profipoints += _selectedThiefSkill.GetPrice();
						_selectedThiefSkill.Rating = value;
						if (_selectedThiefSkill.Rating < 3 && _selectedThiefSkill.Mastered)
							_selectedThiefSkill.Mastered = false;
						profession.Profipoints -= _selectedThiefSkill.GetPrice();
						profession.UpdateThiefManeuvers(Character);
						Character.UpdateDerivedValues();
						NotifyOthers(this, "SelectedCombination","CombatTotal");
						NotifySingle(this, nameof(ThiefSkillBonus), nameof(ThiefSkillMastered),
							nameof(Profipoints), nameof(ThiefSkillMasteredEnabled), nameof(ThiefSkillRequirements), 
							nameof(ThiefManeuvers));
					}
				}
			}
		}
		public bool ThiefSkillMastered
		{
			get
			{
				return _selectedThiefSkill == null ? false : _selectedThiefSkill.Mastered;
			}
			set
			{
				if(_selectedThiefSkill != null)
				{
					if(_selectedThiefSkill.Mastered != value)
					{
						profession.Profipoints += _selectedThiefSkill.GetPrice();
						_selectedThiefSkill.Mastered = value;
						if (_selectedThiefSkill.Rating < 3 && _selectedThiefSkill.Mastered)
							_selectedThiefSkill.Mastered = false;
						profession.Profipoints -= _selectedThiefSkill.GetPrice();
						profession.UpdateThiefManeuvers(Character);
						NotifySingle(this, nameof(ThiefSkillMastered),nameof(Profipoints),nameof(ThiefManeuvers));
					}
				}
			}
		}
		public bool ThiefSkillMasteredEnabled => _selectedThiefSkill == null ? false : (_selectedThiefSkill.Rating == 3);
		public string ThiefSkillBonus => _selectedThiefSkill == null ? "" : "+" + (_selectedThiefSkill.Rating * _selectedThiefSkill.BonusPerLevel);
		public string ThiefSkillMasterBonus => _selectedThiefSkill == null ? "" : _selectedThiefSkill.MasterBonus;
		public bool ThiefSkillMasterBonusEnabled => _selectedThiefSkill == null ? false : _selectedThiefSkill.Mastered;
		public string ThiefSkillSource => _selectedThiefSkill == null ? "" : _selectedThiefSkill.Source.ToString();
		public string ThiefSkillSourceFull => _selectedThiefSkill == null ? "" : _selectedThiefSkill.Source.GetFullName();
		public string ThiefSkillGMSource => _selectedThiefSkill == null ? "" :
			(_selectedThiefSkill.GMSource == null ? "-" : _selectedThiefSkill.GMSource.ToString());
		public string ThiefSkillGMSourceFull => _selectedThiefSkill == null ? "" :
			(_selectedThiefSkill.GMSource == null ? "" : _selectedThiefSkill.GMSource.GetFullName());


		public List<RollType>? ThiefSkillsChecks => SelectedThiefSkill == null ? null : SelectedThiefSkill.BaseRolls;

		public string GuildName
		{
			get => profession.GuildName;
			set => profession.GuildName = value;
		}
		public string GuildMentor 
		{
			get => profession.GuildMentor;
			set => profession.GuildMentor = value;
		}
		public string GuildPosition
		{
			get => profession.GuildPosition;
			set => profession.GuildPosition = value;
		}
		public List<string> GuildPositions => profession.GetGuildPositions();



		public List<ThiefManeuver> ThiefManeuvers => profession.ThiefManeuvers;
		private ThiefManeuver? _selectedThiefManeuver;
		public ThiefManeuver? SelectedThiefManeuver
		{
			get => _selectedThiefManeuver;
			set
			{
				if(value!=SelectedThiefManeuver)
				{
					_selectedThiefManeuver = value;
					NotifySingle(this, nameof(ThiefManeuverName),nameof(ThiefManeuverWeapon),
						nameof(ThiefManeuverAdvantage),nameof(ThiefManeuverPrerequisites),nameof(ThiefManeuverSource));
				}
			}
		}

		public string ThiefManeuverName => _selectedThiefManeuver == null ? "" : _selectedThiefManeuver.Name;
		public string ThiefManeuverWeapon => _selectedThiefManeuver == null ? "" : _selectedThiefManeuver.Weapons;
		public string ThiefManeuverAdvantage => _selectedThiefManeuver == null ? "" : _selectedThiefManeuver.GetAdvantageString();
		public string ThiefManeuverPrerequisites => _selectedThiefManeuver == null ? "" : _selectedThiefManeuver.GetPresentableRequirments();
		public string ThiefManeuverSource => _selectedThiefManeuver == null ? "" : _selectedThiefManeuver.Source.ToString();



		public void AddThiefSkill()
		{
			var tmp = ((ThiefData)ProfessionManager.GetInstance().GetProfessionData(profession)).ThiefSkillCategories;
			foreach(string item in tmp.Keys)
			{
				AllThiefSkills.Add(new Models.ThiefSkillCategory(item, tmp[item]));
			}
		}


		public Interaction<AddThiefSkillViewModel, ThiefSkillViewModel?> ShowAddThiefSkillDialog { get; }
		public ICommand AddThiefSkillCommand { get; }
		public Interaction<ConfirmationViewModel, ResultViewModel?> ShowRemoveThiefSkillDialog { get; }
		public ICommand RemoveThiefSkillCommand { get; }

		public ThiefCreatorViewModel(Character createdCharacter) : base(createdCharacter)
		{
			profession = (Thief)createdCharacter.Profession;
			_allThiefSkills = FillAllThiefSkills();
			//ThiefManeuvers = new ObservableCollection<ThiefManeuver>(profession.ThiefManeuvers);

			ShowAddThiefSkillDialog = new Interaction<AddThiefSkillViewModel, ThiefSkillViewModel?>();
			ShowRemoveThiefSkillDialog = new Interaction<ConfirmationViewModel, ResultViewModel?>();

			AddThiefSkillCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				HashSet<string> skillBlacklist = new HashSet<string>();
				foreach (ThiefSkill item in profession.ThiefSkills)
				{
					skillBlacklist.Add(item.Name);
				}

				AddThiefSkillViewModel atsvm = new AddThiefSkillViewModel(skillBlacklist);

				ViewModelMessenger.Register(atsvm);

				ThiefSkillViewModel? result = await ShowAddThiefSkillDialog.Handle(atsvm);

				if (result != null && result.ThiefSkillData != null)
				{
					ThiefSkill newSkill = result.ThiefSkillData.NewInstance();
					if(profession.AddThiefSkill(newSkill,Character))
					{
						Character.UpdateDerivedValues();
						NotifyOthers(this, "SelectedCombination", "CombatTotal");
						AddThiefSkillToDisplay(newSkill);
						SelectedThiefSkill = newSkill;
						NotifySingle(this, nameof(Profipoints), nameof(SelectedThiefSkill),
							nameof(ThiefManeuvers));
					}
				}

				ViewModelMessenger.Unregister(atsvm);
			});

			RemoveThiefSkillCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				if (SelectedThiefSkill != null)
				{
					ConfirmationViewModel cnf_vm = new ConfirmationViewModel("Smazat dovednost?", "smazat dovednost " + SelectedThiefSkill.Name);

					ViewModelMessenger.Register(cnf_vm);

					ResultViewModel? result = await ShowRemoveThiefSkillDialog.Handle(cnf_vm);

					if (result != null && result.Value)
					{
						if(profession.RemoveThiefSkill(SelectedThiefSkill,Character))
						{
							Character.UpdateDerivedValues();
							NotifyOthers(this, "SelectedCombination", "CombatTotal");
							RemoveThiefSkillFromDisplay(SelectedThiefSkill);
							SelectedThiefSkill = null;
							NotifyEverything();
							//NotifySingle(this, nameof(Profipoints), nameof(SelectedThiefSkill), 
							//	nameof(ThiefManeuvers));
						}
					}

					ViewModelMessenger.Unregister(cnf_vm);
				}
			});
		}

		public override void SetCharacter(Character newCharacter)
		{
			base.SetCharacter(newCharacter);

			profession = (Thief)newCharacter.Profession;

			profession.UpdateThiefManeuvers(Character);
			_allThiefSkills = FillAllThiefSkills();
			SelectedThiefSkill = null;
			SelectedThiefManeuver = null;
		}

		private ObservableCollection<ThiefSkillCategory> FillAllThiefSkills()
		{
			ObservableCollection<ThiefSkillCategory> newAllThiefSkills = new ObservableCollection<ThiefSkillCategory>();

			foreach (ThiefSkill item in profession.ThiefSkills)
			{
				AddThiefSkillToDisplay(item, newAllThiefSkills);
			}

			return newAllThiefSkills;
		}

		private void AddThiefSkillToDisplay(ThiefSkill addThiefSkill)
		{
			AddThiefSkillToDisplay(addThiefSkill, AllThiefSkills);
		}

		private void AddThiefSkillToDisplay(ThiefSkill addThiefSkill, ObservableCollection<ThiefSkillCategory> targetCollection)
		{
			foreach (ThiefSkillCategory item in targetCollection)
			{
				string category = addThiefSkill.Category;
				if (item.CategoryName == category)
				{
					item.Items.Add(addThiefSkill);
					return;
				}
			}
			targetCollection.Add(new ThiefSkillCategory(addThiefSkill.Category, new List<ThiefSkill>() { addThiefSkill }));
		}

		private void RemoveThiefSkillFromDisplay(ThiefSkill removeThiefSkill)
		{
			foreach (ThiefSkillCategory item in AllThiefSkills)
			{
				string category = removeThiefSkill.Category;
				if (item.CategoryName == category)
				{
					item.Items.Remove(removeThiefSkill);
					if (item.Items.Count == 0)
						AllThiefSkills.Remove(item);
					return;
				}
			}
		}
	}
}