﻿using Avalonia.Controls;
using Avalonia.VisualTree;
using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using CharacterManagerForDrDplus.Services;
using CharacterManagerForDrDplus.Views;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Windows.Input;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class InventoryCreatorViewModel : CharacterViewModelBase
	{
		public bool CharacterIsClosed => Character.CharacterIsClosed;

		public string SelectedCategory;
		public List<string> CategorySelection;

		private IItem? _selected;
		public IItem? SelectedItem {
			get
			{
				return _selected;
			}
			set
			{
				_selected = value;
				NotifyAll(nameof(ItemName), nameof(ItemCategory), nameof(ItemWeight), nameof(ItemPrice), nameof(ItemCount), nameof(ItemSource), nameof(ItemSourceFull), nameof(SelectedItem));
			}
		}

		public string ItemName => SelectedItem == null ? "" : SelectedItem.Name;
		public string ItemCategory => SelectedItem == null ? "" : SelectedItem.Category;
		public string ItemWeight => SelectedItem == null ? "" : Math.Round(SelectedItem.Weight, 2).ToString();
		public string ItemPrice => SelectedItem == null ? "" : RuleTables.GetPresentableMoney(SelectedItem.Price);
		public string ItemFullPrice => SelectedItem == null ? "" : RuleTables.GetPresentableMoney(SelectedItem.Price * SelectedItem.Count);
		public int ItemCount
		{
			get
			{
				return SelectedItem == null ? 0 : SelectedItem.Count;
			}
			set
			{
				if(SelectedItem != null)
				{
					if(value != SelectedItem.Count)
					{
						if(Character.CharacterIsClosed)
						{
							SelectedItem.Count = value;
						}
						else
						{
							Character.Money += SelectedItem.Count * (int)SelectedItem.Price!;
							SelectedItem.Count = value;
							Character.Money -= SelectedItem.Count * (int)SelectedItem.Price!;
							NotifySingle(this, nameof(DisplayMoney), nameof(ItemFullPrice));
						}
					}
				}
			}
		}
		public string ItemSource => SelectedItem == null ? "" : SelectedItem.Source.ToString();
		public string ItemSourceFull => SelectedItem == null ? "" : SelectedItem.Source.GetFullName();

		public string DisplayMoney
		{
			get
			{
				int sum = Character.CharacterIsClosed?Character.Money:
					(Character.StartMoneyValue + Character.Money);

				return RuleTables.GetPresentableMoney(sum);
			}
		}

		private ObservableCollection<ItemCategory> _equipmentItems;
		public ObservableCollection<ItemCategory> EquipmentItems
		{
			get
			{
				//return new ObservableCollection<ItemCategory>(ItemManager.GetInstance().GetAllCategories());
				//if (SelectedCategory == "Všechny")
				return _equipmentItems;
				//else
				//	return new ObservableCollection<IItem>(createdCharacter.GetInventoryCategory(SelectedCategory));
			}
		}

		public Interaction<AddItemViewModel, ItemDataViewModel?> ShowAddItemDialog { get; }
		public ICommand AddItemCommand { get; }
		public Interaction<ConfirmationViewModel, ResultViewModel?> ShowRemoveItemDialog { get; }
		public ICommand RemoveItemCommand { get; }
		public Interaction<MessageBoxViewModel, ResultViewModel?> ShowMessageBox { get; }

		public void AddItem()
		{

			//createdCharacter.Inventory["Oblečení"].Add(new EquipmentItem("Šatičky", "Oblečení", 1, null, 1, new RuleSource("nic", 69)));
			NotifyAll(nameof(EquipmentItems));
		}

		public void RemoveItem()
		{
			if (SelectedItem != null)
			{
				Character.Inventory[SelectedItem.Category].Remove((EquipmentItem)SelectedItem);

				NotifyAll(nameof(EquipmentItems));
			}
		}

		public InventoryCreatorViewModel(Character createdCharacter) : base(createdCharacter)
		{
			ShowAddItemDialog = new Interaction<AddItemViewModel, ItemDataViewModel?>();
			ShowRemoveItemDialog = new Interaction<ConfirmationViewModel, ResultViewModel?>();
			ShowMessageBox = new Interaction<MessageBoxViewModel, ResultViewModel?>();
			_equipmentItems = new ObservableCollection<ItemCategory>(createdCharacter.GetInventoryCategories());

			AddItemCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				AddItemViewModel aivm = new AddItemViewModel(CharacterIsClosed);

				ViewModelMessenger.Register(aivm);

				ItemDataViewModel? result = await ShowAddItemDialog.Handle(aivm);

				if (result != null && result.Item != null)
				{
					IItem itemInstance = result.Item.NewPriceInstance(result.BuyingPrice, result.Count);
					if (createdCharacter.AddItem(itemInstance, result.IsBuying ? result.BuyingPrice : 0))
					{
						AddItemToDisplay(itemInstance);
						SelectedItem = itemInstance;
						NotifyEverything();
						//NotifySingle(this, nameof(DisplayMoney));
						NotifyOthers(this, "WeaponCombinations");
					}
					else
					{
						//Window mainWindow = ((IVisual)this).FindAncestorOfType<Window>();
						//await MessageBox.Show(null, "Postava nemá dostatek peněz na koupi. Chybí " + (result.BuyingPrice - createdCharacter.Money) + ".", "Nedostatek peněz", MessageBox.MessageBoxButtons.Ok);
						string message = "Postava nemá dostatek peněz na koupi předmětu\"" + result.Item.Name + "\". Chybí " + RuleTables.GetPresentableMoney(result.BuyingPrice - createdCharacter.Money) + ".";
						var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Nedostatek peněz", message);
						await messageBoxStandardWindow.Show();
					}
				}

				ViewModelMessenger.Unregister(aivm);
			});

			RemoveItemCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				if (SelectedItem != null)
				{
					ConfirmationViewModel cnf_vm = new ConfirmationViewModel("Smazat předmět?", "smazat předmět " + SelectedItem.Name);

					ViewModelMessenger.Register(cnf_vm);

					ResultViewModel? result = await ShowRemoveItemDialog.Handle(cnf_vm);

					if (result != null && result.Value)
					{
						int sellPrice = createdCharacter.CharacterIsClosed ? 0 : (SelectedItem.Price == null ? 0 : (int)SelectedItem.Price);
						createdCharacter.RemoveItem(SelectedItem, sellPrice);
						RemoveItemFromDisplay(SelectedItem);
						SelectedItem = null;
						//NotifySingle(this, nameof(DisplayMoney));
						NotifyEverything();
						NotifyOthers(this, "WeaponCombinations");
					}

					ViewModelMessenger.Unregister(cnf_vm);
				}
			});
		}

		public override void SetCharacter(Character newCharacter)
		{
			base.SetCharacter(newCharacter);

			_equipmentItems = new ObservableCollection<ItemCategory>(newCharacter.GetInventoryCategories());
			SelectedItem = null;
		}

		private void AddItemToDisplay(IItem addItem)
		{
			foreach (ItemCategory item in _equipmentItems)
			{
				if (item.CategoryName == addItem.Category)
				{
					item.Items.Add(addItem);
					//NotifySingle(this, nameof(EquipmentItems));
					return;
				}
			}
			_equipmentItems.Add(new ItemCategory(addItem.Category, new List<IItem>() { addItem }));
			//NotifySingle(this, nameof(EquipmentItems));
		}

		private void RemoveItemFromDisplay(IItem removeItem)
		{
			foreach (ItemCategory item in _equipmentItems)
			{
				if (item.CategoryName == removeItem.Category)
				{
					item.Items.Remove(removeItem);
					if (item.Items.Count == 0)
						_equipmentItems.Remove(item);
					//NotifySingle(this, nameof(EquipmentItems));
					return;
				}
			}
			//_equipmentItems.Add(new ItemCategory(removeItem.Category, new List<IItem>() { removeItem }));
			//NotifySingle(this, nameof(EquipmentItems));
		}
	}
}