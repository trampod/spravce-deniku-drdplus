﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using CharacterManagerForDrDplus.Services;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Windows.Input;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class AttributesCreatorViewModel : CharacterViewModelBase
	{
		CharacterCreatorViewModel _parent;

		private IList<string> ProfessionNameList { get; set; }
		private IDictionary<string,Type> ProfessionList { get; set; }
		private IList<string> RaceNameList { get; set; }
		private IDictionary<string,Race> RaceList { get; set; }
		private IList<string> GenderNameList { get; set; }
		private IDictionary<string,ExceptionalityType> ExceptionalityList { get; set; }

		//private string _name;
		//private int _level;
		//private string _profession;


		public string Name
		{
			get => Character.Name;
			private set
			{
				if (SetAndRaiseIfChanged(() => Character.Name, () => Character.Name = value, value))
				{

				}
				//createdCharacter.Name = value;
				//this.RaiseAndSetIfChanged(ref _name, value);
			}
		}


		public int Level
		{
			get => Character.Level;
			private set
			{
				if (SetAndRaiseIfChanged(() => Character.Level, () => Character.Level = value, value))
				{
					NotifyAll(nameof(MinAgility), nameof(MinStrength), nameof(MinDexterity), nameof(MinWillpower), nameof(MinInteligence), nameof(MinCharisma),
						nameof(MaxAgility), nameof(MaxStrength), nameof(MaxDexterity), nameof(MaxWillpower), nameof(MaxInteligence), nameof(MaxCharisma),
						"ShowMaxSkillPoints", "MaxSkillPoints",
						"SkillPointsPhysical", "SkillPointsMental", "SkillPointsCombined",
						"Profipoints",
						nameof(FirstLevel),nameof(Size),nameof(SizeText));

					foreach (AttributeCode item in Character.BaseAttributes.Keys)
					{
						int max = Character.GetAttributeMax(item);
						if (Character.BaseAttributes[item] > max)
							Character.BaseAttributes[item] = max;
					}

					Character.UpdateDerivedValues();
					NotifyOthers(this, "SelectedCombination");
					NotifyAbsolutelyEveryAttribute();
				}
				//createdCharacter.Level = value;
				//this.RaiseAndSetIfChanged(ref _level, value);
			}
		}

		public bool FirstLevel => Character.Level == 1;

		/*
		public Profession Profession
		{
			get => createdCharacter.Profession;
			private set
			{
				if (SetAndRaiseIfChanged(() => createdCharacter.Profession, () => createdCharacter.Profession = value, value))
				{
					NotifyAll( nameof(ProfessionName),nameof(CombatTotal));
				}

				//createdCharacter.Profession = value;
				//this.RaiseAndSetIfChanged(ref _profession, value); 
			}
		}
		*/

		public string ProfessionName
		{
			get => Character.Profession.Name;
			private set
			{
				if (value == Character.Profession.Name)
					return;

				if (!ProfessionList.ContainsKey(value)) throw new ArgumentException();
				Character.Profession = ProfessionManager.GetInstance().GetProfessionData(ProfessionList[value]).GetNewInstance();
				//createdCharacter.Profession = ProfessionList[value];

				_parent.UpdateProfessionTab();

				NotifyAll( nameof(ProfessionName),nameof(CombatTotal),
					nameof(MinAgility), nameof(MinStrength), nameof(MinDexterity), nameof(MinWillpower), nameof(MinInteligence), nameof(MinCharisma),
					nameof(MaxAgility), nameof(MaxStrength), nameof(MaxDexterity), nameof(MaxWillpower), nameof(MaxInteligence), nameof(MaxCharisma),
					nameof(MainAttributePoints), nameof(MinorAttributePoints),
					nameof(StartMentalSkills),nameof(StartPhysicalSkills),nameof(StartCombinedSkills));

				foreach(AttributeCode item in Character.Profession.MainAttributes)
				{
					if (Character.BaseAttributes[item] == 0)
						Character.BaseAttributes[item] = 1;
				}

				foreach(AttributeCode item in Character.BaseAttributes.Keys)
				{
					int max = Character.GetAttributeMax(item);
					if (Character.BaseAttributes[item] > max)
						Character.BaseAttributes[item] = max;
				}

				Character.StartSkillGrade = Character.StartSkillGrade;
				Character.UpdateDerivedValues();
				//NotifyAbsolutelyEveryAttribute();
				NotifyOthers(this, "SelectedCombination");
				NotifyEverything();


				//Profession = new Fighter(value, AttributeCode.Obr, new string[] { AttributeCode.Obr, AttributeCode.Sil });
			}
		}

		public string Race
		{
			get => Character.Race.Name;
			private set
			{
				if (!RaceList.ContainsKey(value)) throw new ArgumentException();
				Character.Race = RaceList[value];
				Character.Weight = Character.Race.AverageWeight;
				Character.Height = Character.Race.AverageHeight;
				Character.Size = Character.Race.BaseSize + (Character.Female?Character.Race.GetGenderModifier(AttributeCode.Sil):0);
				Character.UpdateDerivedValues();
				//NotifyAbsolutelyEveryAttribute();
				NotifyOthers(this, "SelectedCombination");
				NotifyEverything();
			}
		}

		public bool ShowRacialAbilities => Character.Race.RacialAbilities.Count > 0;

		public string RacialAbilities => string.Join(", ", Character.Race.RacialAbilities);

		public string RaceSource => Character.Race.Source.ToString();

		public string RaceSourceFull => Character.Race.Source.GetFullName();

		public string Gender
		{
			get => Character.Female ? "Žena" : "Muž";
			private set
			{
				Character.Female = (value == "Žena");
				if (Character.ApplyGender)
				{
					int size = Character.Size;
					if (size > MaxSize)
						Character.Size = MaxSize;
					if (size < MinSize)
						Character.Size = MinSize;
				}
				Character.UpdateDerivedValues();
				NotifyOthers(this, "SelectedCombination");
				NotifyEverything();
			}
		}

		public bool ApplyGender
		{
			get => Character.ApplyGender;
			private set
			{
				Character.ApplyGender = value;
				int size = Character.Size;
				if (size > MaxSize)
					Character.Size = MaxSize;
				if (size < MinSize)
					Character.Size = MinSize;
				Character.UpdateDerivedValues();
				NotifyOthers(this, "SelectedCombination");
				NotifyEverything();
			}
		}

		public string Exceptionality
		{
			get
			{
				foreach(string item in ExceptionalityList.Keys)
				{
					if (ExceptionalityList[item] == Character.Exceptionality)
						return item;
				}
				return "";
			}
			set
			{
				if (SetAndRaiseIfChanged(() => Character.Exceptionality, () => Character.Exceptionality = ExceptionalityList[value], ExceptionalityList[value]))
				{
					foreach (AttributeCode item in Character.BaseAttributes.Keys)
					{
						int max = Character.GetAttributeMax(item);
						if (Character.BaseAttributes[item] > max)
							Character.BaseAttributes[item] = max;
					}

					NotifyAll(nameof(BackgroundPoints),
						nameof(AgilityMod), nameof(StrengthMod),nameof(DexterityMod),nameof(WillpowerMod),nameof(InteligenceMod),nameof(CharismaMod),
						nameof(MinAgility), nameof(MinStrength), nameof(MinDexterity), nameof(MinWillpower), nameof(MinInteligence), nameof(MinCharisma),
						nameof(MaxAgility), nameof(MaxStrength), nameof(MaxDexterity), nameof(MaxWillpower), nameof(MaxInteligence), nameof(MaxCharisma),
						nameof(MainAttributePoints), nameof(MinorAttributePoints),
						"SelectedCombination");
				}
			}
		}

		public string BackgroundPoints { get { return (Character.BackroundPoints - Character.OriginGrade - Character.StartMoneyGrade - Character.StartSkillGrade) + " / " + Character.BackroundPoints; } }

		public int OriginGrade
		{
			get => Character.OriginGrade;
			private set
			{
				if (SetAndRaiseIfChanged(() => Character.OriginGrade, () => Character.OriginGrade = value, value))
				{
					NotifyAll(nameof(BackgroundPoints),
						nameof(OriginValue),
						nameof(StartMoneyGrade),nameof(StartMoneyValue),"DisplayMoney",
						nameof(StartSkillGrade),nameof(StartPhysicalSkills), nameof(StartMentalSkills), nameof(StartCombinedSkills));
				}
			}
		}

		public int StartMoneyGrade
		{
			get => Character.StartMoneyGrade;
			private set
			{
				if (SetAndRaiseIfChanged(() => Character.StartMoneyGrade, () => Character.StartMoneyGrade = value, value))
				{
					NotifyAll(nameof(BackgroundPoints),nameof(StartMoneyValue), "DisplayMoney");
				}
			}
		}

		public int StartSkillGrade
		{
			get => Character.StartSkillGrade;
			private set
			{
				if (SetAndRaiseIfChanged(() => Character.StartSkillGrade, () => Character.StartSkillGrade = value, value))
				{
					NotifyAll(nameof(BackgroundPoints),
						nameof(StartPhysicalSkills), nameof(StartMentalSkills), nameof(StartCombinedSkills),
						"SkillPointsPhysical", "SkillPointsMental", "SkillPointsCombined");
				}
			}
		}

		public string OriginValue
		{
			get => Character.Origin;
		}

		public string StartMoneyValue
		{
			get => RuleTables.GetPresentableMoney(Character.StartMoneyValue);
		}

		public int StartPhysicalSkills
		{
			get => Character.StartSkillPoints[SkillType.Fyzicka];
		}

		public int StartMentalSkills
		{
			get => Character.StartSkillPoints[SkillType.Psychicka];
		}

		public int StartCombinedSkills
		{
			get => Character.StartSkillPoints[SkillType.Kombinovana];
		}

		public int Weight
		{
			get => Character.Weight;
			set
			{
				if (SetAndRaiseIfChanged(() => Character.Weight, () => Character.Weight = value, value))
				{
					NotifyAll(nameof(WeightBonus));
				}
			}
		}

		public string WeightBonus { get => "+" + Character.WeightBonus; }

		public int MaxWeight { get=> Character.MaxWeight; }

		public int MinWeight { get=> Character.MinWeight; }

		public int Height
		{
			get => Character.Height;
			set
			{
				if (SetAndRaiseIfChanged(() => Character.Height, () => Character.Height = value, value))
				{
					NotifyAll(nameof(HeightBonus), nameof(CombatTotal), nameof(SpeedTotal),
						"SelectedCombination");
				}
			}
		}

		public string HeightBonus { 
			get
			{
				int bonus = Character.HeightBonus;
				int correction = RuleTables.GetHeightCorrection(bonus);
				return (bonus < 0 ? "" : "+") + bonus + " (" + (correction < 0 ? "" : "+") + correction + ")"; 
			}
		}

		public int MaxHeight { get => Character.MaxHeight; }

		public int MinHeight { get => Character.MinHeight; }

		public string SizeText => (Character.Size >= 0 ? "+" : "") + Character.Size;

		public int Size
		{
			get => Character.Size;
			set
			{
				if (value <= MaxSize && value >= MinSize)
				{
					if (SetAndRaiseIfChanged(() => Character.Size, () => Character.Size = value, value))
					{

					}
				}
			}
		}

		public int MaxSize
		{
			get
			{
				int tmp = Character.Race.BaseSize + ((Character.Female && Character.ApplyGender )? Character.Race.GetGenderModifier(AttributeCode.Sil) : 0)+1;
				
				if (StrengthMod == 0)
					tmp -= 1;
				if (StrengthMod <= 1)
					tmp -= 1;
				return tmp;
			}
		}

		public int MinSize 
		{ 
			get
			{
				int tmp = Character.Race.BaseSize + ((Character.Female && Character.ApplyGender) ? Character.Race.GetGenderModifier(AttributeCode.Sil) : 0) -1;
				int startMax = Character.IsMainAttribute(AttributeCode.Sil) ? 3 : RuleTables.GetStartingMaxAttribute(Character.Exceptionality);

				if (StrengthMod >= Character.GetAttributeMax(AttributeCode.Sil)+2-startMax)
					tmp += 1;
				if (StrengthMod >= Character.GetAttributeMax(AttributeCode.Sil)+1-startMax)
					tmp += 1;
				return tmp;
			}
		}

		public string MainAttributePoints
		{
			get
			{
				int max = Character.Level - 1 + RuleTables.GetStartingMainAttributePoints(Character.Exceptionality);
				int current = Character.FreeMainAttributePoints;
				return current + "/" + max;
			}
		}

		public string MinorAttributePoints
		{
			get
			{
				int max = Character.Level - 1 + RuleTables.GetStartingMinorAttributePoints(Character.Exceptionality);
				int current = Character.FreeMinorAttributePoints;
				return current + "/" + max;
			}
		}

		private void NotifyAbsolutelyEveryAttribute()
		{
			NotifyAll(
				nameof(StrengthMod),
				nameof(CharismaMod),
				nameof(InteligenceMod),
				nameof(AgilityMod),
				nameof(WillpowerMod),
				nameof(DexterityMod),

				nameof(StrengthTotal),
				nameof(CharismaTotal),
				nameof(InteligenceTotal),
				nameof(AgilityTotal),
				nameof(WillpowerTotal),
				nameof(DexterityTotal),

				nameof(ResistenceTotal),
				nameof(EnduranceTotal),
				nameof(SpeedTotal),
				nameof(SensesTotal),

				nameof(CombatTotal),
				nameof(AttackTotal),
				nameof(DefenseTotal),
				nameof(RangedTotal),
				nameof(WoundLimitTotal),
				nameof(FatigueLimitTotal),

				nameof(DignityTotal),
				nameof(BeautyTotal),
				nameof(DangerousnesTotal),

				nameof(Size),
				nameof(MaxSize),
				nameof(MinSize),
				nameof(Height),
				nameof(Weight),
				nameof(MaxHeight),
				nameof(MaxWeight),
				nameof(MinHeight),
				nameof(MinWeight),
				nameof(HeightBonus),
				nameof(WeightBonus),

				nameof(MinAgility), 
				nameof(MinStrength),
				nameof(MinDexterity),
				nameof(MinWillpower),
				nameof(MinInteligence), 
				nameof(MinCharisma),

				nameof(MaxAgility), 
				nameof(MaxStrength), 
				nameof(MaxDexterity),
				nameof(MaxWillpower),
				nameof(MaxInteligence),
				nameof(MaxCharisma),

				nameof(MainAttributePoints),
				nameof(MinorAttributePoints),


				nameof(StartMentalSkills),
				nameof(StartPhysicalSkills),
				nameof(StartCombinedSkills)

				);
		}

		public int StrengthMod
		{
			get => Character.BaseAttributes[AttributeCode.Sil];
			private set
			{
				if (SetAndRaiseIfChanged(() => Character.BaseAttributes[AttributeCode.Sil], () => Character.BaseAttributes[AttributeCode.Sil] = value, value))
				{
					int size = Character.Size;
					if (size > MaxSize)
						Character.Size = MaxSize;
					if (size < MinSize)
						Character.Size = MinSize;
					Character.UpdateDerivedValues();
					NotifyAll(
						nameof(StrengthTotal),
						nameof(ResistenceTotal),nameof(SpeedTotal),nameof(EnduranceTotal),nameof(WoundLimitTotal),nameof(FatigueLimitTotal),
						nameof(DangerousnesTotal),
						nameof(MaxSize),nameof(MinSize),nameof(Size),nameof(SizeText),
						nameof(MainAttributePoints),nameof(MinorAttributePoints),
						"SkillPointsPhysical",
						"SelectedCombination");
				}
			}
		}
		public int AgilityMod
		{
			get => Character.BaseAttributes[AttributeCode.Obr];
			private set
			{
				if (SetAndRaiseIfChanged(() => Character.BaseAttributes[AttributeCode.Obr], () => Character.BaseAttributes[AttributeCode.Obr] = value, value))
				{
					Character.UpdateDerivedValues();
					NotifyAll(
						nameof(AgilityTotal),
						nameof(SpeedTotal),nameof(CombatTotal),nameof(AttackTotal),nameof(DefenseTotal),
						nameof(BeautyTotal),
						nameof(MainAttributePoints), nameof(MinorAttributePoints),
						"SkillPointsPhysical",
						"SelectedCombination");
				}
			}
		}
		public int DexterityMod
		{
			get => Character.BaseAttributes[AttributeCode.Zrc];
			private set
			{
				if (SetAndRaiseIfChanged(() => Character.BaseAttributes[AttributeCode.Zrc], () => Character.BaseAttributes[AttributeCode.Zrc] = value, value))
				{
					Character.UpdateDerivedValues();
					NotifyAll(
						nameof(DexterityTotal),
						nameof(CombatTotal),nameof(RangedTotal),nameof(SensesTotal),
						nameof(BeautyTotal),
						nameof(MainAttributePoints), nameof(MinorAttributePoints),
						"SkillPointsCombined",
						"SelectedCombination");
				}
			}
		}
		public int WillpowerMod
		{
			get => Character.BaseAttributes[AttributeCode.Vol];
			private set
			{
				if (SetAndRaiseIfChanged(() => Character.BaseAttributes[AttributeCode.Vol], () => Character.BaseAttributes[AttributeCode.Vol] = value, value))
				{
					Character.UpdateDerivedValues();
					NotifyAll(
						nameof(WillpowerTotal),
						nameof(EnduranceTotal),nameof(FatigueLimitTotal),
						nameof(DignityTotal),
						nameof(MainAttributePoints), nameof(MinorAttributePoints),
						"SkillPointsMental");
				}
			}
		}
		public int InteligenceMod
		{
			get => Character.BaseAttributes[AttributeCode.Int];
			private set
			{
				if (SetAndRaiseIfChanged(() => Character.BaseAttributes[AttributeCode.Int], () => Character.BaseAttributes[AttributeCode.Int] = value, value))
				{
					Character.UpdateDerivedValues();
					NotifyAll(
						nameof(InteligenceTotal),
						nameof(CombatTotal),
						nameof(DignityTotal),
						nameof(MainAttributePoints), nameof(MinorAttributePoints),
						"SkillPointsMental",
						"SelectedCombination");
				}
			}
		}
		public int CharismaMod
		{
			get => Character.BaseAttributes[AttributeCode.Chr];
			private set
			{
				if (SetAndRaiseIfChanged(() => Character.BaseAttributes[AttributeCode.Chr], () => Character.BaseAttributes[AttributeCode.Chr] = value, value))
				{
					Character.UpdateDerivedValues();
					NotifyAll(
						nameof(CharismaTotal),
						nameof(CombatTotal),
						nameof(DignityTotal),nameof(BeautyTotal),nameof(DangerousnesTotal),
						nameof(MainAttributePoints), nameof(MinorAttributePoints),
						"SkillPointsCombined",
						"SelectedCombination");
				}
			}
		}


		public string StrengthTotal
		{
			get
			{
				int tmp = Character.GetAttribute(AttributeCode.Sil);
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string AgilityTotal
		{
			get
			{
				int tmp = Character.GetAttribute(AttributeCode.Obr);
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string DexterityTotal
		{
			get
			{
				int tmp = Character.GetAttribute(AttributeCode.Zrc);
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string WillpowerTotal
		{
			get
			{
				int tmp = Character.GetAttribute(AttributeCode.Vol);
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string InteligenceTotal
		{
			get
			{
				int tmp = Character.GetAttribute(AttributeCode.Int);
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string CharismaTotal
		{
			get
			{
				int tmp = Character.GetAttribute(AttributeCode.Chr);
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}

		public int MinStrength { get { return Character.GetAttributeMin(AttributeCode.Sil); } }
		public int MinAgility { get { return Character.GetAttributeMin(AttributeCode.Obr); } }
		public int MinDexterity { get { return Character.GetAttributeMin(AttributeCode.Zrc); } }
		public int MinWillpower { get { return Character.GetAttributeMin(AttributeCode.Vol); } }
		public int MinInteligence { get { return Character.GetAttributeMin(AttributeCode.Int); } }
		public int MinCharisma { get { return Character.GetAttributeMin(AttributeCode.Chr); } }

		public int MaxStrength { get { return Character.GetAttributeMax(AttributeCode.Sil); } }
		public int MaxAgility { get { return Character.GetAttributeMax(AttributeCode.Obr); } }
		public int MaxDexterity { get { return Character.GetAttributeMax(AttributeCode.Zrc); } }
		public int MaxWillpower { get { return Character.GetAttributeMax(AttributeCode.Vol); } }
		public int MaxInteligence { get { return Character.GetAttributeMax(AttributeCode.Int); } }
		public int MaxCharisma { get { return Character.GetAttributeMax(AttributeCode.Chr); } }

		public string IsMainStrength { get { return Character.IsMainAttribute(AttributeCode.Sil)?"Bold":"Normal"; } }
		public string IsMainAgility { get { return Character.IsMainAttribute(AttributeCode.Obr) ? "Bold" : "Normal"; } }
		public string IsMainDexterity { get { return Character.IsMainAttribute(AttributeCode.Zrc) ? "Bold" : "Normal"; } }
		public string IsMainWillpower { get { return Character.IsMainAttribute(AttributeCode.Vol) ? "Bold" : "Normal"; } }
		public string IsMainInteligence { get { return Character.IsMainAttribute(AttributeCode.Int) ? "Bold" : "Normal"; } }
		public string IsMainCharisma { get { return Character.IsMainAttribute(AttributeCode.Chr) ? "Bold" : "Normal"; } }


		public string SensesTotal
		{
			get
			{
				int tmp = Character.GetAttribute(AttributeCode.Sms);
				return (tmp < 0 ? tmp.ToString() : "+" + tmp) + (Character.Race.RacialSense==""?"":" (" + Character.Race.RacialSense + " +1)");
			}
		}
		public string ResistenceTotal
		{
			get
			{
				int tmp = Character.GetAttribute(AttributeCode.Odl);
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string EnduranceTotal
		{
			get
			{
				int tmp = Character.GetAttribute(AttributeCode.Vdr);
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string SpeedTotal
		{
			get
			{
				int tmp = Character.GetAttribute(AttributeCode.Rch);
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}


		public string BeautyTotal
		{
			get
			{
				int tmp = Character.Beauty;
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string DangerousnesTotal
		{
			get
			{
				int tmp = Character.Dangerousnes;
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string DignityTotal
		{
			get
			{
				int tmp = Character.Dignity;
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}


		public string CombatTotal
		{
			get
			{
				int tmp = Character.Combat;
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string AttackTotal
		{
			get
			{
				int tmp = Character.Attack;
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string DefenseTotal
		{
			get
			{
				int tmp = Character.Defense;
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string RangedTotal
		{
			get
			{
				int tmp = Character.Ranged;
				return tmp < 0 ? tmp.ToString() : "+" + tmp;
			}
		}
		public string WoundLimitTotal
		{
			get
			{
				int tmp = Character.WoundLimit;
				return (tmp < 0 ? tmp.ToString() : "+" + tmp) + " (" + LogTables.FakeValueFromBonus(tmp) + " bodů)";
			}
		}
		public string FatigueLimitTotal
		{
			get
			{
				int tmp = Character.FatigueLimit;
				return (tmp < 0 ? tmp.ToString() : "+" + tmp) + " (" + LogTables.FakeValueFromBonus(tmp) + " bodů)";
			}
		}


		public ICommand FinishCharacterCommand { get; }
		public Interaction<InformationDisplayViewModel, ResultViewModel> ShowInformationDisplayDialog { get; }


		public AttributesCreatorViewModel(Character character, CharacterCreatorViewModel parent) : base(character)
		{
			_parent = parent;

			ShowInformationDisplayDialog = new Interaction<InformationDisplayViewModel, ResultViewModel>();

			ProfessionList = new Dictionary<string,Type>();
			ProfessionList.Add("Bojovník", typeof(Fighter));
			ProfessionList.Add("Čaroděj", typeof(Wizard));
			ProfessionList.Add("Zloděj", typeof(Thief));
			ProfessionList.Add("Theurg", typeof(Theurg));
			ProfessionList.Add("Hraničář", typeof(Ranger));
			ProfessionList.Add("Kněz", typeof(Priest));

			ProfessionNameList = new List<string>();
			foreach(string item in ProfessionList.Keys)
			{
				ProfessionNameList.Add(item);
			}

			RaceList = new Dictionary<string, Race>();
			IList<Race> tmpRaces = RaceManager.GetInstance().GetAllRaces();

			foreach(Race item in tmpRaces)
			{
				RaceList.Add(item.Name, item);
			}


			RaceNameList = new List<string>();
			foreach(string item in RaceList.Keys)
			{
				RaceNameList.Add(item);
			}

			GenderNameList = new List<string>() {"Muž","Žena" };
			ExceptionalityList = new Dictionary<string,ExceptionalityType>() { { "Vlastnosti" ,ExceptionalityType.Vlastnosti}, { "Kombinované", ExceptionalityType.Kombinovane },{ "Zázemí", ExceptionalityType.Zazemi } };

			FinishCharacterCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				IList<Information> problems = Character.ValidateStartingCharacter();

				if(problems.Count == 0)
				{
					string message = "Postava je plně validní.";
					var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Úspěch", message);
					await messageBoxStandardWindow.Show();
				}
				else
				{
					InformationDisplayViewModel idvm = new InformationDisplayViewModel("Chyby postavy",
						problems);

					ViewModelMessenger.Register(idvm);

					ResultViewModel result = await ShowInformationDisplayDialog.Handle(idvm);

					ViewModelMessenger.Unregister(idvm);
				}

			});

			//this.character = character;
			//Name = character.Name;
			//Level = character.Level;
			//Profession = character.Profession;
		}

		public void FinishCharacter()
		{

			//DataLoader.SaveSkillManagerData(SkillManager.GetInstance());
			//DataLoader.SaveProfessionManagerData(ProfessionManager.GetInstance());

			//Character c = DataLoader.LoadCharacter();
			//character = c;
			//NotifyAbsolutelyEveryAttribute();

			//DataLoader.SaveCharacter(createdCharacter);

			if(!Character.ValidateAndClose(out List<string> issues))
			{
				foreach(string item in issues)
				{
					var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Hele v*le máš tam chybu", item);
					messageBoxStandardWindow.Show();
				}
			}
			else
			{
				NotifyAll(nameof(Character));
			}
		}
	}
}