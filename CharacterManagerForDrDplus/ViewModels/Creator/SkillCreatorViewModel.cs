﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using CharacterManagerForDrDplus.Services;
using ReactiveUI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Windows.Input;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class SkillCreatorViewModel : CharacterViewModelBase
	{
		private ObservableCollection<SkillCategory> _allSkills;
		public ObservableCollection<SkillCategory> AllSkills => _allSkills;

		public Skill? _selectedSkill;
		public Skill? SelectedSkill
		{
			get => _selectedSkill;
			set
			{
				if(value != _selectedSkill)
				{
					_selectedSkill = value;
					NotifySingle(this, nameof(SelectedSkill),
						nameof(SkillName), nameof(SkillCategory), nameof(SkillLevel), nameof(SkillSource), nameof(SkillSourceFull),
						nameof(SkillHighlight),
						nameof(SkillBonuses));
				}
			}
		}

		public string SkillName => SelectedSkill != null ? SelectedSkill.Name + (SelectedSkill.Specifiction == "" ? "" : (" (" + SelectedSkill.Specifiction + ")")) : "";
		
		public string SkillCategory
		{
			get
			{
				return SelectedSkill == null ? "" :
					SelectedSkill.Type switch
					{
						SkillType.Fyzicka => "Fyzická",
						SkillType.Psychicka => "Psychická",
						SkillType.Kombinovana => "Kombinovaná",
						_ => "neznámá"
					};
			}
		}

		public int SkillLevel
		{
			get => SelectedSkill == null ? 0 : SelectedSkill.Rating;
			set
			{
				if(SelectedSkill!= null)
				{
					if(value != SelectedSkill.Rating)
					{
						Character.SkillPoints[SelectedSkill.Type]+=SelectedSkill.Rating-value;
						SelectedSkill.Rating = value;
						Character.Profession.UpdateProfession(Character);
						NotifyOthers(this, "ThiefManeuvers");
						NotifyOthers(this, "SelectedCombination");
						NotifySingle(this, nameof(SkillLevel), nameof(SkillHighlight),
							nameof(MaxSkillPoints),
							nameof(SkillPointsPhysical),
							nameof(SkillPointsMental),
							nameof(SkillPointsCombined));
					}
				}
			}
		}

		public int SkillHighlight => SkillLevel + 1;

		public string SkillSource => SelectedSkill == null ? "" : SelectedSkill.Source.ToString();

		public string SkillSourceFull => SelectedSkill == null ? "" : SelectedSkill.Source.GetFullName();

		public IList<SkillBonus>? SkillBonuses => SelectedSkill == null ? null : SelectedSkill.SkillBonuses;


		private string SkillPointsDisplay(SkillType type)
		{
			if (ShowMaxSkillPoints)
			{
				Character.GetSkillPointLimits(type, out int surePoints, out int variablePoints);
				int sureSpend = surePoints + Character.SkillPoints[type];
				return (sureSpend>=0?sureSpend:0) + "/" + surePoints + (variablePoints == 0 ? "" : " (" + (sureSpend>0?variablePoints:variablePoints+sureSpend) + ")");
			}
			else
			{
				int max = Character.StartSkillPoints[type] + Character.StartSkillPointsAdjusment[type];
				return (max + Character.SkillPoints[type]) + "/" + max;
			}
		}

		public string SkillPointsPhysical => SkillPointsDisplay(SkillType.Fyzicka);
		public string SkillPointsMental => SkillPointsDisplay(SkillType.Psychicka);
		public string SkillPointsCombined => SkillPointsDisplay(SkillType.Kombinovana);

		public string MaxSkillPoints
		{
			get
			{
				int maxPoints = Character.GetMaxSkillPoints();
				int currentPoints = maxPoints
					+ Character.SkillPoints[SkillType.Fyzicka]
					+ Character.SkillPoints[SkillType.Psychicka]
					+ Character.SkillPoints[SkillType.Kombinovana];
				return currentPoints + "/" + maxPoints;
			}
		}

		public bool ShowMaxSkillPoints => Character.Level > 1;



		public Interaction<AddSkillViewModel, SkillViewModel?> ShowAddSkillDialog { get; }
		public ICommand AddSkillCommand { get; }
		public Interaction<ConfirmationViewModel, ResultViewModel?> ShowRemoveSkillDialog { get; }
		public ICommand RemoveSkillCommand { get; }

		public SkillCreatorViewModel(Character createdCharacter) : base (createdCharacter)
		{

			ShowAddSkillDialog = new Interaction<AddSkillViewModel, SkillViewModel?>();
			ShowRemoveSkillDialog = new Interaction<ConfirmationViewModel, ResultViewModel?>();


			//SkillManager sm = SkillManager.GetInstance();

			_allSkills = FillAllSkills();
				
				/*new ObservableCollection<SkillCategory>();
			//IList<SkillData> tmp = sm.GetAllSkills();
			IList<Skill> p = new List<Skill>();
			IList<Skill> m = new List<Skill>();
			IList<Skill> c = new List<Skill>();
			//foreach (SkillData item in tmp)
			//{
			//	switch (item.Type)
			//	{
			//		case SkillType.Physical:
			//			p.Add(item.GetInstance());
			//			break;
			//		case SkillType.Mental:
			//			m.Add(item.GetInstance());
			//			break;
			//		case SkillType.Combined:
			//			c.Add(item.GetInstance());
			//			break;
			//		default:
			//			break;
			//	}
			//}
			AllSkills.Add(new SkillCategory(RuleTables.SkillTypeToString(SkillType.Fyzicka), p));
			AllSkills.Add(new SkillCategory(RuleTables.SkillTypeToString(SkillType.Psychicka), m));
			AllSkills.Add(new SkillCategory(RuleTables.SkillTypeToString(SkillType.Kombinovana), c));

			foreach(Skill item in character.Skills)
			{
				switch (item.Type)
				{
					case SkillType.Fyzicka:
						p.Add(item);
						break;
					case SkillType.Psychicka:
						m.Add(item);
						break;
					case SkillType.Kombinovana:
						c.Add(item);
						break;
					default:
						break;
				}
			}*/


			AddSkillCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				HashSet<string> skillBlacklist = new HashSet<string>();
				foreach(Skill item in Character.Skills)
				{
					if (item.Specifiction == "")
						skillBlacklist.Add(item.Name);
				}

				AddSkillViewModel asvm = new AddSkillViewModel(skillBlacklist);

				ViewModelMessenger.Register(asvm);

				SkillViewModel? result = await ShowAddSkillDialog.Handle(asvm);

				if (result != null && result.SkillData != null)
				{
					Skill itemInstance = result.SkillData.NewInstance(1,result.Specification);
					if (Character.AddSkill(itemInstance))
					{
						Character.Profession.UpdateProfession(Character);
						NotifyOthers(this, "ThiefManeuvers");
						NotifyOthers(this, "SelectedCombination");
						AddSkillToDisplay(itemInstance);
						SelectedSkill = itemInstance;
						NotifyEverything();
						//NotifySingle(this, nameof(MaxSkillPoints),
						//	nameof(SkillPointsPhysical),
						//	nameof(SkillPointsMental),
						//	nameof(SkillPointsCombined));
					}
					else
					{
						//Window mainWindow = ((IVisual)this).FindAncestorOfType<Window>();
						//await MessageBox.Show(null, "Postava nemá dostatek peněz na koupi. Chybí " + (result.BuyingPrice - createdCharacter.Money) + ".", "Nedostatek peněz", MessageBox.MessageBoxButtons.Ok);
						string message = "Postava nemá dostatek bodů na získání dovednosti\"" + itemInstance.FullName + "\".";
						var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Nedostatek bodů dovednosti", message);
						await messageBoxStandardWindow.Show();
					}
				}

				ViewModelMessenger.Unregister(asvm);
			});

			RemoveSkillCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				if (SelectedSkill != null)
				{
					ConfirmationViewModel cnf_vm = new ConfirmationViewModel("Smazat dovednost?", "smazat dovednost " + SelectedSkill.FullName);

					ViewModelMessenger.Register(cnf_vm);

					ResultViewModel? result = await ShowRemoveSkillDialog.Handle(cnf_vm);

					if (result != null && result.Value)
					{
						if(createdCharacter.RemoveSkill(SelectedSkill))
						{
							Character.Profession.UpdateProfession(Character);
							NotifyOthers(this,"ThiefManeuvers");
							NotifyOthers(this, "SelectedCombination");
							RemoveSkillFromDisplay(SelectedSkill);
							SelectedSkill = null;
							NotifyEverything();
							//NotifySingle(this, nameof(MaxSkillPoints), nameof(MaxSkillPoints),
							//	nameof(SkillPointsPhysical),
							//	nameof(SkillPointsMental),
							//	nameof(SkillPointsCombined));
						}
					}

					ViewModelMessenger.Unregister(cnf_vm);
				}
			});
		}

		public override void SetCharacter(Character newCharacter)
		{
			base.SetCharacter(newCharacter);

			_allSkills = FillAllSkills();
			SelectedSkill = null;
		}

		private ObservableCollection<SkillCategory> FillAllSkills()
		{
			ObservableCollection<SkillCategory> newAllSkills = new ObservableCollection<SkillCategory>();

			IList<Skill> p = new List<Skill>();
			IList<Skill> m = new List<Skill>();
			IList<Skill> c = new List<Skill>();

			foreach (Skill item in Character.Skills)
			{
				switch (item.Type)
				{
					case SkillType.Fyzicka:
						p.Add(item);
						break;
					case SkillType.Psychicka:
						m.Add(item);
						break;
					case SkillType.Kombinovana:
						c.Add(item);
						break;
					default:
						break;
				}
			}

			newAllSkills.Add(new SkillCategory(RuleTables.SkillTypeToString(SkillType.Fyzicka), p));
			newAllSkills.Add(new SkillCategory(RuleTables.SkillTypeToString(SkillType.Psychicka), m));
			newAllSkills.Add(new SkillCategory(RuleTables.SkillTypeToString(SkillType.Kombinovana), c));

			return newAllSkills;
		}

		private void AddSkillToDisplay(Skill addSkill)
		{
			foreach (SkillCategory item in _allSkills)
			{
				string category = RuleTables.SkillTypeToString(addSkill.Type);
				if (item.CategoryName == category)
				{
					item.Items.Add(addSkill);
					return;
				}
			}
		}

		private void RemoveSkillFromDisplay(Skill removeSkill)
		{
			foreach (SkillCategory item in _allSkills)
			{
				string category = RuleTables.SkillTypeToString(removeSkill.Type);
				if (item.CategoryName == category)
				{
					item.Items.Remove(removeSkill);
					return;
				}
			}
		}
	}
}