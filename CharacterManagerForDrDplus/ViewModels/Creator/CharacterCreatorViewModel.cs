﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class CharacterCreatorViewModel : ViewModelBase
	{

		#region ViewModel Declarations
		CharacterViewModelBase attributesTab;
		CharacterViewModelBase combatTab;
		CharacterViewModelBase conditionTab;
		CharacterViewModelBase inventoryTab;
		CharacterViewModelBase skillTab;
		CharacterViewModelBase professionTab;
		//CharacterViewModelBase fighterTab;
		//CharacterViewModelBase wizardTab;
		//CharacterViewModelBase thiefTab;
		//CharacterViewModelBase theurgTab;
		//CharacterViewModelBase rangerTab;
		//CharacterViewModelBase priestTab;

		public CharacterViewModelBase AttributesTab
		{
			get => attributesTab;
			private set
			{
				if (SetAndRaiseIfChanged(() => attributesTab, () => attributesTab = value, value))
				{

				}
				// this.RaiseAndSetIfChanged(ref attributesTab, value);
			}
		}
		public CharacterViewModelBase CombatTab
		{
			get => combatTab;
			private set
			{
				if (SetAndRaiseIfChanged(() => combatTab, () => combatTab = value, value))
				{

				}
				// this.RaiseAndSetIfChanged(ref combatTab, value);
			}
		}
		public CharacterViewModelBase ConditionTab
		{
			get => conditionTab;
			private set
			{
				if (SetAndRaiseIfChanged(() => conditionTab, () => conditionTab = value, value))
				{

				}
				// this.RaiseAndSetIfChanged(ref conditionTab, value);
			}
		}
		public CharacterViewModelBase InventoryTab
		{
			get => inventoryTab;
			private set
			{
				if (SetAndRaiseIfChanged(() => inventoryTab, () => inventoryTab = value, value))
				{

				}
				// this.RaiseAndSetIfChanged(ref inventoryTab, value);
			}
		}
		public CharacterViewModelBase SkillTab
		{
			get => skillTab;
			private set
			{
				if (SetAndRaiseIfChanged(() => skillTab, () => skillTab = value, value))
				{

				}
				//this.RaiseAndSetIfChanged(ref skillTab, value);
			}
		}
		public CharacterViewModelBase ProfessionTab
		{
			get => professionTab;
			private set
			{
				if (SetAndRaiseIfChanged(() => professionTab, () => professionTab = value, value))
				{

				}
				//this.RaiseAndSetIfChanged(ref professionTab, value);
			}
		}
		#endregion
		/*
		private int selectedTab;
		public int SelectedTab
		{
			get { return selectedTab; }
			set
			{
				if (SelectedTab == 2)
					ConditionTab;
				selectedTab = value;
			}
		}*/


		Character Character;

		public string ProfessionName
		{
			get
			{
				return Character.Profession.Name;
			}
		}

		internal void UpdateProfessionTab()
		{
			CharacterViewModelBase tmp = Character.Profession switch
			{
				Fighter => new FighterCreatorViewModel(Character),
				Wizard => new WizardCreatorViewModel(Character),
				Thief => new ThiefCreatorViewModel(Character),
				Theurg => new TheurgCreatorViewModel(Character),
				Ranger => new RangerCreatorViewModel(Character),
				Priest => new PriestCreatorViewModel(Character),
				_ => throw new Exception()
			};
			if(ProfessionTab != null)
				ViewModelMessenger.Unregister(ProfessionTab);
			ViewModelMessenger.Register(tmp);
			professionTab = tmp;
			NotifySingle(this,nameof(ProfessionTab));
		}

		public CharacterCreatorViewModel(Character createdCharacter)
		{
			Character = createdCharacter;
			AttributesTab = new AttributesCreatorViewModel(createdCharacter, this);
			CombatTab = new CombatCreatorViewModel(createdCharacter);
			ConditionTab = new ConditionCreatorViewModel(createdCharacter);
			InventoryTab = new InventoryCreatorViewModel(createdCharacter);
			SkillTab = new SkillCreatorViewModel(createdCharacter);

			UpdateProfessionTab();

			//fighterTab = new FighterCreatorViewModel(character);
			//wizardTab = new WizardCreatorViewModel(character);
			//thiefTab = new ThiefCreatorViewModel(character);
			//theurgTab = new TheurgCreatorViewModel(character);
			//rangerTab = new RangerCreatorViewModel(character);
			//priestTab = new PriestCreatorViewModel(character);

			//ProfessionTab = fighterTab;

			ViewModelMessenger.Register(this);
			ViewModelMessenger.Register(AttributesTab);
			ViewModelMessenger.Register(CombatTab);
			ViewModelMessenger.Register(ConditionTab);
			ViewModelMessenger.Register(InventoryTab);
			ViewModelMessenger.Register(SkillTab);
			//ViewModelMessenger.Register(ProfessionTab);
			//ViewModelMessenger.Register(fighterTab);
			//ViewModelMessenger.Register(wizardTab);
			//ViewModelMessenger.Register(thiefTab);
			//ViewModelMessenger.Register(theurgTab);
			//ViewModelMessenger.Register(rangerTab);
			//ViewModelMessenger.Register(priestTab);
		}

		internal void Close()
		{
			ViewModelMessenger.Unregister(this);
			ViewModelMessenger.Unregister(AttributesTab);
			ViewModelMessenger.Unregister(CombatTab);
			ViewModelMessenger.Unregister(ConditionTab);
			ViewModelMessenger.Unregister(InventoryTab);
			ViewModelMessenger.Unregister(SkillTab);
		}

		internal void CharacterChanged(Character newCharacter)
		{
			Character = newCharacter;
			UpdateProfessionTab();
			AttributesTab.SetCharacter(newCharacter);
			CombatTab.SetCharacter(newCharacter);
			ConditionTab.SetCharacter(newCharacter);
			InventoryTab.SetCharacter(newCharacter);
			SkillTab.SetCharacter(newCharacter);
			ProfessionTab.SetCharacter(newCharacter);
			NotifyEveryTab();
		}

		private void NotifyEveryTab()
		{
			AttributesTab.NotifyEverything();
			CombatTab.NotifyEverything();
			ConditionTab.NotifyEverything();
			InventoryTab.NotifyEverything();
			SkillTab.NotifyEverything();
			ProfessionTab.NotifyEverything();
			//NotifySingle(this, nameof(ProfessionName));
			NotifyEverything();
		}

		public void Test()
		{
			int i = 0;
		}

	}
}