﻿using CharacterManagerForDrDplus.Entities;
using ReactiveUI;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class ConditionCreatorViewModel : CharacterViewModelBase
	{

		public ConditionCreatorViewModel(Character createdCharacter) : base(createdCharacter)
		{
		}
	}
}