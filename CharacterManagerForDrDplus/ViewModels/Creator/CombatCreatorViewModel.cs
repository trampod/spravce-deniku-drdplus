﻿using CharacterManagerForDrDplus.Entities;
using ReactiveUI;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class CombatCreatorViewModel : CharacterViewModelBase
	{
		//public ObservableCollection<IWeaponCombination> WeaponCombinations { get; }

		public IList<IWeaponCombination> WeaponCombinations => Character.GetWeaponCombinations();

		private IWeaponCombination _weaponCombination;
		public IWeaponCombination SelectedCombination
		{
			get
			{
				if(_weaponCombination != null)
					_weaponCombination.Recalculate(Character);
				return _weaponCombination;
			}
			set
			{
				if (SetAndRaiseIfChanged(() => _weaponCombination, () => _weaponCombination = value, value))
				{

				}
			}
		}

		public string CombatTotal => (Character.Combat < 0 ? "" : "+") + Character.Combat;
		public string AttackTotal => (Character.Attack < 0 ? "" : "+") + Character.Attack;
		public string RangedTotal => (Character.Ranged < 0 ? "" : "+") + Character.Ranged;
		public string DefenseTotal => (Character.Defense < 0 ? "" : "+") + Character.Defense;

		public CombatCreatorViewModel(Character createdCharacter) : base(createdCharacter)
		{
			//WeaponCombinations = new ObservableCollection<IWeaponCombination>(createdCharacter.GetWeaponCombinations);
			//WeaponCombinations = new ObservableCollection<WeaponCombination>(createdCharacter.OwnedWeaponCombinations);
		}
	}
}