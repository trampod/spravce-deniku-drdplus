﻿using Avalonia.Controls;
using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using CharacterManagerForDrDplus.Services;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Windows.Input;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class MainWindowViewModel : ViewModelBase
	{
		//public ObservableCollection<Character> Characters { get; set; }
		ViewModelBase _characterView;

		Character? _currentCharacter;
		public Character? CurrentCharacter => _currentCharacter;
		string saveLocation = "";
		Window _mainWindow;

		//public bool CharacterLoaded => _currentCharacter != null;

		public ViewModelBase CharacterView
		{
			get => _characterView;
			private set
			{
				if (SetAndRaiseIfChanged(() => _characterView, () => _characterView = value, value))
				{

				}
				// this.RaiseAndSetIfChanged(ref characterView, value);
			}
		}


		public string Greeting => "Welcome to Avalonia!";

		public ICommand NewCharacterCommand { get; }
		public Interaction<NewCharacterViewModel, CharacterViewModel?> ShowNewCharacterDialog { get; }
		public Interaction<ConfirmationViewModel, ResultViewModel?> ShowConfirmationDialog { get; }
		public ICommand LoadCharacterCommand { get; }
		public ICommand SaveCharacterCommand { get; }
		public ICommand SaveAsCharacterCommand { get; }
		public ICommand ExportCharacterCommand { get; }
		public ICommand CloseCharacterCommand { get; }
		public ICommand HelpCommand { get; }
		public Interaction<InformationDisplayViewModel, ResultViewModel> ShowInformationDisplayDialog { get; }

		public MainWindowViewModel(Window mainWindow)
		{
			_mainWindow = mainWindow;

			_characterView = new EmptyCharacterViewModel();

			ShowNewCharacterDialog = new Interaction<NewCharacterViewModel, CharacterViewModel?>();
			ShowConfirmationDialog = new Interaction<ConfirmationViewModel, ResultViewModel?>();
			ShowInformationDisplayDialog = new Interaction<InformationDisplayViewModel, ResultViewModel>();

			//DataLoader.SaveRaceManagerData(RaceManager.GetInstance());
			//DataLoader.SaveItemManagerData(ItemManager.GetInstance());
			//new Fighter("Bojovník", AttributeCode.Obr, new AttributeCode[] { AttributeCode.Obr, AttributeCode.Sil }),
			//new Race("Člověk", "Lidská žena",
			//	new Dictionary<AttributeCode, int>(),
			//	new Dictionary<AttributeCode, int>() { { AttributeCode.Sil, -1 }, { AttributeCode.Chr, 1 } },
			//	new List<string>(), "", 180, 80, 0));


			//currentCharacter = new Character("Velká berta", 1,
			//	ProfessionManager.GetInstance().GetProfessionData(typeof(Thief)).GetNewInstance(),
			//	RaceManager.GetInstance().GetAllRaces()[0]);
			//currentCharacter.UpdateDerivedValues();
			//CharacterView = new CharacterCreatorViewModel(currentCharacter);

			NewCharacterCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				bool canCreate = false;
				if (CurrentCharacter != null)
				{
					ConfirmationViewModel cnf_vm = new ConfirmationViewModel("Vytvořit novou postavu?", "vytvořit novou postavu", "Můžete tím přijít o neuložené změny.");

					ViewModelMessenger.Register(cnf_vm);

					ResultViewModel? confirmationResult = await ShowConfirmationDialog.Handle(cnf_vm);

					ViewModelMessenger.Unregister(cnf_vm);

					if (confirmationResult != null && confirmationResult.Value)
						canCreate = confirmationResult.Value;
				}

				if(_currentCharacter == null || canCreate)
				{
					NewCharacterViewModel ncvm = new NewCharacterViewModel();

					ViewModelMessenger.Register(ncvm);

					CharacterViewModel? result = await ShowNewCharacterDialog.Handle(ncvm);

					if (result != null && result.CharacterData != null)
					{
						NewCharacter(result.CharacterData);
					}

					ViewModelMessenger.Unregister(ncvm);
				}
			});

			LoadCharacterCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				bool canLoad = false;
				if (_currentCharacter != null)
				{
					ConfirmationViewModel cnf_vm = new ConfirmationViewModel("Načíst postavu?", "načíst postavu ze souboru", "Můžete tím přijít o neuložené změny.");

					ViewModelMessenger.Register(cnf_vm);

					ResultViewModel? confirmationResult = await ShowConfirmationDialog.Handle(cnf_vm);

					ViewModelMessenger.Unregister(cnf_vm);

					if (confirmationResult != null && confirmationResult.Value)
					{
						canLoad = confirmationResult.Value;
					}
				}


				if (_currentCharacter == null || canLoad)
				{
					OpenFileDialog dialog = new OpenFileDialog();
					dialog.Filters.Add(new FileDialogFilter() { Name = "Postava", Extensions = { "plus" } });

					string[] results = await dialog.ShowAsync(_mainWindow);

					if (results == null)
						results = new string[] { "" };


					if (results.Length == 0 || results[0] == null || results[0] == "")
					{
						//oznámení problému
					}
					else
					{
						LoadCharacter(results[0]);
					}
				}
			});

			SaveCharacterCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				if (_currentCharacter != null)
				{
					string result = saveLocation;
					if (result == "")
					{
						SaveFileDialog dialog = new SaveFileDialog();
						dialog.Filters.Add(new FileDialogFilter() { Name = "Postava", Extensions = { "plus" } });

						result = await dialog.ShowAsync(_mainWindow);

						if (result == null)
							result = "";

					}

					if (result == null || result == "")
					{
						//oznámení problému
					}
					else
					{
						SaveCharacter(result);
					}
				}
			});

			SaveAsCharacterCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				if (_currentCharacter != null)
				{
					SaveFileDialog dialog = new SaveFileDialog();
					dialog.Filters.Add(new FileDialogFilter() { Name = "Postava", Extensions = { "plus" } });

					string result = await dialog.ShowAsync(_mainWindow);

					if (result == null)
						result = "";

					if (result == null || result == "")
					{
						//oznámení problému
					}
					else
					{
						SaveCharacter(result);
					}
				}
			});

			ExportCharacterCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				if (_currentCharacter != null)
				{
					SaveFileDialog dialog = new SaveFileDialog();
					dialog.Filters.Add(new FileDialogFilter() { Name = "PDF", Extensions = { "pdf" } });

					string result = await dialog.ShowAsync(_mainWindow);

					if (result == null)
						result = "";

					if (result == null || result == "")
					{
						//oznámení problému
					}
					else
					{
						PDFCharacterExporter exporter = new PDFCharacterExporter();
						exporter.ExportCharacter(_currentCharacter, result);
					}
				}
			});

			CloseCharacterCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				ConfirmationViewModel cnf_vm = new ConfirmationViewModel("Zavřít postavu?", "zavřít postavu " + CurrentCharacter!.Name,"Můžete tím přijít o neuložené změny.");

				ViewModelMessenger.Register(cnf_vm);

				ResultViewModel? result = await ShowConfirmationDialog.Handle(cnf_vm);

				ViewModelMessenger.Unregister(cnf_vm);

				if(result != null && result.Value)
				{
					_currentCharacter = null;
					saveLocation = "";
					((CharacterCreatorViewModel)_characterView!).Close();
					_characterView = new EmptyCharacterViewModel();
					NotifySingle(this, nameof(CharacterView));
				}
			});

			HelpCommand = ReactiveCommand.CreateFromTask(async () =>
			{
				InformationDisplayViewModel idvm = new InformationDisplayViewModel("Nápověda",
					HelpManager.GetInstance().Informations);
					//new List<Information>()
					//{
					//	new Information("test", "jsut sume randow jiberish"),
					//	new Information("test2", "kjkjlkjlklklkj"),
					//	new Information("ernie", "Erniew byl jedním z nejslavnějsích vitíračů kotlů co jsem znal. ale Nevěděl že ho to jednoho dne bude stát život. byl totiž zabit medvědem co vytíral kotla... \nthe end"),
					//	new Information("2+2=4", "jsut sume mor randow jiberish")
					//});

				ViewModelMessenger.Register(idvm);

				ResultViewModel result = await ShowInformationDisplayDialog.Handle(idvm);

				ViewModelMessenger.Unregister(idvm);
			});

			/*
			Characters = new ObservableCollection<Character>();
			Characters.Add(new Character("Velká berta", 8, new Fighter("Bojovník",AttributeCode.Obr,new string[] {AttributeCode.Obr,"Str" })));
			Characters.Add(new Character("Tlapka", 3, new Fighter("Zloděj", AttributeCode.Zrc, new string[] { AttributeCode.Obr, AttributeCode.Zrc })));
			Characters.Add(new Character("Krom", 20, new Fighter("Bojovník", AttributeCode.Obr, new string[] { AttributeCode.Obr, "Str" })));
			Characters.Add(new Character("Vitorio", 3, new Fighter("Čaroděj", AttributeCode.Int, new string[] { AttributeCode.Int, AttributeCode.Vol })));
			*/
		}

		public void NewCharacter(Character newCharacter)
		{
			//currentCharacter = new Character("Velká berta", 1,
			//	ProfessionManager.GetInstance().GetProfessionData(typeof(Thief)).GetNewInstance(),
			//	RaceManager.GetInstance().GetAllRaces()[0]);


			_currentCharacter = newCharacter;
			saveLocation = "";
			newCharacter.UpdateDerivedValues();
			if(CharacterView.GetType() == typeof(EmptyCharacterViewModel))
				CharacterView = new CharacterCreatorViewModel(_currentCharacter);
			else
				((CharacterCreatorViewModel)CharacterView).CharacterChanged(_currentCharacter);
			NotifySingle(this, nameof(CurrentCharacter));
		}

		public void SaveCharacter(string path)
		{
			DataLoader.SaveCharacter(_currentCharacter, path);
			saveLocation = path;
		}

		public async void LoadCharacter(string path)
		{
			try
			{
				Character newCharacter = DataLoader.LoadCharacter(path);
				_currentCharacter = newCharacter;
				_currentCharacter.UpdateDerivedValues();
				saveLocation = path;
				if(CharacterView.GetType() == typeof(EmptyCharacterViewModel))
					CharacterView = new CharacterCreatorViewModel(_currentCharacter);
				else
					((CharacterCreatorViewModel)CharacterView).CharacterChanged(_currentCharacter);
				NotifySingle(this, nameof(CurrentCharacter));
			}
			catch (Exception e)
			{
				_currentCharacter = null;
				CharacterView = new EmptyCharacterViewModel();
				saveLocation = "";
				string message = "Nepodařilo se načíst postavu.";
				var messageBoxStandardWindow = MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Chyba", message);
				await messageBoxStandardWindow.Show();
			}
		}

		public void CloseApplication()
		{
			Environment.Exit(0);
		}
	}
}
