﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class NewCharacterViewModel : ViewModelBase
	{
		public List<Profession> ProfessionList { get; }
		public Profession SelectedProfession
		{
			get;
			set;
		}


		public List<Race> RaceList { get; }
		public Race SelectedRace { get; set; }


		public string CharacterName { get; set; } = "";


		public ReactiveCommand<Unit, CharacterViewModel?> NewCharacterCommand { get; }
		public ReactiveCommand<Unit, CharacterViewModel?> NewCharacterCancel { get; }

		private CharacterViewModel? CompleteNewCharacterViewModel
		{
			get => new CharacterViewModel(new Character(CharacterName,1,SelectedProfession,SelectedRace));
		}

		private CharacterViewModel? EmptyCharacterViewModel
		{
			get => new CharacterViewModel(null);
		}
		public NewCharacterViewModel()
		{
			RaceList = new List<Race>();
			ProfessionList = new List<Profession>();


			ProfessionManager pm = ProfessionManager.GetInstance();
			foreach (Type item in ProfessionManager.GetAllProfessions())
			{
				ProfessionList.Add(pm.GetProfessionData(item).GetNewInstance());
			}
			SelectedProfession = ProfessionList[0];

			RaceManager rm = RaceManager.GetInstance();
			foreach (Race item in rm.GetAllRaces())
			{
				RaceList.Add(item);
			}
			SelectedRace = RaceList[0];



			NewCharacterCommand = ReactiveCommand.Create(() =>
			{
				return CompleteNewCharacterViewModel;
			});

			NewCharacterCancel = ReactiveCommand.Create(() =>
			{
				return EmptyCharacterViewModel;
			});
		}
	}
}
