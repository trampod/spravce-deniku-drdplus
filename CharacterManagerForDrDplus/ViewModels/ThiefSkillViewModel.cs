﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class ThiefSkillViewModel : ViewModelBase
	{
		public ThiefSkill? ThiefSkillData { get; }

		public ThiefSkillViewModel(ThiefSkill? thiefSkillData)
		{
			ThiefSkillData = thiefSkillData;
		}
	}
}
