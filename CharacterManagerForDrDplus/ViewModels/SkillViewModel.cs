﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class SkillViewModel : ViewModelBase
	{
		public SkillData? SkillData { get; }
		public string Specification { get; }

		public SkillViewModel(SkillData? skillData, string specification)
		{
			SkillData = skillData;
			Specification = specification;
		}
	}
}