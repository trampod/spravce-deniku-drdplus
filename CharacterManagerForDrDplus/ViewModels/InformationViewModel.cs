﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class InformationViewModel : ViewModelBase
	{
		public Information InformationData { get; set; }

		public string Subject => InformationData.Subject;
		public string Content => InformationData.Content;

		public InformationViewModel(Information information)
		{
			InformationData = information;
		}
	}
}
