﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.ViewModels
{
	public interface IViewModel
	{
		void OnModelChanged();
		void OnModelChanged(IEnumerable<String> properties);
	}
}
