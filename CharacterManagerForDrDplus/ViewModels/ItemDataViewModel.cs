﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class ItemDataViewModel : ViewModelBase
	{
		public IItemData? Item { get; }
		public int BuyingPrice { get; }
		public bool IsBuying { get; }
		public int Count { get; }

		public ItemDataViewModel(IItemData? item, int buyingPrice, bool isBuying, int count)
		{
			Item = item;
			BuyingPrice = buyingPrice;
			IsBuying = isBuying;
			Count = count;
		}
	}
}
