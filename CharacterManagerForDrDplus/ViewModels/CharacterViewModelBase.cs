﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class CharacterViewModelBase : ViewModelBase
	{
		protected Character Character { get; private set; }

		public CharacterViewModelBase(Character newCharacter)
		{
			Character = newCharacter;
		}

		public virtual void SetCharacter(Character newCharacter)
		{
			Character = newCharacter;
		}
	}
}
