﻿using CharacterManagerForDrDplus.Models;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class ViewModelBase : ReactiveObject //INotifyPropertyChanged //ReactiveObject
    {
        private readonly Guid _id = Guid.NewGuid();

        protected virtual Boolean SetAndRaiseIfChanged<T>(Func<T> getter, Action setter, T value, [CallerMemberName] String property = "")
        {
            T oldValue = getter();
            if (!Object.Equals(oldValue, value))
            {
                setter();
                OnPropertyChanged(property);

                NotifyOthers(this, property);

                return true;
            }
            return false;
        }

        protected internal virtual void OnModelChanged(IEnumerable<String> propertyNames)
        {
            foreach (String property in propertyNames)
            {
                OnPropertyChanged(property);
            }
        }

        protected void OnPropertyChanged([CallerMemberName] String property = "")
        {
            OnPropertyChanged(new PropertyChangedEventArgs(property));
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            //PropertyChanged?.Invoke(this, e);
            ((IReactiveObject)this).RaisePropertyChanged(e);
        }

        //public event PropertyChangedEventHandler? PropertyChanged;



        public static void Register(ViewModelBase viewModel)
        {
            ViewModelMessenger.Register(viewModel);
        }

        public static void Unregister(ViewModelBase viewModel)
        {
            ViewModelMessenger.Unregister(viewModel);
        }

        public static void NotifyOthers(ViewModelBase viewModel, params String[] properties)
        {
            ViewModelMessenger.NotifyOthers(viewModel, properties);
        }

        public static void NotifyAll(params String[] properties)
        {
            ViewModelMessenger.NotifyAll(properties);
        }

        public static void NotifySingle(ViewModelBase viewModel, params String[] properties)
        {
            ViewModelMessenger.NotifySingle(viewModel,properties);
        }

        public void NotifyEverything()
        {
            List<string> propertiesNames = new List<string>();
            System.Reflection.PropertyInfo[] tmp = this.GetType().GetProperties();
            string thisType = this.GetType().Name;
            foreach (System.Reflection.PropertyInfo item in tmp)
            {
                if (item.DeclaringType?.Name == thisType)
                    propertiesNames.Add(item.Name);
            }
            NotifySingle(this, propertiesNames.ToArray());
        }





        public override Boolean Equals(Object? obj)
        {
            return Equals(obj as ViewModelBase);
        }

		public Boolean Equals(ViewModelBase? other)
        {
            return other != null && _id.Equals(other._id);
        }

        public override Int32 GetHashCode()
        {
            return HashCode.Combine(_id);
        }

        public static Boolean operator ==(ViewModelBase? left, ViewModelBase? right)
        {
            return EqualityComparer<ViewModelBase>.Default.Equals(left, right);
        }

        public static Boolean operator !=(ViewModelBase? left, ViewModelBase? right)
        {
            return !(left == right);
        }
    }
}
