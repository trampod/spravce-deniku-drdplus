﻿using CharacterManagerForDrDplus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.ViewModels
{
	public class CharacterViewModel : ViewModelBase
	{

		public Character? CharacterData { get; }

		public CharacterViewModel(Character? character)
		{
			CharacterData = character;
		}

	}
}
