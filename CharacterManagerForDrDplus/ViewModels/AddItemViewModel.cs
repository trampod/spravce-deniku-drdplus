﻿using CharacterManagerForDrDplus.Entities;
using CharacterManagerForDrDplus.Models;
using CharacterManagerForDrDplus.Services;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace CharacterManagerForDrDplus.ViewModels
{

	public class AddItemViewModel : ViewModelBase
	{

		private readonly static IItemData[] _empty = new IItemData[0];

		public IList<IItemData> CategoryItems
		{
			get
			{
				/*if (IsSearching)
				{
					return _searchResult;
				}
				else*/
				if (!String.IsNullOrWhiteSpace(_categoryName) && _itemList.TryGetValue(_categoryName, out ItemDataCategory? category))
				{
					return category.Items;
				}
				else
				{
					return _empty;
				}
			}
		}

		private Dictionary<string, ItemDataCategory> _itemList;

		public IReadOnlyCollection<String> CategoryList
		{
			get
			{
				return _itemList.Keys;
			}
		}

		private IReadOnlyCollection<IItemData> _allItems;
		private IList<IItemData> _searchResult;


		private string _categoryName = "";
		public string CategoryName
		{
			get
			{
				return _categoryName;
			}
			set
			{
				if (value != _categoryName)
				{
					_categoryName = value;
					NotifySingle(this, nameof(CategoryItems),
					nameof(SelectedAddItem));
				}
			}
		}

		private string _searchValue = "";
		public string SearchValue
		{
			get => _searchValue;
			set
			{
				/*
				_searchValue = value;
				if (_searchValue != "")
				{
					_searchResult = _allItems.Where(item => item.Name.Contains(_searchValue)).ToList<IItem>();
				}
				NotifySingle(this, nameof(CategoryItems));
				*/
			}
		}

		public bool IsSearching => _searchValue != "";

		private IItemData _selectedAddItem;
		public IItemData SelectedAddItem
		{
			get => _selectedAddItem;
			set
			{
				_selectedAddItem = value;
				if (_selectedAddItem != null)
					_addItemPrice = _selectedAddItem.AveragePrice;
				NotifySingle(this, nameof(AddItemName),
					nameof(AddItemCategory),
					nameof(AddItemWeight),
					nameof(AddItemAveragePrice),
					nameof(AddItemSource), nameof(AddItemSourceFull),
					nameof(AddItemCount),
					nameof(AddItemPrice),
					nameof(SelectedAddItem),
					nameof(AddItemFullPrice));
			}
		}
		public string AddItemName => SelectedAddItem == null ? "" : SelectedAddItem.Name;
		public string AddItemCategory => SelectedAddItem == null ? "" : SelectedAddItem.Category;
		public string AddItemWeight => SelectedAddItem == null ? "" : Math.Round(SelectedAddItem.Weight, 2).ToString();
		public string AddItemAveragePrice => SelectedAddItem == null ? "" : RuleTables.GetPresentableMoney(SelectedAddItem.AveragePrice);
		public string AddItemSource => SelectedAddItem == null ? "" : SelectedAddItem.Source.ToString();
		public string AddItemSourceFull => SelectedAddItem == null ? "" : SelectedAddItem.Source.GetFullName();

		private int _addItemCount = 1;
		public int AddItemCount
		{
			get => _addItemCount;
			set 
			{
				_addItemCount = value;

				NotifySingle(this, nameof(AddItemPrice), nameof(AddItemFullPrice));
			}
		}

		private bool _addItemPay = true;
		public bool AddItemPay
		{
			get => _addItemPay;
			set
			{
				_addItemPay = value;

				NotifySingle(this, nameof(AddItemFullPrice));
			}
		}

		private int? _addItemPrice;
		public double AddItemPrice
		{
			get
			{
				if(_addItemPrice == null)
				{
					_addItemPrice = SelectedAddItem==null?null:SelectedAddItem.AveragePrice;
				}
				return _addItemPrice==null?0:(((double)_addItemPrice) / 100);
			}
			set
			{
				_addItemPrice = (int)Math.Round(value * 100);
				NotifySingle(this, nameof(AddItemFullPrice));
			}
		}

		public string AddItemFullPrice => RuleTables.GetPresentableMoney(_addItemPay?_addItemPrice*_addItemCount:0);
		
		public bool CharacterIsClosed { get; }


		public ReactiveCommand<Unit, ItemDataViewModel?> AddItemCommand { get; }
		public ReactiveCommand<Unit, ItemDataViewModel?> AddItemCancel { get; }

		private ItemDataViewModel? SelectedItemViewModel
		{
			get => new ItemDataViewModel(_selectedAddItem, _addItemPrice == null ? 0 : ((int)_addItemPrice),AddItemPay, _addItemCount);
		}

		private ItemDataViewModel? EmptyItemViewModel
		{
			get => new ItemDataViewModel(null, 0, false,0);
		}

		public AddItemViewModel(bool characterIsClosed)
		{
			CharacterIsClosed = characterIsClosed;
			ItemManager im = ItemManager.GetInstance();


			IList<ItemDataCategory> itemData = im.GetAllCategories();
			/*IList<IItem> allItems = new List<IItem>();
			foreach (ItemCategory item in itemData)
			{
				allItems = (IList<IItem>)allItems.Concat(item.Items);
			}
			_allItems = (IReadOnlyCollection<IItem>)allItems;*/

			_itemList = itemData.ToDictionary(item => item.CategoryName, item => item);

			if (CategoryList.Count > 0)
			{
				_categoryName = CategoryList.First();
			}

			AddItemCommand = ReactiveCommand.Create(() =>
			{
				return SelectedItemViewModel;
			});

			AddItemCancel = ReactiveCommand.Create(() =>
			{
				return EmptyItemViewModel;
			});
		}
	}
}
